import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import ReduxThunk from 'redux-thunk';
import Router from './router';
import reducers from './redux/reducers';
import { library } from '@fortawesome/fontawesome-svg-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faStroopwafel } from '@fortawesome/free-solid-svg-icons';
import './res/fonts/fontIRANSans.scss';
import './res/fonts/fontLato.scss';
import './res/global.scss';

library.add(faStroopwafel)

const Index = () => {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
        <Provider store={store}>
            <Router />
        </Provider>
    )
};

ReactDOM.render(<Index />, document.getElementById("index"));