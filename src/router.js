import React, { Component } from 'react';
import { HashRouter, Switch, Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import Dashboard from './pages/Dashboard/Dashboard';
import { set_token } from './redux/actions';
import Login from "./pages/Auth/Login";
import {ACCESS_TOKEN} from "./redux/types";
import Calendar from "./pages/Calendar/Calendar";

import Settings from "./pages/Settings";
import CompanyDetails from "./pages/Settings/AccountSetup/CompanyDetails";
import Locations from "./pages/Settings/AccountSetup/Locations";
import Resources from "./pages/Settings/AccountSetup/Resources";
import CalendarSettings from "./pages/Settings/AccountSetup/CalendarSettings";
import OnlineBooking from "./pages/Settings/AccountSetup/OnlineBooking";
import StaffNotifications from "./pages/Settings/AccountSetup/StaffNotifications";
import PaymentTypes from "./pages/Settings/PointOfSale/PaymentTypes";
import Taxes from "./pages/Settings/PointOfSale/Taxes";
import Discounts from "./pages/Settings/PointOfSale/Discounts";
import SalesSettings from "./pages/Settings/PointOfSale/SalesSettings";
import SettingsInvoices from "./pages/Settings/PointOfSale/Invoices";
import ClientNotifications from "./pages/Settings/ClientSettings/ClientNotifications";
import ReferralSources from "./pages/Settings/ClientSettings/ReferralSources";
import CancellationReasons from "./pages/Settings/ClientSettings/CancellationReasons";
import Sales from "./pages/Sales/Sales";
import Booking from "./pages/Bookings/Bookings";
import BookingDetail from "./pages/Bookings/Detail";
import Messages from "./pages/Messages/Messages";
import Staff from "./pages/Staff/Staff";
import Services from "./pages/Services/Services";
import Inventory from "./pages/Inventory/Inventory";
import ProductSingle from "./pages/Inventory/Products/ProductSingle";
import Register from "./pages/Auth/Register";
import Forgot from "./pages/Auth/Forgot";
import RegisterEntity from "./pages/Auth/RegisterEntity";
import Comments from "./pages/Comments/Comments";
import NotFound from "./pages/NotFound/NotFound";
import Gallery from "./pages/Settings/AccountSetup/Gallery/Gallery";
import WorkingTime from "./pages/Settings/AccountSetup/WorkingTime/WorkingTime";
import BookingView from "./pages/Bookings/BookingView";
import BookingFormView from "./pages/Bookings/BookingFormView";
import BookingOrderView from "./pages/Bookings/AddBooking/BookingOrderView";
import BookingAddView from "./pages/Bookings/AddBooking/BookingAddView";
import Invoices from "./pages/Invoices/Invoices";
import Invoice from "./pages/Invoices/Invoice";

class RouterComponent extends Component {
    constructor(props) {
        super(props);
    }

    componentWillMount() {
        const token = localStorage.getItem(ACCESS_TOKEN);
        this.props.set_token(token);
    }

    checkLogin() {
        // return this.props.loggedIn ? <Dashboard /> : <Redirect to="/login"/>
    }

    render() {
        const { loggedIn } = this.props;
        return (
            <HashRouter>
                <Switch>
                    {/*<Route path='/' exact render={() => (*/}
                        {/*this.props.loggedIn ? (*/}
                            {/*<Redirect to="/dashboard"/>*/}
                        {/*) : (*/}
                            {/*<Redirect to="/login"/>*/}
                        {/*)*/}
                    {/*)} />*/}
                    <Route path='/' exact component={Login} />
                    <Route path='/login' exact component={Login} />
                    <Route path='/register' exact component={Register} />
                    <Route path='/forgot' exact component={Forgot} />
                    <Route path='/register_entity' exact component={RegisterEntity} />

                    <PrivateRoute path='/dashboard' loggedIn={loggedIn} exact component={Dashboard} />
                    <PrivateRoute path='/calendar' loggedIn={loggedIn} exact component={Calendar} />
                    <PrivateRoute path='/sales' loggedIn={loggedIn} exact component={Sales} />
                    {/* ========== # Start Bookings ========== */}
                    <PrivateRoute path='/bookings' loggedIn={loggedIn} exact component={Booking} />
                    <PrivateRoute path='/booking/create' loggedIn={loggedIn} exact component={BookingAddView} />
                    <PrivateRoute path='/booking/create/:client_id' loggedIn={loggedIn} exact component={BookingOrderView} />
                    <PrivateRoute path='/booking/:id' loggedIn={loggedIn} exact component={BookingView} />
                    <PrivateRoute path='/booking/:id/edit' loggedIn={loggedIn} exact component={BookingFormView} />
                    <PrivateRoute path='/bookings/:id' loggedIn={loggedIn} exact component={BookingDetail} />
                    {/* ========== # End Bookings ========== */}

                    <PrivateRoute path='/invoices' loggedIn={loggedIn} exact component={Invoices} />
                    <PrivateRoute path='/invoices/:id' loggedIn={loggedIn} exact component={Invoice} />

                    <PrivateRoute path='/messages' loggedIn={loggedIn} exact component={Messages} />
                    <PrivateRoute path='/comments' loggedIn={loggedIn} exact component={Comments} />
                    <PrivateRoute path='/staff' loggedIn={loggedIn} exact component={Staff} />
                    <PrivateRoute path='/services' loggedIn={loggedIn} exact component={Services} />
                    <PrivateRoute path='/products' loggedIn={loggedIn} exact component={Inventory} />
                    <PrivateRoute path='/inventory/products/:id' loggedIn={loggedIn} exact component={ProductSingle} />

                    {/* ========== # Start Settings ========== */}
                    <PrivateRoute path='/settings' loggedIn={loggedIn} exact component={Settings} />
                    <PrivateRoute path='/settings/company_details' loggedIn={loggedIn} exact component={CompanyDetails} />
                    <PrivateRoute path='/settings/gallery' loggedIn={loggedIn} exact component={Gallery} />
                    <PrivateRoute path='/settings/workingtimes' loggedIn={loggedIn} exact component={WorkingTime} />

                    <PrivateRoute path='/settings/taxes' loggedIn={loggedIn} exact component={Taxes} />
                    <PrivateRoute path='/settings/invoices' loggedIn={loggedIn} exact component={SettingsInvoices} />

                    <PrivateRoute path='/locations' loggedIn={loggedIn} exact component={Locations} />
                    <PrivateRoute path='/resources' loggedIn={loggedIn} exact component={Resources} />
                    <PrivateRoute path='/calendar_settings' loggedIn={loggedIn} exact component={CalendarSettings} />
                    <PrivateRoute path='/online_booking' loggedIn={loggedIn} exact component={OnlineBooking} />
                    <PrivateRoute path='/staff_notifications' loggedIn={loggedIn} exact component={StaffNotifications} />
                    <PrivateRoute path='/payment_methods' loggedIn={loggedIn} exact component={PaymentTypes} />
                    <PrivateRoute path='/discounts' loggedIn={loggedIn} exact component={Discounts} />
                    <PrivateRoute path='/sales_settings' loggedIn={loggedIn} exact component={SalesSettings} />

                    <PrivateRoute path='/client_notifications' loggedIn={loggedIn} exact component={ClientNotifications} />
                    <PrivateRoute path='/referral_sources' loggedIn={loggedIn} exact component={ReferralSources} />
                    <PrivateRoute path='/cancellation_reasons' loggedIn={loggedIn} exact component={CancellationReasons} />
                    {/* ========== # End Settings ========== */}

                    {/*<Route path='/formBuilder' exact component={FormBuilder} />*/}
                    {/*<Route path='/buttons' exact component={Buttons} />*/}
                    <Route path='*'  component={NotFound} />
                </Switch>
            </HashRouter>
        )
    }
}

const PrivateRoute = ({ component: Component, ...rest, loggedIn }) => {
    return (
        <Route {...rest} render={(props) => {return loggedIn ? <Component {...props} /> : <Redirect to='/login' /> }
        } />
    );
};

const mapStateToProps = (state) => {
    const { loggedIn } = state.auth;
    return {
        loggedIn
    };
};

export default connect(mapStateToProps, { set_token })(RouterComponent);
