import React, {Fragment} from 'react';
import {Redirect} from "react-router-dom";
import { connect } from 'react-redux';
import styles from './styles.scss';
import {
    Alert,
    Button,
    Column, ErrorPage, format_date,
    format_date_item, format_time,
    FullLoading,
    gender,
    Grid,
    PanelContainer,
    Select,
    Table
} from "../../components";
import { get_invoice_id } from '../../redux/actions';

class Invoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 4,
            changed: false,
            selectedId: 0
        }
    }
    componentDidMount() {
        const {loading, data, errors, match, get_invoice_id} = this.props;
        if(!loading && data === null && errors === null) {
            get_invoice_id(match.params.id);
        }
    }
    render() {
        const { loading, data, errors } = this.props;
        console.log('data', data);
        return (
            <PanelContainer hasContent={false} title="Invoice">
                    <Grid>
                        <Column xsSize={12} mSize={2}/>
                        <Column classNames={[styles.invoice]} xsSize={12} mSize={8}>
                            {(loading) ? <FullLoading /> : (errors !== null) ? (<ErrorPage errors={errors} />) : (data !== null) ? (
                                <Fragment>
                                    <Alert info><p><strong>REFERENCE ID: </strong> {data.order_payment.reference_id}</p></Alert>
                                    <h1 className={styles.subject}>INVOICE REPORT: {data.reference_code}</h1>
                                    {/*<p>Thursday, 20 Dec 2018, generated Thursday, 20 Dec 2018 at 16:04</p>*/}
                                    <p>{format_date(data.created_at_by_mini_format)}, generated {format_date_item(data.created_at_by_mini_format)}</p>
                                    <h1 className={styles.title}>CLIENT: </h1>
                                    <div className={styles.client}>
                                        <img src={data.user.small_avatar_url} alt={data.user.full_name} />
                                        <div className={styles.data}>
                                            <p className={styles.name}>{data.user.full_name}</p>
                                            <p className={styles.meta}>{gender(data.user.gender)}</p>
                                            {/*<p className={styles.meta}>Ali Mortazavi</p>*/}
                                        </div>
                                    </div>
                                    <div className={styles.services}>
                                        <h1 className={styles.title}>SERVICES: </h1>
                                        {_.map(data.order_items, (item, ind) => (
                                            <div className={styles.service} key={ind}>
                                                <span className={styles.code}>{ind+1}</span>
                                                <div className={styles.info}>
                                                    <h3 className={styles.name}>{item.name}</h3>
                                                    <div className={styles.meta}>
                                                        <span className={styles.employee}>With {item.item_extra_data.employee.full_name}</span>
                                                        <span className={styles.date}>At {format_date_item(item.item_extra_data.booking_detail.date + ' ' +item.item_extra_data.booking_detail.time)}</span>
                                                    </div>
                                                </div>
                                                <div className={styles.prices_column}>
                                                    {(item.discount !== 0) ? <span className={styles.old_price}>{item.discount_text}</span> : null}
                                                    <span className={styles.new_price}>{item.total}</span>
                                                </div>
                                            </div>
                                        ))}
                                    </div>
                                    <div className="payment">
                                        <h1 className={styles.title}>{(data.order_payments.length > 1) ? 'PAYMENTS' : 'PAYMENT'}</h1>
                                        <Table striped headStriped>
                                            <thead>
                                                <tr>
                                                    <th>DATE</th>
                                                    <th>PAYMENT TYPE</th>
                                                    <th>STATUS</th>
                                                    <th>PRICE</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {_.map(data.order_payments, (item, ind) => (
                                                    <Fragment key={ind}>
                                                        <tr>
                                                            <td>{format_date_item(item.created_at_by_mini_format)}</td>
                                                            <td>{item.type_text}</td>
                                                            <td>{item.status_text}</td>
                                                            <td>{item.cost}</td>
                                                        </tr>
                                                        <tr>
                                                            <th>DESCRIPTION</th>
                                                            <td colSpan="3">{item.description}</td>
                                                        </tr>
                                                    </Fragment>
                                                ))}
                                            </tbody>
                                        </Table>
                                    </div>
                                    <div className="messages">
                                        <h1 className={styles.title}>MESSAGES:</h1>
                                        <Table striped headStriped>
                                            <thead>
                                                <tr>
                                                    <th>#</th>
                                                    <th>MESSAGE</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                {_.map(data.order_messages, (item, ind) => (
                                                    <tr key={ind}>
                                                        <td>{ind+1}</td>
                                                        <td>{item.message}</td>
                                                    </tr>
                                                ))}
                                            </tbody>
                                        </Table>
                                    </div>
                                </Fragment>
                            ) : null}
                        </Column>
                    </Grid>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data } = state.invoice.single;
    return { loading, errors, data };
};

export default connect(mapStateToProps, { get_invoice_id })(Invoice);