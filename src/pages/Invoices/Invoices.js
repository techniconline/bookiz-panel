import React from 'react';
import { connect } from 'react-redux';
import styles from './styles.scss';
import {
    Column,
    format_date_item,
    FullLoading,
    Grid,
    Input,
    PanelContainer,
    Select,
    Table
} from "../../components";
import {Redirect} from "react-router-dom";
import {get_invoices, get_invoice_url} from '../../redux/actions';
import _ from "lodash";

class Invoices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 4,
            searchText: '',
            selectedId: 0
        }
    }
    componentDidMount() {
        this.props.get_invoices();
    }
    totalAmount(item) {
        let total = 0;
        _.map(item.booking_details, booking => total += booking.amount);
        return total;
    }
    render() {
        const { loading, data } = this.props;
        const times = [
            {val: 1, text: 'Today'},
            {val: 2, text: 'Yesterday'},
            {val: 3, text: 'Last 7 days'},
            {val: 4, text: 'This Month'},
            {val: 5, text: 'Last 30 days'},
            {val: 6, text: 'All time'},
            {val: 7, text: 'Custom rang'}
        ];
        const { selectedId, searchText } = this.state;
        if (selectedId !== 0) {
            return <Redirect push to={`/invoices/${selectedId}`}/>;
        }
        return (
            <PanelContainer title="Invoices">
                {(loading) ? <FullLoading /> : (
                    <Grid classNames={[styles.invoices]}>
                        <Column xsSize={12}>
                            <div className={styles.filter}>
                                <Select options={times} onChange={(time)=>{}} selected={this.state.time} />
                                <Input placeholder="Search by Invoice Or Client" onChange={(searchText)=>{ this.setState({ searchText });}} value={searchText} />
                            </div>
                            <h1 className={styles.subject}>INVOICES REPORT</h1>
                            <p>Thursday, 20 Dec 2018, generated Thursday, 20 Dec 2018 at 16:04</p>
                            <Table headStriped rowLinked>
                                <thead>
                                    <tr>
                                        <th>INVOICE #</th>
                                        <th>CLIENT</th>
                                        <th>STATUS</th>
                                        <th>INVOICE DATE</th>
                                        <th>GROSS TOTAL</th>
                                    </tr>
                                </thead>
                                <tbody>
                                {_.map(data, (item, key) => (
                                    <tr key={key} onClick={()=>{ this.props.get_invoice_url(item.order.single_url); this.setState({ selectedId: item.id })}}>
                                        <td>{item.order.reference_code}</td>
                                        <td>{item.user.full_name}</td>
                                        <td>{item.reservation_status_text}</td>
                                        <td>{format_date_item(item.created_date)}</td>
                                        <td>{this.totalAmount(item)}</td>
                                    </tr>
                                ))}
                                </tbody>
                            </Table>
                        </Column>
                    </Grid>
                )}
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data, pagination } = state.invoice;
    return { loading, errors, data, pagination };
};

export default connect(mapStateToProps, { get_invoices, get_invoice_url })(Invoices);