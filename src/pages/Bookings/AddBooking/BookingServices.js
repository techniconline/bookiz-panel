import React, { Component } from 'react';
import { connect } from 'react-redux';
import { get_all_services, on_service_cart } from '../../../redux/actions';
import styles from "../styles.scss";
import {FullLoading} from "../../../components";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faCheck } from "@fortawesome/free-solid-svg-icons";

class BookingServices extends Component {
    constructor(props) {
        super(props);
        this.state = {
            added: []
        }
    }
    componentDidMount() {
        this.props.get_all_services();
    }
    componentDidUpdate() {
        if(this.props.refs)
            this.props.refs({ ...this.state });
    }
    onAdd(service) {
        let { added } = this.state;
        const ind = added.indexOf(service.id);
        if(ind > -1) {
            added.splice(ind, 1);
        }else {
            added.push(service.id);
        }
        this.setState({ added });
    }
    render() {
        return (this.props.loading) ? <FullLoading/> : (
                <div className={styles.add_booking_services}>
                    {_.map(this.props.data, (item, key) => {
                        if(item.entity_relation_services.length === 0)
                            return null;
                        return (
                            <div className={styles.list} key={key}>
                                <h4>{item.title}</h4>
                                <ul>
                                    {_.map(item.entity_relation_services, (service, i) => (
                                        <li key={i} onClick={this.onAdd.bind(this, service)} >
                                            <span className={styles.check_service}>
                                                {(this.state.added.indexOf(service.id) > -1) ? <FontAwesomeIcon icon={faCheck} /> : null}
                                            </span>
                                            <span className={styles.service_title}>{service.title}</span>
                                            <span>{service.amount_text}</span>
                                        </li>
                                    ))}
                                </ul>
                            </div>
                        )
                    })}
                </div>
            )
    }
}

const mapStateToProps = state => {
    const { data, loading } = state.service;
    const { carts_id } = state.booking_order;
    return {
        data,
        loading,
        carts_id
    };
};

export default connect(mapStateToProps, { get_all_services, on_service_cart })(BookingServices);