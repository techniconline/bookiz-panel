import React, {Fragment} from 'react';
import {
    Grid,
    InputForm,
    Button,
    SelectForm,
    Checkbox,
    Column, format_date, gender, PanelContainer, FullLoading, ErrorPage, TextAreaForm, Input, Tabs, Tab, Toast
} from "../../../components";
import styles from '../styles.scss';
import Popup from "../../../components/Popup/Popup";
import {Link, Redirect} from "react-router-dom";
import { connect } from "react-redux";
import {selected_client, get_members, get_all_services, booking_carts } from "../../../redux/actions/index";
import BookingFormViewList from "../BookingFormViewList";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import { faArrowRight } from "@fortawesome/free-solid-svg-icons";
import AddBookingClient from "./AddBookingClient";
import BookingServices from "./BookingServices";

class BookingAddView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            search: '',
            note: '',
            note_entity: '',
            date: new Date(),
            data: [],
            toast_message: null,
            toast_show: false,
            search_client: ''
        }
    }
    componentDidMount() {
        this.props.get_members();
        this.props.get_all_services();
        const htmlEl = document.getElementsByTagName('html');
        htmlEl[0].classList.add(styles.full_content);
    }
    handleChange(date) {
        this.setState({ date });
    }
    // onSave() {
    //     const { date } = this.state;
    //     const data = [...this.state.data];
    //     data.pop();
    //     const formData = _.map(data, item => {
    //         const { start_time, duration, employee_id, service_title, entity_service_id, service } = item;
    //         let start = date.setHours(start_time.split('_')[0]);
    //         start = new Date(start).setMinutes(start_time.split('_')[1]);
    //         let end = date.setHours(new Date(start).getHours() + Number(duration.split('_')[0]));
    //         end = new Date(end).setMinutes(new Date(start).getMinutes() + Number(duration.split('_')[1]));
    //         // employee_id: entity_employee.entity_id,
    //         //     service_id: model_row.id,
    //         //     entity_service_id: model_row.entity_service_id,
    //         //     service_title: model_row.title,
    //         //     start_time: start_h + '_' + start_min,
    //         //     duration: diff_h+'_'+diff_min
    //         return {
    //             date,
    //             time: start_time,
    //             entity_employee_id: employee_id,
    //             entity_service_id,
    //             entity_relation_service_id: service,
    //             params: {
    //                 note_entity: this.state.note,
    //                 note: this.state.note
    //             },
    //         }
    //     });
    // }
    onSubmit() {
        if(this.props.client_selected !== null) {
            if(this._services.added.length > 0)
                this.props.booking_carts(this._services.added, this.props.client_selected);
            else
                this.setState({ toast_message: 'Please choose service', toast_show: true}, () => { setTimeout(() => { this.setState({ toast_show: false}); }, 4000) });
        }else {
            this.setState({ toast_message: 'Please choose a client', toast_show: true}, () => { setTimeout(() => { this.setState({ toast_show: false}); }, 4000) });
        }

    }

    renderData() {
        const { client_selected, cart_loading, cart_submit, cart_client_id } = this.props;
        if(cart_submit) return <Redirect to={`/booking/create/${cart_client_id}`} />;
        return (
            <Fragment>
                <Toast danger show={this.state.toast_show}>{this.state.toast_message}</Toast>
                <Grid>
                    <Column classNames={[styles.cols]} xsSize={12} mSize={8}>
                        <div className={styles.content}>
                            {(client_selected === null) ? <h2 className={styles.select_client}>Please Choose a client</h2> : null}
                            <BookingServices refs={e => this._services = e} />
                        </div>
                    </Column>
                    <Column classNames={[styles.cols]} xsSize={12} mSize={4}>
                        <div className={styles.sidebar}>
                            {(client_selected === null) ? <AddBookingClient /> : (client_selected.user) ? (
                                <div className={styles.user_info}>
                                    <img src={client_selected.user.small_avatar_url} title={client_selected.user.first_name} />
                                    <div className={styles.info}>
                                        <h2 className={styles.name}>{client_selected.user.first_name} {client_selected.user.last_name}</h2>
                                        <p>{gender(client_selected.user.gender)}</p>
                                    </div>
                                    <div className={styles.unselect_client} onClick={() => { this.props.selected_client(null); }}>
                                        <FontAwesomeIcon icon={faArrowRight}/>
                                    </div>
                                </div>
                            ) : (
                                <div className={styles.user_info}>
                                    <img src={client_selected.small_avatar_url} title={client_selected.full_name} />
                                    <div className={styles.info}>
                                        <h2 className={styles.name}>{client_selected.full_name} </h2>
                                    </div>
                                    <div className={styles.unselect_client} onClick={() => { this.props.selected_client(null); }}>
                                        <FontAwesomeIcon icon={faArrowRight}/>
                                    </div>
                                </div>
                            )}
                            <div className={styles.add_form_btn}>
                                <Button title="SAVE CART"
                                        loading={cart_loading}
                                        full dark
                                        onPress={this.onSubmit.bind(this)} />
                            </div>
                        </div>
                    </Column>
                </Grid>
            </Fragment>
        )
    }
    render() {
        return (
            <PanelContainer hasContent={false} title="Booking / Create">
                <div className={styles.booking_view}>
                    {this.renderData()}
                </div>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { cart_submit, cart_client_id, cart_loading } = state.booking_order;
    return {
        client_selected: state.client.selected,
        cart_submit,
        cart_client_id,
        cart_loading,

    }
};

export default connect(mapStateToProps, { get_members, get_all_services, selected_client, booking_carts })(BookingAddView);