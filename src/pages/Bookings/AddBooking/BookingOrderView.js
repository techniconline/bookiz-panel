import React, {Fragment} from 'react';
import {
    Grid,
    Button,
    Column, gender, PanelContainer, FullLoading, ErrorPage
} from "../../../components";
import styles from '../styles.scss';
import { connect } from "react-redux";
import { get_order, get_client, on_payment } from "../../../redux/actions";
import BookingOrderServices from "./BookingOrderServices";
import {CURRENT_ENTITY} from "../../../redux/types";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";

class BookingOrderView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            search: '',
            note: '',
            note_entity: '',
            date: new Date(),
            data: [],
            search_client: '',
            payment: 'fake'
        }
    }
    componentDidMount() {
        this.props.get_order(this.props.match.params.client_id);
        this.props.get_client(this.props.match.params.client_id);
        const htmlEl = document.getElementsByTagName('html');
        htmlEl[0].classList.add(styles.full_content);
    }
    handleChange(date) {
        this.setState({ date });
    }
    onSave() {
        const { date } = this.state;
        const data = [...this.state.data];
        data.pop();
        const formData = _.map(data, item => {
            const { start_time, duration, employee_id, service_title, entity_service_id, service } = item;
            let start = date.setHours(start_time.split('_')[0]);
            start = new Date(start).setMinutes(start_time.split('_')[1]);
            let end = date.setHours(new Date(start).getHours() + Number(duration.split('_')[0]));
            end = new Date(end).setMinutes(new Date(start).getMinutes() + Number(duration.split('_')[1]));
            // employee_id: entity_employee.entity_id,
            //     service_id: model_row.id,
            //     entity_service_id: model_row.entity_service_id,
            //     service_title: model_row.title,
            //     start_time: start_h + '_' + start_min,
            //     duration: diff_h+'_'+diff_min
            return {
                date,
                time: start_time,
                entity_employee_id: employee_id,
                entity_service_id,
                entity_relation_service_id: service,
                params: {
                    note_entity: this.state.note,
                    note: this.state.note
                },
            }
        });
    }
    onSubmit() {
        let booking_details = [];
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        _.map(this._data.services, service => {
            const {item, employee_id, date, time} = service;
            const entity_relation_service = item.extera_data.entity_relation_service;
            if(item.extera_data.calendar !== null) {
                booking_details.push({
                    service: item.item_id,
                    booking_model_id: entity_id,
                    booking_model_type: 'entities',
                    model_id: entity_relation_service.id,
                    model_type: 'entity_relation_services',
                    entity_service: entity_relation_service.entity_service_id,
                    employee: employee_id,
                    date,
                    time,
                    entity: entity_id,
                    booking_calendar: item.extera_data.calendar.id,
                })
            }
        });
        const data = {
            option: {payment: this.state.payment},
            payment: 'online',
            booking_details,
            entity_client_id: this.props.match.params.client_id,
            entity_id
        };
        console.log(data);
        this.props.on_payment(data);
    }
    render() {
        const { client, client_loading, client_errors, loading, data, errors, payment_loading, payment_errors } = this.props;
        return (
            <PanelContainer hasContent={false} title="Booking / Order">
                <div className={styles.booking_view}>
                    <Grid>
                        <Column classNames={[styles.cols]} xsSize={12} mSize={8}>
                            <div className={styles.content}>
                                {(payment_errors !== null) ? <ErrorPage errors={payment_errors}/> : null}
                                <BookingOrderServices refs={e => this._data = e} />
                            </div>
                        </Column>
                        <Column classNames={[styles.cols]} xsSize={12} mSize={4}>
                            <div className={styles.sidebar}>
                                {(client_loading) ? <FullLoading /> : (client_errors) ? <ErrorPage errors={client_errors}/> : (client!==null) ? (
                                    <div className={styles.user_info}>
                                        <img src={client.user.small_avatar_url} title={client.user.first_name} />
                                        <div className={styles.info}>
                                            <h2 className={styles.name}>{client.user.first_name} {client.user.last_name}</h2>
                                            <p>{gender(client.user.gender)}</p>
                                        </div>
                                    </div>
                                ) : null }
                                <div className={styles.order_payment}>
                                    <h1>Payment</h1>
                                    {(loading) ? <FullLoading /> : (errors) ? <ErrorPage errors={errors}/> : (data !== null) ? (
                                        <ul>
                                            {_.map(data.gateways.online, (payment, key) => (
                                                <li key={key} onClick={() => { this.setState({ payment: payment.option.payment})}}>
                                                    <span className={styles.check_service}>{(this.state.payment === payment.option.payment) ? <FontAwesomeIcon icon={faCheck} /> : null}</span>
                                                    <img src={payment.option.image} alt={payment.option.name} />
                                                    <div className={styles.info}>
                                                        <p>{payment.option.name}</p>
                                                        <p>{payment.option.payment}</p>
                                                    </div>
                                                </li>
                                            ))}
                                        </ul>
                                    ) : null}
                                </div>
                                <div className={styles.add_form_btn}>
                                    <Button title="SAVE BOOKING" loading={payment_loading} full dark onPress={this.onSubmit.bind(this)} />
                                </div>
                            </div>
                        </Column>
                    </Grid>
                </div>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data, payment_loading, payment_errors } = state.booking_order;
    return {
        client: state.client.single,
        client_loading: state.client.single_loading,
        client_errors: state.client.single_errors,
        loading, errors, data,
        payment_loading, payment_errors
    }
};

export default connect(mapStateToProps, { get_order, get_client, on_payment })(BookingOrderView);