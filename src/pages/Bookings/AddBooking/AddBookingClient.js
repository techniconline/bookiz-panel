import React, { Component } from 'react';
import { connect } from 'react-redux';
import styles from "../styles.scss";
import {Button, ErrorPage, FullLoading, gender, InputForm, Input, Tab, Tabs} from "../../../components";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faSearch} from "@fortawesome/free-solid-svg-icons";
import { selected_client, get_clients, search_clients, save_client } from '../../../redux/actions';

class AddBookingClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            search_my_client: '',
            search_client: '',
            connector: '',
            first_name: '',
            last_name: '',
            password: '',
        }
    }
    componentDidMount() {
        this.props.get_clients();
        // if(this.props.data !== null) {
        //     const {
        //         first_name,
        //         last_name,
        //         username,
        //     } = this.props.data.user;
        //     this.setState({
        //         first_name,
        //         last_name,
        //         connector: username,
        //     });
        // }
    }
    onSearch() {
        this.props.search_clients(this.state.search_client);
    }
    onSearchMyClient(search_my_client) {
        this.props.get_clients(search_my_client);
        this.setState({ search_my_client });
    }
    onCreate() {
        const { first_name, last_name, password, connector } = this.state;
        const body = { first_name, last_name, password, connector };
        // if(this.props.data !== null) body['entity_employee_id'] = this.props.data.id;
        this.props.save_client(body);
    }
    render(){
        const { connector, first_name, last_name, password } = this.state;
        const { data, loading, errors, search_data, search_loading, search_errors} = this.props;
        return (
            <div className={styles.client_container}>
                <Tabs full>
                    <Tab heading="My Clients">
                        <div className={styles.search_client}>
                            <Input placeholder="Search Client (Name | Email | Mobile)"
                                   value={this.state.search_my_client}
                                   onChange={this.onSearchMyClient.bind(this)} />
                        {(loading) ? <FullLoading /> : (errors) ? <ErrorPage errors={errors} /> :
                            (data.length === 0) ? (<ErrorPage errors={[['Client Not found']]} />) : (
                            <ul className={styles.clients_list}>
                                {_.map(data, (client_item, key) => (
                                    <li key={key} onClick={() => { this.props.selected_client(client_item); }}>
                                        <img src={client_item.user.small_avatar_url} alt={client_item.user.first_name} />
                                        <div className={styles.info}>
                                            <h4 className={styles.name}>{client_item.user.first_name} {client_item.user.last_name}</h4>
                                            <p>{gender(client_item.user.gender)}</p>
                                        </div>
                                    </li>
                                ))}
                            </ul>
                        )}
                        </div>
                    </Tab>
                    <Tab heading="Find Client">
                        <div className={styles.search_client}>
                            <InputForm labelText="Find Client"
                                       placeholder="Search by mobile or email"
                                       value={this.state.search_client}
                                       onChange={(search_client) => { this.setState({ search_client }); }} />
                            <Button full dark onPress={this.onSearch.bind(this)}>
                                SEARCH <FontAwesomeIcon style={{fontSize: 16}} icon={faSearch} />
                            </Button>
                        </div>
                        {(search_loading) ? <FullLoading /> : (search_errors) ? <ErrorPage errors={errors} /> :
                            (search_data.length === 0) ? (<ErrorPage errors={[['Client Not found']]} />) : (
                                <ul className={styles.clients_list}>
                                    {_.map(search_data, (client_item, key) => (
                                        <li key={key} onClick={() => { this.props.selected_client(client_item); }}>
                                            <img src={client_item.small_avatar_url} alt={client_item.full_name} />
                                            <div className={styles.info}>
                                                <h4 className={styles.name}>{client_item.full_name}</h4>
                                            </div>
                                        </li>
                                    ))}
                                </ul>
                            )}
                    </Tab>
                    <Tab heading="Add New Client">
                        <div className={styles.search_client}>
                            <InputForm id="connector"
                                       onChange={(connector)=>{ this.setState({ connector })}}
                                       value={connector}
                                       type="text"
                                       labelText="Email OR Mobile"  />
                            <InputForm id="firstName"
                                       onChange={(first_name)=>{ this.setState({ first_name })}}
                                       value={first_name}
                                       type="text"
                                       labelText="First Name"  />
                            <InputForm id="lastName"
                                       onChange={(last_name)=>{ this.setState({ last_name })}}
                                       value={last_name}
                                       type="text"
                                       labelText="Last Name"  />
                            <InputForm id="password"
                                       onChange={(password)=>{ this.setState({ password })}}
                                       value={password}
                                       type="text"
                                       labelText="Password"  />
                            <Button full dark loading={this.props.save_loading} title="CREATE CLIENT" onPress={this.onCreate.bind(this)} />
                        </div>
                    </Tab>
                </Tabs>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        data,
        loading,
        errors,
        search_data,
        search_loading,
        search_errors,
        save_loading,
        save_errors,
    } = state.client;
    return {
        data,
        loading,
        errors,
        search_data,
        search_loading,
        search_errors,
        save_loading,
        save_errors,
    };
};

export default connect(mapStateToProps, { selected_client, get_clients, search_clients, save_client })(AddBookingClient);