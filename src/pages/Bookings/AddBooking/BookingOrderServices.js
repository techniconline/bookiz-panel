import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {DatePickerForm, ErrorPage, FullLoading, SelectForm, date_to_string} from "../../../components";
import styles from "../styles.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCheck} from "@fortawesome/free-solid-svg-icons";

class BookingOrderServices extends Component{
    constructor(props) {
        super(props);
        this.state = {
            set_data: false,
            services_op: {}
        }
    }
    componentDidUpdate() {
        if(this.props.refs) this.props.refs({ services: this.state.services_op });
        if(!this.state.set_data && !this.props.loading && this.props.data) {
            const services_op = {};
            _.map(this.props.data.cart_items, cart => {
                // date_to_string(new Date)
                const employees = [{ val: -1, text: 'Anyone'}];
                _.map(cart.extera_data.employees, item => {
                    employees.push({ val: item.entity_employee_id, text: item.full_name})
                });
                const dates = [{ val: 0, text: 'SELECT Date', hide: true }];
                _.map(cart.extera_data.timesheet, (item, date) => {
                    // dates.push(new Date(date))
                    dates.push({ val: String(date), text: date });
                });
                services_op[cart.id] = {
                    employee_id: 0,
                    employees,
                    dates,
                    times: [],
                    date: new Date(),
                    time: null,
                    item: cart
                }
            });
            this.setState({ set_data: true, services_op });
        }
    }
    onStaff(cart_id, id) {
        const { services_op } = this.state;
        services_op[cart_id] = { ...services_op[cart_id], employee_id: Number(id)};
        this.setState({ services_op })
    }
    onDate(cart_id, date) {
        const { services_op } = this.state;
        const times = [{ val: 0, text: 'SELECT TIME', hide: true }];
        // _.map(services_op[cart_id].item.extera_data.timesheet[date_to_string(date)] , (item, time) => {
        _.map(services_op[cart_id].item.extera_data.timesheet[date] , (item, time) => {
            // times.push(time)
            times.push({ val: String(time), text: time });
        });
        services_op[cart_id] = { ...services_op[cart_id], date, times};
        this.setState({ services_op })
    }
    onTime(cart_id, time) {
        const { services_op } = this.state;
        services_op[cart_id] = { ...services_op[cart_id], time};
        this.setState({ services_op })
    }
    renderData() {
        const { cart_items } = this.props.data;
        const { services_op } = this.state;
        return (
            <div className={styles.order_view}>
                <h1>Services</h1>
                <div className={styles.order_services}>
                    {_.map(cart_items, (cart, key) => (
                        <div key={key} className={styles.service}>
                            <div className={styles.info}>
                                <p className={styles.title}>{cart.name}</p>
                                {(cart.extera_data.calendar === null) ? <p className={styles.p_desc}>This Service Not Available. Because Not Set Any Date.</p> : null}
                                <div className={styles.options}>
                                    <div className={styles.option}>
                                        <SelectForm labelText="SELECT STAFF"
                                                    selected={services_op[cart.id].employee_id}
                                                    options={services_op[cart.id].employees}
                                                    onChange={this.onStaff.bind(this, cart.id)}
                                        />
                                    </div>
                                    <div className={styles.option}>
                                        <SelectForm labelText="Select Date"
                                                    options={services_op[cart.id].dates}
                                                    selected={services_op[cart.id].date}
                                                    onChange={this.onDate.bind(this, cart.id)}/>

                                        {/*<DatePickerForm labelText="Select Date"*/}
                                                        {/*includeDates={services_op[cart.id].dates}*/}
                                                        {/*selected={services_op[cart.id].date}*/}
                                                        {/*onChange={this.onDate.bind(this, cart.id)}*/}
                                        {/*/>*/}
                                    </div>
                                    <div className={styles.option}>
                                        <SelectForm labelText="Select Time"
                                                    options={services_op[cart.id].times}
                                                    selected={services_op[cart.id].time}
                                                    onChange={this.onTime.bind(this, cart.id)}/>
                                        {/*<DatePickerForm labelText="Select Time"*/}
                                                    {/*timeCaption="SELECT TIME"*/}
                                                    {/*includeTimes={services_op[cart.id].times}*/}
                                                    {/*showTimeSelectOnly*/}
                                                    {/*showTimeSelect*/}
                                                    {/*dateFormat="h:mm aa"*/}
                                                    {/*selected={services_op[cart.id].time}*/}
                                                    {/*onChange={this.onTime.bind(this, cart.id)}*/}
                                        {/*/>*/}
                                    </div>
                                </div>
                                {/*<div className={styles.metas}>*/}
                                    {/*<p className={styles.meta}>Staff: -</p>*/}
                                    {/*<p className={styles.meta}>Date: -</p>*/}
                                    {/*<p className={styles.meta}>Time: -</p>*/}
                                {/*</div>*/}
                            </div>
                            <div className={styles.prices}>
                                {(cart.discount_amount !== 0) ? <span className={styles.old_price}>{cart.discount_text}</span> : null}
                                <span className={styles.new_price}>{cart.cost}</span>
                            </div>
                        </div>
                    ))}
                </div>
            </div>
        )
    }
    render() {
        const { data, loading, errors } = this.props;
        return (loading) ? <FullLoading /> :
            (errors !== null) ? <ErrorPage errors={errors} /> :
                (data !== null && this.state.set_data) ? this.renderData() : null;
    }
}

const mapStateToProps = state => {
    const { loading, errors, data } = state.booking_order;
    return { loading, errors, data }
};

export default connect(mapStateToProps, null)(BookingOrderServices);