import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {
    Button,
    Column,
    ErrorPage,
    FullLoading,
    gender,
    Grid, Modal,
    PanelContainer
} from "../../components";
import styles from './styles.scss';
import RejectBooking from "./RejectBooking";
import AcceptBooking from "./AcceptBooking";
import { get_booking } from '../../redux/actions';

class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reject_modal: false,
            accept_modal: false
        }
    }
    componentDidMount() {
        this.props.get_booking(this.props.match.params.id);
    }
    renderReject() {
        const { reject_modal} = this.state;
        return (
            <Modal open={reject_modal} title="REJECT BOOKING" onClose={()=>{this.setState({ reject_modal: false })}} >
                <RejectBooking item={this.props.data} onDismiss={()=>{this.setState({ reject_modal: false })}}  />
            </Modal>
        )
    }
    renderAccept() {
        const { accept_modal } = this.state;
        return (
            <Modal open={accept_modal} title="ACCEPT BOOKING" onClose={()=>{this.setState({ accept_modal: false })}} >
                <AcceptBooking item={this.props.data} onDismiss={()=>{this.setState({ accept_modal: false })}}  />
            </Modal>
        )
    }
    renderData() {
        const {data} = this.props;
        return (
            <div className={styles.booking_page}>
                {this.renderReject()}
                {this.renderAccept()}
                <Grid>
                    <Column xsSize={12}>
                        <h3>Client Info</h3>
                        <div className={styles.client_info}>
                            <div className={styles.user_info}>
                                <img src={data.user.small_avatar_url} title={data.user.first_name} />
                                <div className={styles.info}>
                                    <h2 className={styles.name}>{data.user.full_name}</h2>
                                    <p>{gender(data.user.gender)}</p>
                                </div>
                            </div>
                            <div className={styles.actions}>
                                <Button title="REJECT" danger onPress={()=>{this.setState({ reject_modal: true })}} />
                                <Button title="ACCEPT" success onPress={()=>{this.setState({ accept_modal: true })}} />
                                <Button type="link" href="./#/clients" title="BACK TO LIST" onPress={()=>{}} />
                            </div>
                        </div>
                    </Column>
                    <Column xsSize={12} mSize={8}>
                        <h3>Services</h3>
                        <div className={styles.booking_services}>
                            {_.map(data.booking_details, (item, key) => (
                                <div key={key} className={styles.booking_service}>
                                    <div className={styles.detail}>
                                        <div className={styles.info}>
                                            <p className={styles.title}>{item.entity_relation_service.title}</p>
                                            {(item.entity_employee) ? <p className={styles.employee}>{item.entity_employee.user.full_name}</p> : null}
                                        </div>
                                        <div className={styles.prices}>
                                            {(item.entity_relation_service.price !== item.entity_relation_service.amount) ? (
                                                <span className={styles.old_price}>{item.entity_relation_service.price}</span>
                                            ) : null }
                                            <span className={styles.new_price}>{item.entity_relation_service.amount}</span>
                                        </div>
                                        <div className={styles.date_time}>
                                            <p className={styles.date}>{item.date}</p>
                                            <p>{item.time}</p>
                                        </div>
                                    </div>
                                    {(item.params_json.description !== null) ? (
                                        <p className={styles.description}>{item.params_json.description}</p>
                                    ) : null}
                                </div>
                            ))}
                        </div>
                    </Column>
                    {/*<Column xsSize={12} mSize={6}>*/}
                        {/*<h3>Note: </h3>*/}
                        {/*<TextArea value={this.state.note}*/}
                                  {/*classNames={[styles.mt_1]}*/}
                                  {/*placeholder="Description ..."*/}
                                  {/*rows={5}*/}
                                  {/*onChange={(note) => { this.setState({ note }); }} />*/}
                    {/*</Column>*/}
                </Grid>
            </div>
        )
    }
    render() {
        const {loading, errors, data} = this.props;
        return (
            <PanelContainer title="Booking / Detail">
                {(loading) ? (
                    <FullLoading />
                ) : (errors) ? <ErrorPage errors={errors} /> : (data !== null) ? this.renderData() : null}
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.booking.single.loading,
        errors: state.booking.single.errors,
        data: state.booking.single.data,
    }
};

export default connect(mapStateToProps, { get_booking })(Detail);
