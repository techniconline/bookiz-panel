import React, {Fragment} from 'react';
import {
    Grid,
    InputForm,
    Button,
    SelectForm,
    Checkbox,
    Column, format_date, gender, PanelContainer, FullLoading, ErrorPage, TextAreaForm
} from "../../components";
import styles from './styles.scss';
import Popup from "../../components/Popup/Popup";
import {Link} from "react-router-dom";
import connect from "react-redux/es/connect/connect";
import {get_booking, get_members, get_all_services } from "../../redux/actions";
import BookingFormViewList from "./BookingFormViewList";
import AppointmentList from "../Calendar_backup/Appointment/AppointmentList";

class BookingFormView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            search: '',
            note: '',
            note_entity: '',
            date: new Date(),
            set_data: false,
            data: []
        }
    }
    componentDidUpdate() {
        if(!this.state.set_data && this.props.data !== null && !this.props.loading) {
            let data = [];
            const { note, note_entity } = this.props.data.params_json;
            _.map(this.props.data.booking_details, service => {
                const { entity_employee, model_row, time, end_book_time } = service;
                const start_h = Number(time.split(':')[0]);
                const start_min = Number(time.split(':')[1]);
                const end_time = end_book_time.split(" ")[1];
                const end_h = Number(end_time.split(':')[0]);
                const end_min = Number(end_time.split(':')[1]);
                const diff_time = (end_min + (end_h * 60)) - (start_min + (start_h * 60));
                const diff_h = Math.floor(diff_time / 60);
                const diff_min = diff_time % 60;
                data.push({
                    employee_id: entity_employee.entity_id,
                    service_id: model_row.id,
                    entity_service_id: model_row.entity_service_id,
                    service_title: model_row.title,
                    start_time: start_h + '_' + start_min,
                    duration: diff_h+'_'+diff_min
                });
            });
            this.setState({ data, set_data: true, note: (note) ? note : '', note_entity: (note_entity) ? note_entity : ''});
        }
    }
    componentDidMount() {
        this.props.get_members();
        this.props.get_all_services();
        this.props.get_booking(this.props.match.params.id);
        const htmlEl = document.getElementsByTagName('html');
        htmlEl[0].classList.add(styles.full_content);
    }
    handleChange(date) {
        this.setState({ date });
    }
    onSave() {
        const { date } = this.state;
        const data = [...this.state.data];
        data.pop();
        const formData = _.map(data, item => {
            const { start_time, duration, employee_id, service_title, entity_service_id, service } = item;
            let start = date.setHours(start_time.split('_')[0]);
            start = new Date(start).setMinutes(start_time.split('_')[1]);
            let end = date.setHours(new Date(start).getHours() + Number(duration.split('_')[0]));
            end = new Date(end).setMinutes(new Date(start).getMinutes() + Number(duration.split('_')[1]));
            // employee_id: entity_employee.entity_id,
            //     service_id: model_row.id,
            //     entity_service_id: model_row.entity_service_id,
            //     service_title: model_row.title,
            //     start_time: start_h + '_' + start_min,
            //     duration: diff_h+'_'+diff_min
            return {
                date,
                time: start_time,
                entity_employee_id: employee_id,
                entity_service_id,
                entity_relation_service_id: service,
                params: {
                    note_entity: this.state.note,
                    note: this.state.note
                },
            }
        });
        // this.props.add_appointment(newAppointment);
        // this.setState({ onSave: true });
    }
    renderData() {
        const { data } = this.props;
        return (
            <Fragment>
                <Grid>
                    <Column classNames={[styles.cols]} xsSize={12} mSize={8}>
                        <div className={styles.content}>
                            <BookingFormViewList onData={(data)=>{this.setState({ data })}} data={this.state.data} />
                            <div className={styles.note}>
                                <TextAreaForm labelText="Notes" placeholder="Add an booking note (visible to staff only) " value={this.state.note} onChange={(note)=>{this.setState({ note })}} />
                            </div>
                            <div className={styles.note}>
                                <TextAreaForm labelText="Entity Notes" placeholder="Add an booking note (visible to staff only) " value={this.state.note_entity} onChange={(note_entity)=>{this.setState({ note_entity })}} />
                            </div>
                            {/*{_.map(booking_list, (booking_data, key) => (*/}
                                {/*<Fragment key={key}>*/}
                                    {/*<h1 className={styles.date_booking}>{format_date(key)}</h1>*/}
                                    {/*{_.map(booking_data, (booking, ind) => (*/}
                                        {/*<div key={ind} className={styles.service}>*/}
                                            {/*<p className={styles.time}>{booking.time}</p>*/}
                                            {/*<div className={styles.service_data}>*/}
                                                {/*<div className={styles.service_info}>*/}
                                                    {/*<h3>{booking.model_row.title}</h3>*/}
                                                    {/*<p className={styles.staff}>With {(booking.entity_employee !== null) ? booking.entity_employee.user.full_name : 'Everyone'}</p>*/}
                                                {/*</div>*/}
                                                {/*<div className={styles.prices}>*/}
                                                    {/*{(booking.model_row.discount !== null) ? <p className={styles.old_price}>{booking.model_row.discount}</p> : null}*/}
                                                    {/*<p className={styles.price}>{booking.model_row.price}</p>*/}
                                                {/*</div>*/}
                                            {/*</div>*/}
                                        {/*</div>*/}
                                    {/*))}*/}
                                {/*</Fragment>*/}
                            {/*))}*/}
                        </div>
                    </Column>
                    <Column classNames={[styles.cols]} xsSize={12} mSize={4}>
                        <div className={styles.sidebar}>
                            <div className={styles.user_info}>
                                <img src={data.user.small_avatar_url} title={data.user.first_name} />
                                <div className={styles.info}>
                                    <h2 className={styles.name}>{data.user.full_name}</h2>
                                    <p>{gender(data.user.gender)}</p>
                                </div>
                            </div>
                            <div className={styles.options_btn}>
                                <Button title="SAVE BOOKING" dark onPress={this.onSave.bind(this)} />
                            </div>
                        </div>
                    </Column>
                </Grid>
            </Fragment>
        )
    }
    render() {
        const {loading, errors, data} = this.props;
        return (
            <PanelContainer hasContent={false} title="Booking / Detail">
                <div className={styles.booking_view}>
                    {(loading || !this.state.set_data) ? (
                        <FullLoading />
                    ) : (errors) ? <ErrorPage errors={errors} /> : (data !== null) ? this.renderData() : null}
                </div>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.booking.single.loading,
        errors: state.booking.single.errors,
        data: state.booking.single.data,
    }
};

export default connect(mapStateToProps, { get_booking, get_members, get_all_services })(BookingFormView);