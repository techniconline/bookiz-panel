import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {Alert, Button, TextAreaForm} from "../../components";
import styles from "./styles.scss";
import _ from 'lodash';
import {booking_reject} from "../../redux/actions";
import {PulseLoader} from "react-spinners";

class RejectBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reject_note: '',
            reason_reject: 0,
        }
    }
    componentDidUpdate() {
        const { loading, errors, data } = this.props;
        if(!loading && data !== null && errors === null) {
            this.props.onDismiss(true);
        }
    }
    onSubmit() {
        const {booking_reject, item} = this.props;
        const { reject_note } = this.state;
        booking_reject(item.id , reject_note);
    }
    renderErrors() {
        const { errors } = this.props;
        if(errors !== null) {
            return (
                <Alert danger>
                    {_.map(errors, (error, key) => <p key={key} className={styles.errorAlert}>* {error[0]}</p> )}
                </Alert>
            )
        }
        return null;
    }
    renderButton() {
        return (this.props.loading) ? (
            <Button dark>
                <PulseLoader
                    sizeUnit={"px"}
                    size={10}
                    color={'#fff'}
                    loading
                />
            </Button>
        ) : (
            <Button title="REJECT" dark onPress={this.onSubmit.bind(this)}  />
        )
    }
    render() {
        const { reject_note } = this.state;
        return (
            <Fragment>
                {this.renderErrors()}
                <TextAreaForm value={reject_note}
                              labelText="NOTE"
                              classNames={[styles.mt_1]}
                              placeholder="Note (Optional)"
                              onChange={(reject_note) => { this.setState({ reject_note }); }} />
                {this.renderButton()}
                <Button title="CANCEL" onPress={this.props.onDismiss.bind(this, false)} />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data } = state.booking.reject;
    return { loading, errors, data }
};

export default connect(mapStateToProps , { booking_reject })(RejectBooking);