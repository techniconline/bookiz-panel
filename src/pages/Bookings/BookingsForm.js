import React from 'react';
import {
    Grid,
    InputForm,
    Button,
    SelectForm,
    Checkbox,
    Column
} from "../../components";
import styles from './styles.scss';

class BookingsForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            first_name: '',
            last_name: '',
            mobile: '',
            telephone: '',
            email: '',
            sn: 2,
            gender: 1,
            source: 1,
            birth_month: 0,
            birth_day: 0,
            birth_year: 0,
            address: '',
            suburb: '',
            city: '',
            state: '',
            zip: ''
        }
    }

    render() {
        const notifications = [
            { val: 1, text: "Dont't send notifications"},
            { val: 2, text: "Email"}
        ];
        const genders = [
            { val: 1, text: 'Unknown'},
            { val: 2, text: 'Male'},
            { val: 3, text: 'Female'},
        ];
        const sources = [
            {val: 1, text: 'Select source'},
            {val: 2, text: 'Walk-In'},
        ];
        const months = [
            {val: 0, text: 'Select month'},
            {val: 1, text: 'January'},
            {val: 2, text: 'February'},
            {val: 3, text: 'March'},
            {val: 4, text: 'April'},
            {val: 5, text: 'May'},
            {val: 6, text: 'June'},
            {val: 7, text: 'July'},
            {val: 8, text: 'August'},
            {val: 9, text: 'September'},
            {val: 10, text: 'October'},
            {val: 11, text: 'November'},
            {val: 12, text: 'December'}
        ];
        const days = [{val: 0, text: 'Select day'}];
        for(let i = 1 ; i <= 31 ; i++){
            days.push({val: i, text: i})
        }
        let first_year = 1940;
        const years = [];
        while(first_year <= 2010) {
            years.push({val: first_year, text: first_year});
            first_year++;
        }
        years.reverse();
        years.unshift({val: 0, text: 'Select year'});
        return (
            <form>
                <Grid>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="First name" value={this.state.first_name} onChange={(first_name)=>{this.setState({ first_name });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="Last name" value={this.state.last_name} onChange={(last_name)=>{this.setState({ last_name });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="Mobile" value={this.state.mobile} onChange={(mobile)=>{this.setState({ mobile });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="Telephone" value={this.state.telephone} onChange={(telephone)=>{this.setState({ telephone });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <InputForm type="email" labelText="Email" value={this.state.email} onChange={(email)=>{this.setState({ email });}} />
                    </Column>
                </Grid>
                <Grid>
                    <Column xsSize={12} mSize={6}>
                        <SelectForm labelText="Gender" options={genders} selected={this.state.gender} onChange={(gender)=>{this.setState({ gender });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <SelectForm labelText="Referral source" options={sources} selected={this.state.source} onChange={(source)=>{this.setState({ source });}} />
                    </Column>
                </Grid>
                <Grid>
                    <Column xsSize={12} mSize={4}>
                        <SelectForm labelText="Birthday" options={months} selected={this.state.birth_month} onChange={(birth_month)=>{this.setState({ birth_month });}} />
                    </Column>
                    <Column xsSize={12} mSize={4}>
                        <SelectForm labelText="&nbsp;" options={days} selected={this.state.birth_day} onChange={(birth_day)=>{this.setState({ birth_day });}} />
                    </Column>
                    <Column xsSize={12} mSize={4}>
                        <SelectForm labelText="&nbsp;" options={years} selected={this.state.birth_year} onChange={(birth_year)=>{this.setState({ birth_year });}} />
                    </Column>
                </Grid>
                <Grid>
                    <Column xsSize={12}>
                        <InputForm labelText="Address" onChange={(addresss)=>{this.setState({ addresss });}} />
                    </Column>
                    <Column xsSize={12}>
                        <InputForm labelText="Suburb" onChange={(suburb)=>{this.setState({ suburb });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="City" onChange={(city)=>{this.setState({ city });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="State" onChange={(state)=>{this.setState({ state });}} />
                    </Column>
                    <Column xsSize={12}>
                        <InputForm labelText="Zip / Post Code" onChange={(zip)=>{this.setState({ zip });}} />
                    </Column>
                </Grid>
                <Grid>
                    <Column xsSize={12} >
                        <SelectForm labelText="Send notifications by" options={notifications} selected={this.state.sn} onChange={(sn)=>{this.setState({ sn });}} />
                        <div className={styles.mt_1}>
                            <Checkbox title="Accepts marketing notifications" defaultChecked />
                        </div>
                        <div className={styles.mt_1}>
                            <Button dark>Save</Button>
                            <Button onPress={this.props.onCancel} dark>Cancel</Button>
                        </div>
                    </Column>
                </Grid>
            </form>
        )
    }
}

export default BookingsForm;