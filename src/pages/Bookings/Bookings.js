import React from 'react';
import styles from './styles.scss';
import {
    Table,
    PanelContainer,
    Input,
    FAB,
    Modal,
    gender, ButtonGroup, Button, TextAreaForm
} from "../../components";
import { connect } from 'react-redux';
import { Redirect, Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { get_bookings } from '../../redux/actions';
import _ from 'lodash';
import BookingsForm from "./BookingsForm";
import RejectBooking from "./RejectBooking";
import AcceptBooking from "./AcceptBooking";

class Bookings extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            modalForm: false,
            editData: null,
            detailData: null,
            selectedId: 0,

            reject_modal: false,
            accept_modal: false,
            select_item: null,
        }
    }
    componentDidMount() {
        this.props.get_bookings();
    }
    renderAdd(){
        const { editData, modalForm } = this.state;
        return (
            <Modal open={modalForm}
                   title={(editData) ? 'Edit Booking' : 'Add Booking' }
                   full
                   onClose={()=>{this.setState({ modalForm: false, editData: null })}} >
                <BookingsForm editData={editData} onCancel={()=>{this.setState({ modalForm: false, editData: null })}} />
            </Modal>
        )
    }
    renderReject() {
        const { reject_modal, select_item } = this.state;
        return (
            <Modal open={reject_modal}
                   title={"REJECT BOOKING " + ((select_item !== null) ? '#'+select_item.id : '')}
                   onClose={()=>{this.setState({ reject_modal: false })}} >
                <RejectBooking item={select_item} onDismiss={()=>{this.setState({ reject_modal: false })}}  />
            </Modal>
        )
    }
    renderAccept() {
        const { accept_modal, select_item } = this.state;
        return (
            <Modal open={accept_modal}
                   title={"ACCEPT BOOKING " + ((select_item !== null) ? '#'+select_item.id : '')}
                   onClose={()=>{this.setState({ accept_modal: false })}} >
                <AcceptBooking item={select_item} onDismiss={()=>{this.setState({ accept_modal: false })}}  />
            </Modal>
        )
    }
    render() {
        const { loading, data, errors, pagination } = this.props;
        const { selectedId } = this.state;
        if (selectedId !== 0) {
            return <Redirect push to={`/booking/${selectedId}`}/>;
        }
        return (
            <PanelContainer hasContent={false} title="Bookings">
                {this.renderAccept()}
                {this.renderReject()}
                {this.renderAdd()}
                {/*<div className={styles.clients_search}>*/}
                    {/*<Input type="search" placeholder="Search by name, e-mail or mobile number" onChange={(search)=>{ this.setState({ search }); }} value={this.state.search} />*/}
                {/*</div>*/}
                <div className={styles.bookings}>
                    <Table striped rowLinked>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Services Count</th>
                                <th>Date & Time</th>
                                <th>Gender</th>
                                <th>Status</th>
                                <th>Created At</th>
                                {/*<th>Actions</th>*/}
                            </tr>
                        </thead>
                        <tbody>
                            {_.map(data, (item, key) => (
                                <tr key={key} onClick={()=>{ this.setState({ selectedId: item.id })}}>
                                    {/*<tr key={key} >*/}
                                    <td>#{item.id}</td>
                                    <td>{item.user.full_name}</td>
                                    <td>{item.booking_details.length}</td>
                                    <td>{item.booking_details[0].book_time}</td>
                                    <td>{gender(item.user.gender)}</td>
                                    <td>{item.reservation_status_text}</td>
                                    <td>{item.created_date}</td>
                                    {/*<td>*/}
                                        {/*<ButtonGroup classNames={[styles.actions]}>*/}
                                            {/*<Button success title="ACCEPT" onPress={()=>{this.setState({ select_item: item, accept_modal: true })}} />*/}
                                            {/*<Button danger title="REJECT" onPress={()=>{this.setState({ select_item: item, reject_modal: true })}}/>*/}
                                            {/*<Button dark title="VIEW" onPress={()=>{ this.setState({ selectedId: item.id })}}/>*/}
                                        {/*</ButtonGroup>*/}
                                    {/*</td>*/}
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                    <p className={styles.text_table}>Showing {data.length} of {pagination.total} results</p>
                </div>
                {/*<FAB dark>*/}
                    {/*<div className={styles.fab_link} onClick={()=>{this.setState({ modalForm: true })}}>*/}
                        {/*<FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />*/}
                    {/*</div>*/}
                {/*</FAB>*/}

                <FAB dark>
                    <div className={styles.fab_link}>
                        <Link to="booking/create">
                            <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                        </Link>
                    </div>
                </FAB>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data, pagination } = state.booking;
    return { loading, errors, data, pagination };
};

export default connect(mapStateToProps, { get_bookings })(Bookings);