import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {
    Button,
    format_date,
    Column,
    ErrorPage,
    FullLoading,
    gender,
    Grid,
    Modal,
    PanelContainer
} from "../../components";
import RejectBooking from "./RejectBooking";
import AcceptBooking from "./AcceptBooking";
import styles from "./styles.scss";
import {get_booking} from "../../redux/actions";
import Popup from "../../components/Popup/Popup";
import {Link} from "react-router-dom";

class BookingView extends Component {
    constructor(props) {
        super(props);
        this.state = {
            reject_modal: false,
            accept_modal: false,
            status_id: 0
        }
    }
    componentDidMount() {
        this.props.get_booking(this.props.match.params.id);
        const htmlEl = document.getElementsByTagName('html');
        htmlEl[0].classList.add(styles.full_content);
    }
    onStatus(status) {
        if (status.id === 1) this.setState({ accept_modal: true });
        else if (status.id === 2) this.setState({reject_modal: true});
        this._status_ref.onToggle(false);
    }
    renderReject() {
        const { reject_modal} = this.state;
        return (
            <Modal open={reject_modal} title="REJECT BOOKING" onClose={()=>{this.setState({ reject_modal: false })}} >
                <RejectBooking item={this.props.data} onDismiss={(e)=>{this.setState({ reject_modal: false, status_id: (e) ? 2 : 0 })}}  />
            </Modal>
        )
    }
    renderAccept() {
        const { accept_modal } = this.state;
        return (
            <Modal open={accept_modal} title="ACCEPT BOOKING" onClose={()=>{this.setState({ accept_modal: false })}} >
                <AcceptBooking item={this.props.data} onDismiss={(e)=>{this.setState({ accept_modal: false, status_id: (e) ? 1 : 0 })}}  />
            </Modal>
        )
    }
    renderData() {
        const {data} = this.props;
        let { status_id } = this.state;
        if(data.reservation_status === 1) status_id = 1;
        let booking_list = {};
        let total_price = 0;

        _.map(data.booking_details, booking => {
            let item = booking_list[booking.date];
            if(typeof item === 'undefined') item = [];
            item.push(booking);
            booking_list = { ...booking_list, [booking.date]: item};
            total_price += booking.amount;
        });

        const statuses = [
            { id: 0, title: 'NEW BOOKING' },
            { id: 1, title: 'CONFIRM' },
            { id: 2, title: 'REJECT' },
            // { id: 2, title: 'ARRIVED' },
            // { id: 3, title: 'STARTED' },
        ];
        const active_status = _.filter(statuses, s => s.id === status_id)[0];

        return (
            <Fragment>
                {this.renderReject()}
                {this.renderAccept()}
                <Grid>
                   <Column classNames={[styles.cols]} xsSize={12} mSize={8}>
                       <div className={styles.content}>
                           {_.map(booking_list, (booking_data, key) => (
                               <Fragment key={key}>
                                   <h1 className={styles.date_booking}>{format_date(key)}</h1>
                                   {_.map(booking_data, (booking, ind) => (
                                       <div key={ind} className={styles.service}>
                                           <p className={styles.time}>{booking.time}</p>
                                           <div className={styles.service_data}>
                                               <div className={styles.service_info}>
                                                   <h3>{booking.model_row.title}</h3>
                                                   <p className={styles.staff}>With {(booking.entity_employee !== null) ? booking.entity_employee.user.full_name : 'Everyone'}</p>
                                               </div>
                                               <div className={styles.prices}>
                                                   {(booking.model_row.discount !== null) ? <p className={styles.old_price}>{booking.model_row.discount}</p> : null}
                                                   <p className={styles.price}>{booking.model_row.price}</p>
                                               </div>
                                           </div>
                                       </div>
                                   ))}
                               </Fragment>
                           ))}
                           <div className={styles.total_result}>
                               <p className={styles.total_price}>{total_price}</p>
                           </div>
                       </div>
                   </Column>
                   <Column classNames={[styles.cols]} xsSize={12} mSize={4}>
                       <div className={styles.sidebar}>
                           <div className={styles.user_info}>
                               <img src={data.user.small_avatar_url} title={data.user.first_name} />
                               <div className={styles.info}>
                                   <h2 className={styles.name}>{data.user.full_name}</h2>
                                   <p>{gender(data.user.gender)}</p>
                               </div>
                           </div>
                           <div className={styles.booking_status_btn}>
                               <Popup position="bottom"
                                      disabled={active_status.id !== 0}
                                      refs={(status_ref) => { this._status_ref = status_ref; } }
                                      item={<Button title={active_status.title} />}>
                                   <ul className={styles.options_list}>
                                        {_.map(statuses, (status, key) => (status.id !== active_status.id) ? (
                                           <li key={key}><a onClick={this.onStatus.bind(this, status)}>{status.title}</a></li>
                                       ) : null)}
                                   </ul>
                               </Popup>
                           </div>
                           <div className={styles.options_btn}>
                               <Button title="CHECKOUT" dark full />
                               {/*<Button title="CHECKOUT" dark />*/}
                               {/*<Popup position="top" item={<Button title="MORE OPTIONS" />}>*/}
                                   {/*<ul className={styles.options_list}>*/}
                                       {/*<li><Link to="/booking/36/edit">EDIT BOOKING</Link></li>*/}
                                       {/*<li><a>RESCHEDULE</a></li>*/}
                                       {/*<li><a>CANCEL</a></li>*/}
                                       {/*<li><a>NO SHOW</a></li>*/}
                                   {/*</ul>*/}
                               {/*</Popup>*/}
                           </div>
                       </div>
                   </Column>
                </Grid>
            </Fragment>
        )
    }
    render() {
        const {loading, errors, data} = this.props;
        return (
            <PanelContainer hasContent={false} title="Booking / Detail">
                <div className={styles.booking_view}>
                    {(loading) ? (
                        <FullLoading />
                    ) : (errors) ? <ErrorPage errors={errors} /> : (data !== null) ? this.renderData() : null}
                </div>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.booking.single.loading,
        errors: state.booking.single.errors,
        data: state.booking.single.data,
    }
};

export default connect(mapStateToProps, { get_booking })(BookingView);