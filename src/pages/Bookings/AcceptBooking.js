import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {Alert, Button, TextAreaForm} from "../../components";
import styles from "./styles.scss";
import _ from 'lodash';
import {booking_accept} from "../../redux/actions";
import {PulseLoader} from "react-spinners";

class AcceptBooking extends Component {
    constructor(props) {
        super(props);
        this.state = {
            accept_note: ''
        }
    }
    componentDidUpdate() {
        const { loading, errors, data } = this.props;
        if(!loading && data !== null && errors === null) {
            this.props.onDismiss(true);
        }
    }
    onSubmit() {
        const {booking_accept, item} = this.props;
        const { accept_note } = this.state;
        booking_accept(item.id , accept_note);
    }
    renderErrors() {
        const { errors } = this.props;
        if(errors !== null) {
            return (
                <Alert danger>
                    {_.map(errors, (error, key) => <p key={key} className={styles.errorAlert}>* {error[0]}</p> )}
                </Alert>
            )
        }
        return null;
    }
    renderButton() {
        return (this.props.loading) ? (
            <Button dark>
                <PulseLoader
                    sizeUnit={"px"}
                    size={10}
                    color={'#fff'}
                    loading
                />
            </Button>
        ) : (
            <Button title="ACCEPT" dark onPress={this.onSubmit.bind(this)} />
        )
    }
    render() {
        const { accept_note } = this.state;
        return (
            <Fragment>
                {this.renderErrors()}
                <TextAreaForm value={accept_note}
                              labelText="NOTE"
                              classNames={[styles.mt_1]}
                              placeholder="Note (Optional)"
                              onChange={(accept_note) => { this.setState({ accept_note }); }} />

                {this.renderButton()}
                <Button title="CANCEL" onPress={this.props.onDismiss.bind(this, false)} />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data } = state.booking.accept;
    return { loading, errors, data }
};

export default connect(mapStateToProps , { booking_accept })(AcceptBooking);