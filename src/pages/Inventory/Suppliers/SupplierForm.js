import React from 'react';
import {InputForm, Button, Grid, Column, TextAreaForm, SelectForm, Checkbox} from "../../../components";
import styles from './../styles.scss';

class SupplierForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            first_name: '',
            last_name: '',
            phone: '',
            email: '',
            telephone: '',
            website: '',
            street: '',
            suburb: '',
            city: '',
            state: '',
            post_code: '',
            country: '',
        }
    }
    render() {
        return (
            <div>
                <Grid>
                    <Column xsSize={12} mSize={6}>
                        <h3>Supplier Details</h3>
                        <InputForm labelText="SUPPLIER NAME" placeholder="e.g. L'Oreal" value={this.state.name} onChange={(name)=>{ this.setState({ name }); }} />
                        <TextAreaForm labelText="SUPPLIER DESCRIPTION" placeholder="e.g. Local provider of hair products" value={this.state.name} onChange={(description)=>{ this.setState({ description }); }} />
                        <Grid classNames={[styles.gridMargin]}>
                            <Column>
                                <h3>Contact Information</h3>
                            </Column>
                            <Column xsSize={12} mSize={6}>
                                <InputForm labelText="FIRST NAME" placeholder="e.g. John" value={this.state.first_name} onChange={(first_name)=>{ this.setState({ first_name }); }} />
                            </Column>
                            <Column xsSize={12} mSize={6}>
                                <InputForm labelText="LAST NAME" placeholder="e.g. DOE" value={this.state.last_name} onChange={(last_name)=>{ this.setState({ last_name }); }} />
                            </Column>
                            <Column xsSize={12} mSize={6}>
                                <InputForm type="tel" labelText="PHONE NUMBER" value={this.state.phone} onChange={(phone)=>{ this.setState({ phone }); }} />
                            </Column>
                            <Column xsSize={12} mSize={6}>
                                <InputForm type="email" labelText="EMAIL" value={this.state.email} placeholder="mail@example.com" onChange={(email)=>{ this.setState({ email }); }} />
                            </Column>
                            <Column xsSize={12} mSize={6}>
                                <InputForm type="tel" labelText="TELEPHONE" value={this.state.telephone} onChange={(telephone)=>{ this.setState({ telephone }); }} />
                            </Column>
                            <Column xsSize={12} mSize={6}>
                                <InputForm type="url" labelText="WEBSITE" value={this.state.website} placeholder="wwww.google.con" onChange={(website)=>{ this.setState({ website }); }} />
                            </Column>
                        </Grid>
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <h3>Physical Address</h3>
                        <TextAreaForm labelText="STREET" placeholder="e.g. 12 Main Street" value={this.state.street} onChange={(street)=>{ this.setState({ street }); }} />
                        <InputForm labelText="SUBURB" value={this.state.suburb} onChange={(suburb)=>{ this.setState({ suburb }); }} />
                        <InputForm labelText="CITY" value={this.state.city} onChange={(city)=>{ this.setState({ city }); }} />
                        <InputForm labelText="STATE" value={this.state.state} onChange={(state)=>{ this.setState({ state }); }} />
                        <InputForm labelText="ZIP / POST CODE" value={this.state.post_code} onChange={(post_code)=>{ this.setState({ post_code }); }} />
                        <SelectForm labelText="COUNTRY" options={[]} value={this.state.country} onChange={(country)=>{ this.setState({ country }); }} />
                        <Checkbox title="SAME AS POSTAL ADDRESS" defaultChecked={true} />
                    </Column>
                </Grid>
                <div className={styles.mt_2}>
                    <Button dark>Save</Button>
                    <Button classNames={[styles.ml_1]} onPress={this.props.onDismiss.bind(this)} light>Cancel</Button>
                </div>
            </div>
        )
    }
}

export default SupplierForm;