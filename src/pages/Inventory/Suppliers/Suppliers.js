import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBox } from "@fortawesome/free-solid-svg-icons";
import {Button, Modal, Table} from "../../../components";
import styles from './../styles.scss';
import SupplierForm from "./SupplierForm";

class Suppliers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faBox} />
                <h1>Add Product Suppliers</h1>
                <p>Setup your product suppliers here, to keep track of details needed for purchase orders.</p>
                <Button onPress={()=>{ this.setState({ showModal: true })}} full dark>NEW SUPPLIER</Button>
            </div>
        )
    }
    renderModal(){
        return (
            <Modal big open={this.state.showModal} onClose={()=>{ this.setState({ showModal: false })}} title="Add Supplier">
                <SupplierForm onDismiss={()=>{ this.setState({ showModal: false })}} />
            </Modal>
        )
    }
    renderList() {
        return (
            <Table striped rowLinked>
                <thead>
                    <tr>
                        <th>SUPPLIER NAME</th>
                        <th>PHONE</th>
                        <th>EMAIL</th>
                        <th>PRODUCTS ASSIGNED</th>
                        <th>UPDATED AT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr onClick={()=>{ this.setState({ showModal: true })}}>
                        <td>L'Oreal</td>
                        <td>+98 21 66557070</td>
                        <td>mail@gmail.com</td>
                        <td>0</td>
                        <td>30 Dec 2018, 16:41</td>
                    </tr>
                </tbody>
            </Table>
        )
    }
    render() {
        return (
            <div className={styles.supplier}>
                {this.renderList()}
                {this.renderEmpty()}
                {this.renderModal()}
            </div>
        )
    }
}

export default Suppliers;

