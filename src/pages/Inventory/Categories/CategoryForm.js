import React from 'react';
import {InputForm, Button} from "../../../components";
import styles from './../styles.scss';

class CategoryForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }
    render() {
        return (
            <div>
                <InputForm labelText="CATEGORY NAME" placeholder="e.g. Hair Products" value={this.state.name} onChange={(name)=>{ this.setState({ name }); }} />
                <div className={styles.mt_2}>
                    <Button dark>Save</Button>
                    <Button classNames={[styles.ml_1]} onPress={this.props.onDismiss.bind(this)} light>Cancel</Button>
                </div>
            </div>
        )
    }
}

export default CategoryForm;