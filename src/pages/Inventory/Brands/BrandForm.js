import React from 'react';
import {InputForm, Button} from "../../../components";
import styles from './../styles.scss';

class BrandForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: ''
        }
    }
    render() {
        return (
            <div>
                <InputForm labelText="BRAND NAME" placeholder="e.g. L'Oreal" value={this.state.name} onChange={(name)=>{ this.setState({ name }); }} />
                <div className={styles.mt_2}>
                    <Button dark>Save</Button>
                    <Button classNames={[styles.ml_1]} onPress={this.props.onDismiss.bind(this)} light>Cancel</Button>
                </div>
            </div>
        )
    }
}

export default BrandForm;