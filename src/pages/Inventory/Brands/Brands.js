import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBox } from "@fortawesome/free-solid-svg-icons";
import {Button, Modal, Table} from "../../../components";
import styles from './../styles.scss';
import BrandForm from "./BrandForm";

class Brands extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faBox} />
                <h1>Add Product Brands</h1>
                <p>Setup your brand names here, and link them to your products. Too easy!</p>
                <Button onPress={()=>{ this.setState({ showModal: true })}} full dark>NEW BRAND</Button>
            </div>
        )
    }
    renderModal(){
        return (
            <Modal open={this.state.showModal} onClose={()=>{ this.setState({ showModal: false })}} title="Add Brand">
                <BrandForm onDismiss={()=>{ this.setState({ showModal: false })}} />
            </Modal>
        )
    }
    renderList() {
        return (
            <Table striped rowLinked>
                <thead>
                    <tr>
                        <th>BRAND NAME</th>
                        <th>PRODUCT ASSIGNED</th>
                        <th>UPDATED AT</th>
                    </tr>
                </thead>
                <tbody>
                    <tr onClick={()=>{ this.setState({ showModal: true })}}>
                        <td>ASUS</td>
                        <td>0</td>
                        <td>30 Dec 2018, 16:41</td>
                    </tr>
                </tbody>
            </Table>
        )
    }
    render() {
        return (
            <div className={styles.brands}>
                {this.renderList()}
                {this.renderEmpty()}
                {this.renderModal()}
            </div>
        )
    }
}

export default Brands;

