import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faBox, faPlus} from "@fortawesome/free-solid-svg-icons";
import {Button, FAB, format_date_item, Modal, Table} from "../../../components";
import styles from './../styles.scss';
import ProductForm from "./ProductForm";
import { get_products } from '../../../redux/actions';
import {MoonLoader} from "react-spinners";
import _ from 'lodash';

class Products extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false,
            editData: null
        }
    }
    componentDidMount() {
        this.props.get_products();
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faBox} />
                <h1>Add New Products</h1>
                <p>Have you got stuff you want to sell? No sweat - you can add your retail products here and sell them with an existing appointment or as one-off items. Too easy!</p>
                <Button onPress={()=>{ this.setState({ showModal: true, editData: null })}} full dark>NEW PRODUCT</Button>
            </div>
        )
    }
    renderModal(){
        const {editData, showModal} = this.state;
        return (
            <Modal open={showModal} onClose={()=>{ this.setState({ showModal: false })}} title={(editData!==null) ? "Edit Product": "Create Product"}>
                <ProductForm editData={editData} onDismiss={()=>{ this.setState({ showModal: false })}} />
            </Modal>
        )
    }
    renderList() {
        return (
            <Table striped rowLinked>
                <thead>
                    <tr>
                        <th>#</th>
                        <th>PRODUCT NAME</th>
                        <th>BARCODE</th>
                        <th>RETAIL PRICE</th>
                        <th>STOCK ON HAND</th>
                        <th>UPDATED AT</th>
                    </tr>
                </thead>
                <tbody>
                    {_.map(this.props.data, (product, key) => (
                        <tr key={key} onClick={()=>{ this.setState({ editData: product, showModal: true})}}>
                            <td>{product.id}</td>
                            <td>{product.title}</td>
                            <td>123ABC</td>
                            <td>
                                <span className={styles.old_price}>20$</span>
                                <span className={styles.new_price}>15$</span>
                            </td>
                            <td>{product.quantity}</td>
                            <td>{format_date_item(product.created_at)}</td>
                        </tr>
                    ))}
                    {/*<tr onClick={()=>{ this.setState({ redirectUrl: '/inventory/products/1', redirectToReferrer: true })}}>*/}
                        {/*<td>Shampoo</td>*/}
                        {/*<td>123ABC</td>*/}
                        {/*<td>*/}
                            {/*<span className={styles.old_price}>20$</span>*/}
                            {/*<span className={styles.new_price}>15$</span>*/}
                        {/*</td>*/}
                        {/*<td>6</td>*/}
                        {/*<td>30 Dec 2018, 16:41</td>*/}
                    {/*</tr>*/}
                </tbody>
            </Table>
        )
    }
    render() {
        const { loading , data} = this.props;
        return (
            <div className={styles.products}>
                {(loading) ? (
                    <div className={styles.loading}>
                        <MoonLoader
                            sizeUnit={"px"}
                            size={30}
                            color={'#24334A'}
                            loading={loading}
                        />
                    </div>
                ): (data.length === 0) ? this.renderEmpty(): this.renderList()}
                {this.renderModal()}
                <FAB dark>
                    <div className={styles.fab_link} onClick={()=>{this.setState({ showModal: true, editData: null })}}>
                        <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                    </div>
                </FAB>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.product.loading,
        errors: state.product.errors,
        data: state.product.data,
    }
};

export default connect(mapStateToProps, { get_products })(Products);

