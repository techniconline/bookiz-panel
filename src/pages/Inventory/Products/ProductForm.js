import React from 'react';
import { connect } from 'react-redux';
import {Checkbox, Column, Grid, InputForm, TextAreaForm , SelectForm, Button } from "../../../components";
import styles from './../styles.scss';
import { save_product } from '../../../redux/actions';
import {PulseLoader} from "react-spinners";

class ProductForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            on_submit: false,
            title: '',
            quantity: '',
            unit: '',
            category: '',
            brand: '',
            retail_price: '',
            special_price: '',
            tax: '',
            commission: true,
            barcode: '',
            sku: '',
            description: '',
            supply_price: '',
            initial_stock: '',
            supplier: '',
            reorder_point: '',
            reorder_qty: ''
        }
    }
    componentDidMount() {
        if(this.props.editData !== null) {
            const { title, quantity } = this.props.editData;
            this.setState({ title, quantity });
        }
    }
    componentDidUpdate() {
        console.log(this.props, this.state.on_submit)
        if(this.state.on_submit && !this.props.loading) {
            this.props.onDismiss();
        }
    }
    onSubmit() {
        const { title, quantity } = this.state;
        this.props.save_product({ title, quantity });
        setTimeout(() => {
            this.setState({ on_submit: true });
        }, 100);
    }
    render() {
        const {loading, errors} = this.props;
        const categories = [
            {val: 1, text: 'category 1'}
        ];
        const taxes = [
            {val: 1, text: 'No tax'},
            {val: 2, text: 'TaxName 1%'}
        ];
        const suppliers = [
            {val: 1, text: "Select supplier"},
            {val: 2, text: "L'Oreal"},
            {val: 3, text: "ASUS"}
        ];
        return (
            <div>
                <Grid>
                    <Column xsSize={12}>
                        <h3>Product Details</h3>
                        <InputForm labelText="PRODUCT TITLE" placeholder="e.g. Large Shampoo" value={this.state.title} onChange={(title)=>{ this.setState({ title }); }} />
                        <InputForm type="number" labelText="PRODUCT QUANTITY" placeholder="e.g. NUMBER" value={this.state.quantity} onChange={(quantity)=>{ this.setState({ quantity }); }} />
                        {/*<SelectForm labelText="CATEGORY" selected={this.state.category} options={[{val: 1, text: 'category 1'}]} onChange={(category)=>{ this.setState({ category }); }} />*/}
                        {/*<SelectForm labelText="BRAND" selected={this.state.brand} options={categories} onChange={(brand)=>{ this.setState({ brand }); }} />*/}
                        {/*<Grid classNames={[styles.gridMargin]}>*/}
                            {/*<Column>*/}
                                {/*<h3>Enable Retail Sales</h3>*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm type="tel" labelText="RETAIL PRICE" placeholder="0.00" value={this.state.retail_price} onChange={(retail_price)=>{ this.setState({ retail_price }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm type="tel" labelText="SPECIAL PRICE" placeholder="0.00" value={this.state.special_price} onChange={(special_price)=>{ this.setState({ special_price }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<SelectForm labelText="TAX" selected={this.state.tax} options={taxes} onChange={(tax)=>{ this.setState({ tax }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column classNames={[styles.mt_2]}>*/}
                                {/*<Checkbox title="Enable commission" defaultChecked onChange={(commission)=>{this.setState({ commission })}} />*/}
                            {/*</Column>*/}
                        {/*</Grid>*/}
                    </Column>
                    {/*<Column xsSize={12} mSize={6}>*/}
                        {/*<Grid classNames={[styles.gridMargin]}>*/}
                            {/*<Column>*/}
                                {/*<h3>Enable Retail Sales</h3>*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm labelText="BARCODE" placeholder="e.g. 123ABC" value={this.state.barcode} onChange={(barcode)=>{ this.setState({ barcode }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm labelText="SKU" placeholder="e.g. 123ABC" value={this.state.sku} onChange={(sku)=>{ this.setState({ sku }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} >*/}
                                {/*<TextAreaForm rows={5} labelText="DESCRIPTION" placeholder="e.g. the world's most spectacular product" value={this.state.description} onChange={(description)=>{ this.setState({ description }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column>*/}
                                {/*<h3>Enable Stock Control</h3>*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm type="tel" labelText="SUPPLY PRICE" placeholder="0.00" value={this.state.supply_price} onChange={(supply_price)=>{ this.setState({ supply_price }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm type="tel" labelText="INITIAL STOCK" placeholder="0.00" value={this.state.initial_stock} onChange={(initial_stock)=>{ this.setState({ initial_stock }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12}>*/}
                                {/*<SelectForm labelText="SUPPLIER" selected={this.state.supplier} options={suppliers} onChange={(supplier)=>{ this.setState({ supplier }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm type="tel" labelText="REORDER POINT" placeholder="0" value={this.state.reorder_point} onChange={(reorder_point)=>{ this.setState({ reorder_point }); }} />*/}
                            {/*</Column>*/}
                            {/*<Column xsSize={12} mSize={6}>*/}
                                {/*<InputForm type="tel" labelText="REORDER QTY." placeholder="0.00" value={this.state.reorder_qty} onChange={(reorder_qty)=>{ this.setState({ reorder_qty }); }} />*/}
                            {/*</Column>*/}
                        {/*</Grid>*/}
                    {/*</Column>*/}
                </Grid>
                <div className={styles.mt_2}>
                    {(loading) ? (
                        <Button type="button" dark classNames={[styles.btnLoading]}>
                            <PulseLoader sizeUnit={"px"} size={9} color={'#fff'} loading />
                        </Button>) : <Button dark onPress={this.onSubmit.bind(this)}>Save</Button>}
                    <Button classNames={[styles.ml_1]} onPress={this.props.onDismiss.bind(this)} light>Cancel</Button>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.product.save_loading,
        errors: state.product.save_errors,
    }
};

export default connect(mapStateToProps, { save_product })(ProductForm);