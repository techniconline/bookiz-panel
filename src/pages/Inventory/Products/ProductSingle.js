import React from 'react';
import {Button, Grid, Modal, PanelContainer, Table} from "../../../components";
import styles from './../styles.scss';
import ProductForm from "./ProductForm";

class ProductSingle extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }
    renderModal(){
        return (
            <Modal big open={this.state.showModal} onClose={()=>{ this.setState({ showModal: false })}} title="Create Product">
                <ProductForm onDismiss={()=>{ this.setState({ showModal: false })}} />
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer title="Inventory / Products / View Product">
                {this.renderModal()}
                <div className={styles.view_product}>
                    <Grid spaceBetween>
                        <h1>Shampoo</h1>
                        <Button light onPress={()=>{ this.setState({ showModal: true })}}>EDIT</Button>
                    </Grid>
                    <div className={[styles.divider, styles.mt_1].join(' ')} />
                    <Grid>
                        <div className={styles.list_box}>
                            <div className={styles.item}>
                                <span className={styles.label}>Barcode</span>
                                <span className={styles.value}>123ABC</span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>SKU</span>
                                <span className={styles.value}>987CBA</span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>Retail Price</span>
                                <span className={styles.value}>
                                    <span className={styles.old_price}>20$</span>
                                    <span className={styles.new_price}>15$</span>
                                </span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>Supply Price</span>
                                <span className={styles.value}>10$</span>
                            </div>
                        </div>
                        <div className={styles.list_box}>
                            <div className={styles.item}>
                                <span className={styles.label}>Total On Hand</span>
                                <span className={styles.value}>6</span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>Total Stock Cost</span>
                                <span className={styles.value}>60$</span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>Avg. Stock Cost</span>
                                <span className={styles.value}>10$</span>
                            </div>
                        </div>
                        <div className={styles.list_box}>
                            <div className={styles.item}>
                                <span className={styles.label}>Brand</span>
                                <span className={styles.value}>ASUS</span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>Category</span>
                                <span className={styles.value}>Hair Products</span>
                            </div>
                            <div className={styles.item}>
                                <span className={styles.label}>Supplier</span>
                                <span className={styles.value}>L'Oreal</span>
                            </div>
                        </div>
                    </Grid>

                    <h2>Stock History</h2>
                    <Table>
                        <thead>
                            <tr>
                                <th>TIME & DATE</th>
                                <th>STAFF</th>
                                <th>ACTION</th>
                                <th>QTY. ADJUSTED</th>
                                <th>COST PRICE</th>
                                <th>STOCK ON HAND</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>30 Dec 2018, 20:25</td>
                                <td>ahmad khorshidi</td>
                                <td>New Stock</td>
                                <td>+6</td>
                                <td>10$</td>
                                <td>6</td>
                            </tr>
                        </tbody>
                    </Table>
                </div>
            </PanelContainer>
        )
    }
}

export default ProductSingle;