import React from 'react';
import {PanelContainer, Tab, Tabs} from "../../components";
import styles from './styles.scss';
import Products from "./Products/Products";
import Orders from "./Orders/Orders";
import Brands from "./Brands/Brands";
import Categories from "./Categories/Categories";
import Suppliers from "./Suppliers/Suppliers";

class Inventory extends React.Component {
    render() {
        return (
            <PanelContainer title="Products">
                <div className={styles.inventory}>
                    <Products />
                    {/*<Tabs>*/}
                        {/*<Tab heading="PRODUCTS">*/}
                            {/*<Products />*/}
                        {/*</Tab>*/}
                        {/*<Tab heading="ORDERS">*/}
                            {/*<Orders />*/}
                        {/*</Tab>*/}
                        {/*<Tab heading="BRANDS">*/}
                            {/*<Brands/>*/}
                        {/*</Tab>*/}
                        {/*<Tab heading="CATEGORIES">*/}
                            {/*<Categories />*/}
                        {/*</Tab>*/}
                        {/*<Tab heading="SUPPLIERS">*/}
                            {/*<Suppliers />*/}
                        {/*</Tab>*/}
                    {/*</Tabs>*/}
                </div>
            </PanelContainer>
        )
    }
}

export default Inventory;