import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faBox } from "@fortawesome/free-solid-svg-icons";
import {Button, Modal} from "../../../components";
import styles from './../styles.scss';
import OrderForm from "./OrderForm";

class Orders extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showModal: false
        }
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faBox} />
                <h1>Manage Stock Orders</h1>
                <p>Send purchase orders directly to your suppliers and effortlessly receive new stock deliveries</p>
                <Button onPress={()=>{ this.setState({ showModal: true })}} full dark>NEW ORDER</Button>
            </div>
        )
    }
    renderModal(){
        return (
            <Modal open={this.state.showModal} onClose={()=>{ this.setState({ showModal: false })}} title="New Service">
                <OrderForm onDismiss={()=>{ this.setState({ showModal: false })}} />
            </Modal>
        )
    }
    render() {
        return (
            <div className={styles.orders}>
                {this.renderEmpty()}
                {this.renderModal()}
            </div>
        )
    }
}

export default Orders;

