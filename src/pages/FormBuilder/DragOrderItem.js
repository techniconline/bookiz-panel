import React from 'react';
import { findDOMNode } from 'react-dom';
import { DragSource, DropTarget } from 'react-dnd';
import flow from 'lodash/flow';
import styles from './styles.scss';

const cardSource = {
    beginDrag(props) {
        return {
            id: props.id,
            index: props.index,
            originalIndex: props.index
        };
    }
};

const cardTarget = {
    hover(props, monitor, component) {
        if (!component) {
            return null
        }
        const dragIndex = monitor.getItem().index;
        const hoverIndex = props.index

        // Don't replace items with themselves
        if (dragIndex === hoverIndex) {
            return
        }

        // Determine rectangle on screen
        const hoverBoundingRect = findDOMNode(
            component,
        ).getBoundingClientRect();

        // Get vertical middle
        const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2

        // Determine mouse position
        const clientOffset = monitor.getClientOffset()

        // Get pixels to the top
        const hoverClientY = (clientOffset).y - hoverBoundingRect.top

        // Only perform the move when the mouse has crossed half of the items height
        // When dragging downwards, only move when the cursor is below 50%
        // When dragging upwards, only move when the cursor is above 50%

        // Dragging downwards
        if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
            return
        }

        // Dragging upwards
        if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
            return
        }

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        monitor.getItem().index = hoverIndex;

        // Time to actually perform the action
       return props.moveCard(dragIndex, hoverIndex)

        // Note: we're mutating the monitor item here!
        // Generally it's better to avoid mutations,
        // but it's good here for the sake of performance
        // to avoid expensive index searches.
        // monitor.getItem().index = hoverIndex
    },
};

class DragOrderItem extends React.Component {
    render() {
        const {isDragging, connectDragSource, connectDropTarget} = this.props;

        const opacity = isDragging ? 0 : 1;
        const display = isDragging ? 'none' : 'block';

        return connectDropTarget &&
            connectDragSource &&
            connectDragSource(
            connectDropTarget(
                <div className={styles.dragOrderItem} style={{ opacity, display }}>
                    {this.props.children}
                </div>
            )
        );
    }
}

export default flow(
    DropTarget('card', cardTarget, connect => ({
        connectDropTarget: connect.dropTarget()
    })),
    DragSource('card', cardSource, (connect, monitor) => ({
        connectDragSource: connect.dragSource(),
        isDragging: monitor.isDragging()
    }))
)(DragOrderItem);