import React from 'react';
import { DropTarget } from 'react-dnd';
import styles from './styles.scss';

const collect = (connect, monitor) => {
    return {
        connectDropTarget: connect.dropTarget(),
        hovered: monitor.isOver(),
        item: monitor.getItem()
    }
};

class FBTarget extends React.Component {
    render() {
        const { connectDropTarget, hovered, item } = this.props;
        const backgroundColor = hovered ? 'lightgreen' : 'white';

        return connectDropTarget(
            <div className={styles.dndTarget} style={{backgroundColor}}>
                {this.props.children}
            </div>
        );
    }
}

export default DropTarget('item', {}, collect)(FBTarget);





