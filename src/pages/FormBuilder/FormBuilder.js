import React from 'react';
import { DragDropContext } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';
import uuid  from 'uuid';
import update from 'immutability-helper';

import styles from './styles.scss';
import {PanelContainer} from "../../components/Panel/Panel";
import FBItem from './FBItem';
import FBTarget from "./FBTarget";
import DragOrderItem from './DragOrderItem';
import {Checkbox, Input, InputForm, Radio, TextArea, Button} from "../../components";

class FormBuilder extends React.Component {
    state = {
        form: []
    };

    addItemToForm(item) {
        this.setState(prevState => {
            prevState.form.push({ ...item, id: uuid() });
            return { form: prevState.form };
        });
    }

    addMoreOption(id) {
        this.setState(prevState => {
            let form = prevState.form;
            const indexItem =  form.findIndex(item => item.id === id);
            form[indexItem].options.push({ title:'عنوان', checked: false, id: uuid() });
            return { form };
        });
    }

    onChangeTitle(text, id) {
        this.setState(prevState => {
            let form = prevState.form;
            const index =  form.findIndex(item => item.id === id);
            form[index].title = text;
            return { form };
        });
    }

    onChangeOptionTitle(text, itemId, optionId) {
        this.setState(prevState => {
            let form = prevState.form;
            const indexItem =  form.findIndex(item => item.id === itemId);
            const index = form[indexItem].options.findIndex(item => item.id === optionId);
            form[indexItem].options[index].title = text;
            return { form };
        });
    }

    deleteItemForm(id) {
        this.setState(prevState => {
            let form = prevState.form;
            const index =  form.findIndex(item => item.id === id);
            form.splice(index, 1);
            return { form };
        });
    }

    renderTargetItem(item) {
        let result = null;

        switch (item.type) {
            case 'text':
                result = <Input type={item.type} />;
                break;
            case 'password':
                result = <Input type={item.type} />;
                break;
            case 'number':
                result = <Input type={item.type} />;
                break;
            case 'date':
                result = <Input type={item.type} />;
                break;
            case 'textarea':
                result = <TextArea />;
                break;
            case 'checkbox':
                result = (
                    <div>
                        <a onClick={() =>{this.addMoreOption(item.id)}} className={styles.addMoreOption}>+</a>
                        <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexWrap: 'wrap'}}>
                            {item.options.map((option, index) => {
                                return (
                                    <div key={index}>
                                        <Input onChange={(value) => this.onChangeOptionTitle(value, item.id, option.id)} nobordered value={option.title}/>
                                        <Checkbox/>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                );
                break;
            case 'radio':
                result = (
                    <div>
                        <a onClick={() =>{this.addMoreOption(item.id)}} className={styles.addMoreOption}>+</a>
                        <div style={{ display: 'flex', justifyContent: 'flex-start', alignItems: 'center', flexWrap: 'wrap'}}>
                            {item.options.map((option, index) => {
                                return (
                                    <div key={index}>
                                        <Input onChange={(value) => this.onChangeOptionTitle(value, item.id, option.id)} nobordered value={option.title}/>
                                        <Radio/>
                                    </div>
                                )
                            })}
                        </div>
                    </div>
                );
                break;
            default:
                result = <Input labelText={item.title} type={item.type} />;
        }
        return (
            <div>
                <Input onChange={(value) => this.onChangeTitle(value, item.id) } nobordered value={item.title}  />
                {result}
                <div style={{marginTop: '1rem'}}>
                    <Button onPress={() => this.deleteItemForm(item.id) } danger title="حذف" />
                </div>
            </div>
        );
    }

    moveCard(dragIndex, hoverIndex) {
        console.log(this.state.form);
        const { form } = this.state;
        const dragCard = form[dragIndex];

        this.setState(update(this.state, {form: {
            $splice: [
                [dragIndex, 1],
                [hoverIndex, 0, dragCard]
            ]
        }}));
    }

    render() {
        return (
            <PanelContainer>
                <div className={styles.dndContainer}>
                    <div className={styles.dndItemContainer}>
                        <FBItem item={{ title: 'متن کوتاه' , type: 'text' }}  handleDrop={(item) => this.addItemToForm(item)}>
                            <InputForm labelText='متن کوتاه' disabled />
                        </FBItem>
                        <FBItem item={{  title: 'عدد' , type: 'number' }}  handleDrop={(item) => this.addItemToForm(item)}>
                            <InputForm labelText='عدد' disabled />
                        </FBItem>
                        <FBItem item={{  title: 'رمز عبور' , type: 'password' }}  handleDrop={(item) => this.addItemToForm(item)}>
                            <InputForm labelText='رمز عبور' disabled />
                        </FBItem>
                        <FBItem item={{  title: 'تاریخ', type: 'date' }}  handleDrop={(item) => this.addItemToForm(item)}>
                            <InputForm labelText='تاریخ' disabled />
                        </FBItem>
                        <FBItem item={{  title: 'متن طولانی', type: 'textarea' }}  handleDrop={(item) => this.addItemToForm(item)}>
                            <TextArea labelText='متن طولانی' disabled />
                        </FBItem>
                        <FBItem item={{  title: 'انتخاب چند از چند گزینه', type: 'checkbox', options: [{ title:'عنوان', checked: false, id: uuid() }]}}  handleDrop={(item) => this.addItemToForm(item)}>
                            <Checkbox defaultChecked title="عنوان" disabled />
                        </FBItem>
                        <FBItem item={{  title: 'انتخاب یک از چند گزینه', type: 'radio', options: [{ title:'عنوان', checked: false, id: uuid() }] }}  handleDrop={(item) => this.addItemToForm(item)}>
                            <Radio defaultChecked title="عنوان" disabled  />
                        </FBItem>
                    </div>
                    <FBTarget>
                        {(this.state.form.length > 0) ? (
                            this.state.form.map((item, index) => {
                                return (<DragOrderItem key={index} moveCard={this.moveCard.bind(this)} index={index} id={item.id} >
                                    {this.renderTargetItem(item)}
                                </DragOrderItem>)
                            })
                        ) : null}
                    </FBTarget>
                </div>
            </PanelContainer>
        )
    }
};


export default DragDropContext(HTML5Backend)(FormBuilder)
