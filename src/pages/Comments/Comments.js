import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import styles from './styles.scss';
import {ErrorPage, PanelContainer, Table} from "../../components";
import { get_comments } from '../../redux/actions';
import {MoonLoader} from "react-spinners";


class Comments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            empty: false
        }
    }
    componentDidMount() {
        this.props.get_comments();
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faEnvelope} />
                <h1>No Recent Comments</h1>
                <p>Bookiz will send automated customer notifications, reminders, confirmations and much more. Make the most of this feature by saving customer contact details in Shedul and give it a try!</p>
                <p>You can manage these features on the </p>
            </div>
        )
    }
    renderList() {
        const { data } = this.props;

        if(data.length === 0) return this.renderEmpty();
        return (
            <Table headStriped>
                <thead>
                    <tr>
                        <th>ID #</th>
                        <th>Name</th>
                        <th>Email</th>
                        <th>Comment</th>
                        <th>Created At</th>
                    </tr>
                </thead>
                <tbody>
                {_.map(data, (comment, key) => (
                    <tr key={key} >
                        <td>{comment.id}</td>
                        <td>{comment.name}</td>
                        <td>{comment.email}</td>
                        <td>{comment.comment}</td>
                        <td>{comment.created_at}</td>
                    </tr>
                ))}
                </tbody>
            </Table>
        )
    }
    render(){
        const { loading, errors, data } = this.props;
        return (
            <PanelContainer hasContent={false} title="Comments">
                <div className={styles.messages}>
                    {
                        (loading) ? (
                            <div className={styles.loading}>
                                <MoonLoader
                                    sizeUnit={"px"}
                                    size={30}
                                    color={'#24334A'}
                                    loading={loading}
                                />
                            </div>
                        ): (errors) ? <ErrorPage errors={errors} /> : (data !== null) ? this.renderList() : null
                    }
                </div>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { data, loading, errors } = state.comment;
    return { data, loading, errors }
};

export default connect(mapStateToProps, { get_comments })(Comments);