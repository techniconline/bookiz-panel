import React from 'react';
import { connect } from 'react-redux';
import {
    Column,
    date_to_string,
    ErrorPage,
    FullLoading,
    PanelContainer,
    string_format_date_time,
    Wrapper
} from "../../components";
import BigCalendar from "react-big-calendar";
import moment from "moment/moment";
import styles from './styles.scss';
import { get_calendar_bookings } from '../../redux/actions';

class CalendarComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            get_data: false,
            view: 'day',
            date: new Date()
        }
    }
    componentDidMount() {
        const today = date_to_string(new Date());
        this.props.get_calendar_bookings(today, today);
    }
    componentDidUpdate() {
        if(!this.state.get_data && !this.props.loading) {
            this.setState({ get_data: true })
        }
    }
    eventStyleGetter(e) {
        if(e.hexColor)
            return { style: { backgroundColor: '#' + e.hexColor } };
        return null;
    }
    onView(view) {
        const d = this.state.date;
        if(view === 'week') {
            const e = d;
            const s = d;
            let start_date = new Date(s.setDate(s.getDate()-(6-(6-d.getDay()))));
            let end_date = new Date(e.setDate(e.getDate()+(6-d.getDay())));
            start_date = date_to_string(start_date);
            end_date = date_to_string(end_date);
            this.props.get_calendar_bookings(start_date, end_date);
        }else {
            const date = date_to_string(d);
            this.props.get_calendar_bookings(date, date);
        }
        this.setState({ view });
    }
    onNavigate(d) {
        const { view } = this.state;
        if(view === 'week') {
            const e = d;
            const s = d;
            let start_date = new Date(s.setDate(s.getDate()-(6-(6-d.getDay()))));
            let end_date = new Date(e.setDate(e.getDate()+(6-d.getDay())));
            start_date = date_to_string(start_date);
            end_date = date_to_string(end_date);
            this.props.get_calendar_bookings(start_date, end_date);
        }else {
            const date = date_to_string(d);
            this.props.get_calendar_bookings(date, date);
        }
        this.setState({ date: d });
    }
    render() {
        const data = [];
        const { get_data } = this.state;
        const { loading, errors, bookings} = this.props;
        _.map(bookings, (item, ind) => {
            _.map(item.booking_details, (booking, i) => {
                data.push({
                    id: item.id,
                    hexColor: (ind%2 === 0) ? ((i%2 === 0) ? '1565C0' : '00bcd4') : ((i%2 === 0) ? '673ab7' : '9c27b0') ,
                    title: booking.model_row.title + ' - ' + item.user.full_name + ((booking.entity_employee !== null) ? (' (' + booking.entity_employee.user.full_name + ')') : ''),
                    start: string_format_date_time(booking.book_time),
                    end: string_format_date_time(booking.end_book_time),
                })
            })
        });
        // var futureDate = new Date(2016,9,27,10,00,00);
        const allViews =  ["week", "day"];
        const localizer = BigCalendar.momentLocalizer(moment);
        const { blocked_times } = this.state;
        const events = data.concat(blocked_times);
        return (
            <PanelContainer title="Calendar">
                {(loading && !get_data) ? <FullLoading /> :
                    (errors !== null && !get_data) ? <ErrorPage errors={errors} /> :
                        (
                            <div className={styles.calendar}>
                                {(loading) ? <div className={styles.loading}><FullLoading /></div> : null}
                                <BigCalendar
                                    events={events}
                                    eventPropGetter={this.eventStyleGetter.bind(this)}
                                    views={allViews}
                                    defaultView="day"
                                    step={30}
                                    localizer={localizer}
                                    onNavigate={this.onNavigate.bind(this)}
                                    onView={this.onView.bind(this)}
                                />
                            </div>
                        )}
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {
        bookings: state.booking.calendar.data,
        loading: state.booking.calendar.loading,
        errors: state.booking.calendar.errors,
    }
};

export default connect(mapStateToProps, { get_calendar_bookings })(CalendarComponent);