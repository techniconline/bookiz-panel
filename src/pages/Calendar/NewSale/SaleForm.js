import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCartPlus} from "@fortawesome/free-solid-svg-icons";
import {Button, Column, Grid, Input, InputForm, Modal} from "../../../components/index";
import { get_sale_items } from '../../../redux/actions';
import styles from '../styles.scss';
import SelectCheckout from "./SelectCheckout";
import {CheckoutItem} from "./ListItems";

class SaleForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            payment: 15,
            search: '',
            modalCheckout: false,
            data: []
        }
    }
    selectCheckout(item) {
        const last_data = this.state.data;
        let in_data = false;
        const data = _.map(last_data, (fItem) => {
            let result = fItem;
            if(item.id === fItem.id && item.type === fItem.type) {
                result = { ...item, quantity: Number(fItem.quantity)+1 };
                in_data = true;
            }
            return result;
        });
        if(!in_data) data.push({ ...item, quantity: 1 });
        this.setState({ modalCheckout: false, data });
    }
    renderNewCheckout() {
        return (
            <Modal no_padding height="90%" title="Select Item" onClose={()=>{ this.setState({ modalCheckout: false })}} open={this.state.modalCheckout}>
                <SelectCheckout onSelected={this.selectCheckout.bind(this)} onDismiss={()=>{ this.setState({ modalCheckout: false })}} />
            </Modal>
        )
    }
    updateItem(id, type, prop, val) {
        const data = _.map(this.state.data, item => {
            let result = item;
            if(item.id === id, item.type === type) {
                result = { ...item, [prop]: val };
            }
            return result;
        });
        this.setState({ data });
    }
    renderData() {
        return (
            <div className={styles.checkouts}>
                <div >
                    {_.map(this.state.data, (item, key) => <CheckoutItem key={key} num={key+1} item={item} setItem={this.updateItem.bind(this)} />)}
                </div>
                <Button onPress={()=>{ this.setState({ modalCheckout: true })}} full dark>ADD ITEM TO SALE</Button>
            </div>
        )
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faCartPlus} />
                <p>Your order is empty. You haven't added any items yet</p>
                <Button onPress={()=>{ this.setState({ modalCheckout: true })}} full dark>ADD ITEM TO SALE</Button>
            </div>
        )
    }
    render() {
        return (
            <div className={styles.checkout}>
                {this.renderNewCheckout()}
                <Grid end>
                    <Column xsSize={12} mSize={3}>
                        <div className={styles.sidebar}>
                            <div className={styles.search_container}>
                                <Input type="search" value={this.state.search} placeholder="Search client" onChange={(search)=>{this.setState({ search })}} />
                            </div>
                            {(this.state.data.length !== 0) ? (
                                <Fragment>
                                    <div className={styles.payment}>
                                        <InputForm labelText="Pay" type="number" value={this.state.payment} onChange={(payment)=>{this.setState({ payment })}} />
                                    </div>
                                    <div className={styles.payment_types}>
                                        <Button dark full>OTHER</Button>
                                        <Button dark full>CARD</Button>
                                        <Button dark full>CASH</Button>
                                        <Button dark full>VOUCHER</Button>
                                    </div>
                                </Fragment>
                            ) : null }
                        </div>
                    </Column>
                    <Column xsSize={12} mSize={9}>
                        <div className={styles.content}>
                            {(this.state.data.length === 0) ? this.renderEmpty() : this.renderData()}
                        </div>
                    </Column>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {

    }
};

export default connect(mapStateToProps, { get_sale_items })(SaleForm);