import React from 'react';
import styles from '../styles.scss';
import {Input, InputForm, SelectForm} from "../../../components/index";


export const ProductItem = (props) => {
    const { name, barcode, price, discount, stock, time, staff} = props.item;
    return (
        <li className={styles.item} onClick={() => { props.onClick({ ...props.item, type: props.type}) }}>
            <span>
                <span className={styles.title}>{name}</span>
                {(props.type === 'product') ? (
                    <span className={styles.subtitle}>{barcode} / {stock} in stock</span>
                ) : (props.type === 'service') ? (
                    <span className={styles.subtitle}>{time} with {staff}</span>
                ) : null}
            </span>
            {(discount !== 0) ? (
                <span className={styles.prices}>
                    <span className={styles.new}>{price - discount}$</span>
                    <span className={styles.old}>{price}$</span>
                </span>
            ) : (
                <span className={styles.prices}>
                    <span className={styles.new}>{price - discount}$</span>
                </span>
            )}
        </li>
    )
};

export const CheckoutItem = (props) => {
    console.log('props.item', props.item)
    const { id, name, barcode, price, discount, quantity, stock, type, time, staff} = props.item;
    const { item, data, detail, subtitle, num, prices, old, fields} = styles;
    const discounts = [
        {val: 0, text: 'No discount'},
        {val: 10, text: 'Something 10% off'},
        {val: 5, text: 'Special Discount 5$ off'}
    ];
    return (
        <div className={item}>
            <div className={data}>
                <div className={detail}>
                    <span className={num}>{props.num}</span>
                    <div>
                        <h3>{name}</h3>
                        {(type === 'product') ? (
                            <span className={subtitle}>{barcode} / {stock} in stock</span>
                        ) : (type === 'service') ? (
                            <span className={subtitle}>{time} with {staff}</span>
                        ) : null}
                    </div>
                </div>
                {(Number(discount) !== 0) ? (
                    <span className={prices}>
                        <span className={styles.new}>{(price*quantity) - discount}$</span>
                        <span className={old}>{(price*quantity)}$</span>
                    </span>
                ) : (
                    <span className={prices}>
                        <span className={styles.new}>{price*quantity}$</span>
                    </span>
                )}
            </div>
            <div className={fields}>
                <InputForm type="number" labelText="Quantity" value={quantity} onChange={(quantity)=>{ props.setItem(id, type, 'quantity', quantity); }} />
                <InputForm type="number" labelText="Price" value={price} disabled />
                <SelectForm labelText="Staff" options={[{val:1, text: 'ahmad khorshidi'}]}  />
                <SelectForm labelText="Discount" options={discounts} selected={discount} onChange={(discount)=>{ props.setItem(id, type, 'discount', discount); }} />
            </div>
        </div>
    )
}