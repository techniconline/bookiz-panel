import React from 'react';
import uuid from 'uuid';
import styles from '../styles.scss';
import '../style.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import {Button, Column, Grid, InputForm, SelectForm, TextAreaForm} from "../../../components";

class BlockedTimeForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            date: new Date(),
            start_time: null,
            end_time: null,
            staff: 1,
            description: '',
        };
        this.handleChange = this.handleChange.bind(this);
    }

    componentWillMount() {
        const { start, end, description, staff } = this.props.event;
        const start_time = start.getHours() + '_' + start.getMinutes();
        const end_time = end.getHours() + '_' + end.getMinutes();
        this.setState({
            date: start,
            description,
            staff,
            start_time,
            end_time
        })
    }

    handleChange(date) {
        this.setState({ date });
    }

    onSave() {
        const id = uuid();
        const { start_time, end_time, description, date, staff } = this.state;
        let start = date.setHours(start_time.split('_')[0]);
        start = new Date(start).setMinutes(start_time.split('_')[1]);
        let end = date.setHours(end_time.split('_')[0]);
        end = new Date(end).setMinutes(end_time.split('_')[1]);
        const item = {
            id,
            title: 'Blocked Time',
            type: 'blocked_time',
            hexColor: '546E7A',
            start: new Date(start),
            end: new Date(end),
            description,
            staff
        };
        this.props.onSave(item, this.props.isEdit);
    }

    render() {
        const { date, start_time, end_time, staff, description } = this.state;
        const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        const day = days[date.getDay()];
        const month = months[date.getMonth()];
        const dateText = `${day}, ${date.getDate()} ${month} ${date.getFullYear()}`;
        const staffs = [
            { val: 1, text: 'Wendy Smith'},
            { val: 2, text: 'Ahmad Khorshidi'},
        ];
        const times = [];
        for(let hour = 0; hour < 24 ; hour++) {
            for(let min = 0; min < 60 ; min=min+5) {
                const text = ((hour < 10) ? '0' + hour : hour) + ':' + ((min < 10) ? '0' + min : min);
                const val = hour + '_' + min;
                times.push({val ,text})
            }
        }

        return (
            <div className={styles.pv_1}>
                <Grid>
                    <Column xsSize={12}>
                        <InputForm onClick={()=>{ this._calendar.setOpen(true) }} labelText="Date" value={dateText} />
                        <DatePicker
                            selected={date}
                            customInput={<span />}
                            ref={(c) => this._calendar = c}
                            onChange={this.handleChange}
                            dateFormat="DD MMM YYYY"
                        />
                        <SelectForm labelText="Staff" selected={staff} options={staffs} onChange={(staff)=>{this.setState({ staff })}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <SelectForm labelText="Start time" selected={start_time} options={times} onChange={(start_time) => {this.setState({ start_time })}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <SelectForm labelText="End time" selected={end_time} options={times} onChange={(end_time) => {this.setState({ end_time })}} />
                    </Column>
                    <Column xsSize={12}>
                        <TextAreaForm labelText="Start time" value={description} placeholder="e.g. lunch meeting" onChange={(description)=>{this.setState({ description });}} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <Button dark full onPress={this.onSave.bind(this)}>SAVE</Button>
                    </Column>
                    {(this.props.isEdit) ? (
                        <Column xsSize={12} mSize={6}>
                            <Button danger full>DELETE</Button>
                        </Column>
                    ) : null }
                </Grid>
            </div>
        );
    }
}

export default BlockedTimeForm;