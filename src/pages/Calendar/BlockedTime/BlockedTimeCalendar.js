import React from 'react';
import BigCalendar from "react-big-calendar";
import moment from "moment/moment";
import styles from '../styles.scss';

class BlockedTimeCalendar extends React.Component {
    constructor(props){
        super(props);
    }
    render() {
        const allViews =  ["week", "day"];
        const localizer = BigCalendar.momentLocalizer(moment);
        return (
            <div className={styles.calendar}>
                <BigCalendar
                    events={this.props.events}
                    selectable={true}
                    onSelectSlot={this.props.onBlockedTime.bind(this)}
                    views={allViews}
                    defaultView="day"
                    step={15}
                    localizer={localizer}
                />
            </div>
        );
    }
}

export default BlockedTimeCalendar;