import React from 'react';
import styles from './styles.scss';
import {Button, Column, Grid, Input, PanelContainer, Select, Table} from "../../components/index";
import {Redirect} from "react-router-dom";

class Invoices extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 4,
            searchText: '',
            selectedId: 0
        }
    }
    render() {
        const times = [
            {val: 1, text: 'Today'},
            {val: 2, text: 'Yesterday'},
            {val: 3, text: 'Last 7 days'},
            {val: 4, text: 'This Month'},
            {val: 5, text: 'Last 30 days'},
            {val: 6, text: 'All time'},
            {val: 7, text: 'Custom rang'}
        ];
        const { selectedId, searchText } = this.state;
        if (selectedId !== 0) {
            return <Redirect push to={`/invoices/${selectedId}`}/>;
        }
        return (
            <PanelContainer title="Invoices">
                <Grid classNames={[styles.invoices]}>
                    <Column xsSize={12}>
                        <div className={styles.filter}>
                            <Select options={times} onChange={(time)=>{}} selected={this.state.time} />
                            <Input placeholder="Search by Invoice Or Client" onChange={(searchText)=>{ this.setState({ searchText });}} value={searchText} />
                        </div>
                        <h1 className={styles.subject}>INVOICES REPORT</h1>
                        <p>Thursday, 20 Dec 2018, generated Thursday, 20 Dec 2018 at 16:04</p>
                        <Table headStriped rowLinked>
                            <thead>
                                <tr>
                                    <th>INVOICE #</th>
                                    <th>CLIENT</th>
                                    <th>STATUS</th>
                                    <th>INVOICE DATE</th>
                                    <th>GROSS TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr onClick={()=>{ this.setState({ selectedId: 1 })}}>
                                    <td>6089B076</td>
                                    <td>Walk-In</td>
                                    <td>New</td>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>25.00$</td>
                                </tr>
                                <tr>
                                    <td>6089B076</td>
                                    <td>Walk-In</td>
                                    <td>New</td>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>25.00$</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Column>
                </Grid>
            </PanelContainer>
        )
    }
}

export default Invoices;