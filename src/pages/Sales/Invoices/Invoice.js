import React from 'react';
import styles from './styles.scss';
import {Button, Column, Grid, PanelContainer, Select, Table} from "../../components/index";
import {Redirect} from "react-router-dom";

class Invoice extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 4,
            changed: false,
            selectedId: 0
        }
    }
    onFilter(key, val){
        if(this.state.changed) {
            console.log('FILTER');
            this.setState({ changed: false });
        }
    }
    render() {
        const times = [
            {val: 1, text: 'Today'},
            {val: 2, text: 'Yesterday'},
            {val: 3, text: 'Last 7 days'},
            {val: 4, text: 'This Month'},
            {val: 5, text: 'Last 30 days'},
            {val: 6, text: 'All time'},
            {val: 7, text: 'Custom rang'}
        ];
        const { selectedId } = this.state;
        if (selectedId !== 0) {
            return <Redirect push to={`/invoices/${selectedId}`}/>;
        }
        return (
            <PanelContainer hasContent={false} title="Invoice">
                <Grid>
                    <Column xsSize={12} mSize={2}/>
                    <Column classNames={[styles.invoice]} xsSize={12} mSize={8}>
                        <h1 className={styles.subject}>INVOICE REPORT</h1>
                        <p>Thursday, 20 Dec 2018, generated Thursday, 20 Dec 2018 at 16:04</p>
                        <div className={styles.client}>
                            <img src="" alt=""/>
                            <div className={styles.data}>
                                <p className={styles.name}>Ali Mortazavi</p>
                                <p className={styles.meta}>Ali Mortazavi</p>
                                <p className={styles.meta}>Ali Mortazavi</p>
                            </div>
                            <div className="services">

                            </div>
                        </div>
                    </Column>
                </Grid>
            </PanelContainer>
        )
    }
}

export default Invoice;