import React from 'react';
import {PanelContainer, Tabs, Tab} from "../../components";
import styles from './styles.scss';
import DailySales from "./DailySales";
import Appointments from "./Appointments";
import Index from "../Invoices/Invoices";
import Vouchers from "./Vouchers";

class Sales extends React.Component {
    render() {
        return (
            <PanelContainer title="Sales">
                <div className={styles.sales}>
                    <Tabs>
                        <Tab heading="DAILY SALES">
                            <DailySales />
                        </Tab>
                        <Tab heading="APPOINTMENTS">
                            <Appointments />
                        </Tab>
                        <Tab heading="INVOICES">
                            <Index />
                        </Tab>
                        <Tab heading="VOUCHERS">
                            <Vouchers />
                        </Tab>
                    </Tabs>
                </div>
            </PanelContainer>
        )
    }
}

export default Sales;
