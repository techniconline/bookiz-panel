import React from 'react';
import {Column, Grid, Table} from "../../components";
import styles from './styles.scss';

class DailySales extends React.Component {
    render() {
        return (
            <div className={styles.content_tab} >
                <h1>Daily Sales: Wednesday, 19 Dec 2018</h1>
                <p>Generated Wednesday, 19 Dec 2018 at 14:04</p>
                <Grid classNames={[styles.mt_2]}>
                    <Column xsSize={12} mSize={6}>
                        <h4 className={styles.text_center}>Transaction Summary</h4>
                        <Table striped>
                            <thead>
                                <tr>
                                    <th>ITEM TYPE</th>
                                    <th>SALES QTY</th>
                                    <th>REFUND QTY</th>
                                    <th>GROSS TOTAL</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Services</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <td>Products</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <td>Vouchers</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <td>Late cancellation fees</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <td>No show fees</td>
                                    <td>0</td>
                                    <td>0</td>
                                    <td>$0</td>
                                </tr>
                                <tr>
                                    <th>Total Sales</th>
                                    <th>0</th>
                                    <th>0</th>
                                    <th>$0</th>
                                </tr>
                            </tbody>
                        </Table>
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <h4 className={styles.text_center}>Cash Movement Summary</h4>
                        <Table striped>
                            <thead>
                                <tr>
                                    <th>PAYMENT TYPE</th>
                                    <th>PAYMENTS COLLECTED</th>
                                    <th>REFUNDS PAID</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Other</td>
                                    <td>0.00$</td>
                                    <td>0.00$</td>
                                </tr>
                                <tr>
                                    <td>Card</td>
                                    <td>0.00$</td>
                                    <td>0.00$</td>
                                </tr><tr>
                                    <td>Cash</td>
                                    <td>0.00$</td>
                                    <td>0.00$</td>
                                </tr>
                                <tr>
                                    <td>Voucher Redemptions	</td>
                                    <td>0.00$</td>
                                    <td>0.00$</td>
                                </tr>
                                <tr>
                                    <th>Payments collected</th>
                                    <th>0.00$</th>
                                    <th>0.00$</th>
                                </tr>
                                <tr>
                                    <th>Of which tips</th>
                                    <th>0.00$</th>
                                    <th>0.00$</th>
                                </tr>
                                <tr>
                                    <th>Outstanding</th>
                                    <th>0.00$</th>
                                    <th />
                                </tr>
                            </tbody>
                        </Table>
                    </Column>
                </Grid>
            </div>
        )
    }
}

export default DailySales;
