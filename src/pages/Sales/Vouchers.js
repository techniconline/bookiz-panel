import React from 'react';
import styles from './styles.scss';
import {Button, Column, Grid, Select, Table} from "../../components";

class Vouchers extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            status: 1,
            changed: false
        }
    }
    onFilter(key, val){
        if(this.state.changed) {
            console.log('FILTER');
            this.setState({ changed: false });
        }
    }
    render() {
        const statuses = [
            {val: 1, text: 'All statuses'},
            {val: 2, text: 'Unpaid'},
            {val: 3, text: 'Valid'},
            {val: 4, text: 'Expired'},
            {val: 5, text: 'Redeemed'},
            {val: 6, text: 'Refunded Invoice'}
        ];
        return (
            <div className={styles.appointments}>
                <Grid>
                    <Column xsSize={12}>
                        <form className={styles.filter}>
                            <div><Select options={statuses} onChange={(status)=>{this.setState({ status, changed: true });}} selected={this.state.status} /></div>
                            <div><Button dark disabled={!this.state.changed} onPress={this.onFilter.bind(this)}>View</Button></div>
                        </form>
                        <p>Thursday, 20 Dec 2018, generated Thursday, 20 Dec 2018 at 16:04</p>
                        <Table headStriped>
                            <thead>
                                <tr>
                                    <th>ISSUE DATE</th>
                                    <th>EXPIRY DATE</th>
                                    <th>INVOICE NO.	</th>
                                    <th>CLIENT</th>
                                    <th>TYPE</th>
                                    <th>STATUS</th>
                                    <th>CODE</th>
                                    <th>TOTAL</th>
                                    <th>REDEEMED</th>
                                    <th>REMAINING</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>6089B076</td>
                                    <td>Walk-In</td>
                                    <td>Walk-In</td>
                                    <td>New</td>
                                    <td>6089B076</td>
                                    <td>25.00$</td>
                                    <td>25.00$</td>
                                    <td>25.00$</td>
                                </tr>
                                <tr>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>6089B076</td>
                                    <td>Walk-In</td>
                                    <td>Walk-In</td>
                                    <td>New</td>
                                    <td>6089B076</td>
                                    <td>25.00$</td>
                                    <td>25.00$</td>
                                    <td>25.00$</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Column>
                </Grid>
            </div>
        )
    }
}

export default Vouchers;