import React from 'react';

import styles from './styles.scss';
import {Button, Column, Grid, Select, Table} from "../../components";

class Appointments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            time: 4,
            type: 1,
            changed: false
        }
    }
    onFilter(key, val){
        if(this.state.changed) {
            console.log('FILTER');
            this.setState({ changed: false });
        }
    }
    render() {
        const types = [
            {val: 1, text: 'All staff'},
            {val: 2, text: 'Staff 1'},
            {val: 3, text: 'Staff 2'}
        ];
        const times = [
            {val: 1, text: 'Today'},
            {val: 2, text: 'Yesterday'},
            {val: 3, text: 'Last 7 days'},
            {val: 4, text: 'This Month'},
            {val: 5, text: 'Last 30 days'},
            {val: 6, text: 'Tomorrow'},
            {val: 7, text: 'Next 7 days'},
            {val: 8, text: 'Next Month'},
            {val: 9, text: 'Next 30 days'},
            {val: 10, text: 'All time'},
            {val: 11, text: 'All time'},
            {val: 12, text: 'Custom rang'}
        ];
        return (
            <div className={styles.appointments}>
                <Grid>
                    <Column xsSize={12}>
                        <form className={styles.filter}>
                            <div><Select options={times} onChange={(time)=>{this.setState({ time, changed: true });}} selected={this.state.time} /></div>
                            <div><Select options={types} onChange={(type)=>{this.setState({ type, changed: true });}} selected={this.state.type} /></div>
                            <div><Button dark disabled={!this.state.changed} onPress={this.onFilter.bind(this)}>View</Button></div>
                        </form>
                        <p>Saturday, 1 Dec 2018 to Thursday, 20 Dec 2018, all staff, generated Thursday, 20 Dec 2018 at 15:22</p>
                        <Table headStriped>
                            <thead>
                                <tr>
                                    <th>REF #</th>
                                    <th>CLIENT</th>
                                    <th>SERVICE</th>
                                    <th>DATE</th>
                                    <th>TIME</th>
                                    <th>DURATION</th>
                                    <th>STAFF</th>
                                    <th>PRICE</th>
                                    <th>STATUS</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>6089B076</td>
                                    <td>Walk-In</td>
                                    <td>Blow Dry</td>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>15:30</td>
                                    <td>1h 30min</td>
                                    <td>Staff 1</td>
                                    <td>25.00$</td>
                                    <td>New</td>
                                </tr>
                                <tr>
                                    <td>6089B076</td>
                                    <td>Walk-In</td>
                                    <td>Blow Dry</td>
                                    <td>Wednesday, 12 Dec 2018</td>
                                    <td>15:30</td>
                                    <td>1h 30min</td>
                                    <td>Staff 1</td>
                                    <td>25.00$</td>
                                    <td>New</td>
                                </tr>
                            </tbody>
                        </Table>
                    </Column>
                </Grid>
            </div>
        )
    }
}

export default Appointments;