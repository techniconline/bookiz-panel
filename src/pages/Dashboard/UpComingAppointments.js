import React, { Fragment } from 'react';
import {connect} from "react-redux";
import _ from "lodash";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faChartBar} from "@fortawesome/free-solid-svg-icons";
import {Column, Wrapper, ChartJs, FullLoading, ErrorPage} from '../../components'
import styles from './styles.scss';
import {get_dashboard_chart} from "../../redux/actions";


class UpComingAppointments extends React.Component {
    constructor(props) {
        super(props);
    }

    componentDidMount() {
        const body = {
            group_by: 'date',
            start_date: this.getDate(new Date(new Date().setDate(new Date().getDate()-30))),
            // start_date: '2019-03-05',
            end_date: this.getDate(new Date()),
            start_time: '00:00:00',
            end_time: '23:59:59',
            counter: 'amount'
        };
        this.props.get_dashboard_chart('upcoming', body);
    }
    getDate(date) {
        let month = date.getMonth()+1;
        month = (month < 10) ? '0'+month : month;
        let day = date.getDate();
        day = (day < 10) ? '0'+day : day;
        return date.getFullYear()+'-'+month+'-'+day;
    }
    render() {
        const labels = [];
        const bookingData = [];
        const { data, loading, errors } = this.props;
        _.map(data, item => {
            labels.push(item.date);
            bookingData.push(item.counter);
        });

        const recentSales = {
            type: 'bar',
            data: {
                labels,
                datasets: [
                    {
                        label: "Bookings",
                        backgroundColor: 'rgba(0, 172, 193, 0.5)',
                        borderColor: 'rgb(0, 172, 193)',
                        data: bookingData,
                    }
                ]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    xAxes: [{
                        stacked: false,
                        beginAtZero: true,
                        scaleLabel: {
                            labelString: 'Month'
                        },
                        ticks: {
                            stepSize: 1,
                            min: 0,
                            autoSkip: false
                        }
                    }],
                    yAxes: [{
                        ticks: {
                            beginAtZero:true,
                            autoSkip: false
                        }
                    }]
                }
            }
        };
        return (
            <Fragment>
                {(loading) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="RECENT BOOKINGS">
                                <div className={styles.empty_data}>
                                    <FullLoading />
                                </div>
                            </Wrapper>
                        </Column>
                    ) :
                    (errors !== null) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="RECENT BOOKINGS">
                                <div className={styles.empty_data}>
                                    <ErrorPage errors={errors} />
                                </div>
                            </Wrapper>
                        </Column>
                    ): (data.length === 0) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="UPCOMING APPOINTMENTS">
                                <div className={styles.empty_data}>
                                    <FontAwesomeIcon icon={faChartBar} />
                                    <h4>Your Schedule is Empty</h4>
                                    <p>Make some appointments for schedule data to appear</p>
                                </div>
                            </Wrapper>
                        </Column>
                    ) : (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="UPCOMING BOOKING">
                                <ChartJs data={recentSales} className={styles.chartjs}  />
                            </Wrapper>
                        </Column>
                    )}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.dashboard.upcoming.loading,
        errors: state.dashboard.upcoming.errors,
        data: state.dashboard.upcoming.data
    };
};

export default connect(mapStateToProps, { get_dashboard_chart })(UpComingAppointments);