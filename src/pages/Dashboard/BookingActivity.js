import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {Column, ErrorPage, FullLoading, format_date, format_time, Wrapper} from "../../components";
import styles from "./styles.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faClock, faTable} from "@fortawesome/free-solid-svg-icons";
import {get_bookings} from '../../redux/actions';

class BookingActivity extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.get_bookings();
    }
    render() {
        const { data, loading, errors } = this.props;
        let lenData = 0;
        return (
            <Fragment>
                {(loading) ? (
                    <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                        <Wrapper title="BOOKING ACTIVITY">
                            <div className={styles.empty_data}>
                                <FullLoading />
                            </div>
                        </Wrapper>
                    </Column>
                ) : (errors !== null) ? (
                    <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                        <Wrapper title="BOOKING ACTIVITY">
                            <div className={styles.empty_data}>
                                <ErrorPage errors={errors} />
                            </div>
                        </Wrapper>
                    </Column>
                ): (data.length === 0) ? (
                    <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                        <Wrapper title="BOOKING ACTIVITY">
                            <div className={styles.empty_data}>
                                <FontAwesomeIcon icon={faClock} />
                                <h4>Your Schedule is Empty</h4>
                                <p>Make some appointments for schedule data to appear</p>
                            </div>
                        </Wrapper>
                    </Column>
                ) : (
                    <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                        <Wrapper title="BOOKING ACTIVITY">
                            <div className={styles.wrapper}>
                                <ul className={styles.list}>
                                    {_.map(data, (item, ind) => {
                                        return _.map(item.booking_details, (service, key) => {
                                            if(lenData === 6)
                                                return null;
                                            lenData++;
                                            return (
                                                <li key={key}>
                                                    <p className={styles.title}>
                                                        <span className={styles.badge}>{item.action}</span>
                                                        {service.model_row.title}
                                                    </p>
                                                    <p className={styles.content}>
                                                        <span className={styles.date}>{format_date(service.book_time)}</span> at
                                                        <span className={styles.time}>{format_time(service.book_time)} 1h 30min</span>
                                                        {(service.entity_employee) ? <span className={styles.user}>{service.entity_employee.user.full_name}</span> : null}
                                                    </p>
                                                </li>
                                            )
                                        })
                                    })}
                                </ul>
                            </div>
                        </Wrapper>
                    </Column>
                )}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        data: state.booking.data,
        loading: state.booking.loading,
        errors: state.booking.errors,
    }
};

export default connect(mapStateToProps, { get_bookings })(BookingActivity);