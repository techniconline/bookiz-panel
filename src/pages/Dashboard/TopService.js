import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {Column, ErrorPage, FullLoading, Table, Wrapper} from "../../components";
import styles from "./styles.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faTable} from "@fortawesome/free-solid-svg-icons";
import { get_all_services } from '../../redux/actions';

class TopService extends Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.get_all_services();
    }
    render() {
        const { data, loading, errors } = this.props;

        return (
            <Fragment>
                {(loading) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="TOP SERVICES">
                                <div className={styles.empty_data}>
                                    <FullLoading />
                                </div>
                            </Wrapper>
                        </Column>
                    ) : (errors !== null) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="TOP SERVICES">
                                <div className={styles.empty_data}>
                                    <ErrorPage errors={errors} />
                                </div>
                            </Wrapper>
                        </Column>
                    ): (data.length === 0) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]}>
                            <Wrapper title="TOP SERVICES">
                                <div className={styles.empty_data}>
                                    <FontAwesomeIcon icon={faTable} />
                                    <h4>Your Schedule is Empty</h4>
                                    <p>Make some services for schedule data to appear</p>
                                </div>
                            </Wrapper>
                        </Column>
                    ) : (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]}>
                            <Wrapper title="TOP SERVICES">
                                <div className={styles.wrapper}>
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>TOP SERVICES</th>
                                                <th>PRICE</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        {_.map(data, item => {
                                            return _.map(item.entity_relation_services, (service, key) => (
                                                <tr key={key}>
                                                    <td>{service.id}</td>
                                                    <td>{service.title}</td>
                                                    <td>{service.amount_text}</td>
                                                </tr>
                                            ))
                                        })}
                                        </tbody>
                                    </Table>
                                </div>
                            </Wrapper>
                        </Column>
                    )}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.service.loading,
        errors: state.service.errors,
        data: state.service.data,
    };
};

export default connect(mapStateToProps, { get_all_services })(TopService);