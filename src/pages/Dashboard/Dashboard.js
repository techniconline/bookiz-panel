import React from 'react';
import { connect } from 'react-redux';
import {Column, Grid, PanelContainer, Wrapper, Table} from '../../components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faClock, faTable } from '@fortawesome/free-solid-svg-icons';
import { get_sales, get_appointments } from '../../redux/actions';
import styles from './styles.scss';
import RecentSales from './RecentSales';
import UpComingAppointments from './UpComingAppointments';
import TodayAppointments from './TodayAppointments';
import TopStaff from "./TopStaff";
import TopService from "./TopService";
import BookingActivity from "./BookingActivity";


class Dashboard extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            rsVal: 7
        }
    }

    componentDidMount() {
        this.recentSales();
    }

    recentSales(days = 7) {
        this.props.get_sales(days);
        this.props.get_appointments(days);
    }

    render() {
        return (
            <PanelContainer hasContent={false} title="Dashboard">
                <Grid>
                    <RecentSales />
                    <UpComingAppointments />
                    <BookingActivity />
                    <TodayAppointments />
                    <TopService />
                    <TopStaff />
                </Grid>

            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, { get_sales, get_appointments })(Dashboard);