import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {Column, ErrorPage, FullLoading, Grid, Table, Wrapper} from "../../components";
import styles from "./styles.scss";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faChartBar, faTable} from "@fortawesome/free-solid-svg-icons";
import { get_dashboard_list } from '../../redux/actions';

class TopStaff extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: [],
            set_has_data: false
        }
    }
    componentDidMount() {
        const body = {
            group_by: 'date',
            start_date: this.getDate(new Date(new Date().setDate(new Date().getDate()-30))),
            for_employee: 1,
            end_date: this.getDate(new Date()),
            start_time: '00:00:00',
            end_time: '23:59:59',
            counter: 'amount'
        };
        this.props.get_dashboard_list('top_staff', body);
    }
    componentDidUpdate() {
        if(!this.state.set_has_data && this.props.data.length > 0) {
            let data = {};
            let has_data = false;
            _.map(this.props.data, item =>{
                if(typeof item.entity_employee !== "undefined") {
                    const user = data[item.entity_employee.id];
                    if(typeof user !== "undefined") {
                        data[item.entity_employee.id] = {
                            full_name: item.entity_employee.user.first_name + ' ' + item.entity_employee.user.last_name,
                            amount: Number(user.amount) + item.counter
                        }
                    }else {
                        data[item.entity_employee.id] = {
                            full_name: item.entity_employee.user.first_name + ' ' + item.entity_employee.user.last_name,
                            amount: item.counter
                        }
                    }
                }
                has_data = true;
            });
            this.setState({ set_has_data: true, data, has_data });
        }
    }
    getDate(date) {
        let month = date.getMonth()+1;
        month = (month < 10) ? '0'+month : month;
        let day = date.getDate();
        day = (day < 10) ? '0'+day : day;
        return date.getFullYear()+'-'+month+'-'+day;
    }
    render() {
        const { data, loading, errors } = this.props;
        return (
            <Fragment>
                {(loading) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="TOP STAFF">
                                <div className={styles.empty_data}>
                                    <FullLoading />
                                </div>
                            </Wrapper>
                        </Column>
                    ) :
                    (errors !== null) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="TOP STAFF">
                                <div className={styles.empty_data}>
                                    <ErrorPage errors={errors} />
                                </div>
                            </Wrapper>
                        </Column>
                    ): (data.length === 0 || !this.state.has_data) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]}>
                            <Wrapper title="TOP STAFF">
                                <div className={styles.empty_data}>
                                    <FontAwesomeIcon icon={faTable} />
                                    <h4>Your Schedule is Empty</h4>
                                    <p>Make some Staff for schedule data to appear</p>
                                </div>
                            </Wrapper>
                        </Column>
                    ) : (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]}>
                            <Wrapper title="TOP STAFF">
                                <div className={styles.wrapper}>
                                    <Table>
                                        <thead>
                                            <tr>
                                                <th>#ID</th>
                                                <th>TOP STAFF</th>
                                                <th>THIS MONTH</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            {_.map(this.state.data, (item, key) => (
                                                <tr key={key}>
                                                    <td>{key}</td>
                                                    <td>{item.full_name}</td>
                                                    <td>{item.amount}</td>
                                                </tr>
                                            ))}
                                        </tbody>
                                    </Table>
                                </div>
                            </Wrapper>
                        </Column>
                    )}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.dashboard.staff.loading,
        errors: state.dashboard.staff.errors,
        data: state.dashboard.staff.data
    };
};

export default connect(mapStateToProps, { get_dashboard_list })(TopStaff);