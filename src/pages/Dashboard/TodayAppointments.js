import React, { Fragment } from 'react';
import {connect} from "react-redux";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faCalendarWeek} from "@fortawesome/free-solid-svg-icons";
import {Column, ErrorPage, FullLoading, string_format_date_time, date_to_string, Wrapper} from '../../components'
import styles from './styles.scss';
import moment from "moment/moment";
import BigCalendar from "react-big-calendar";
import { get_calendar_bookings } from '../../redux/actions';


class TodayAppointments extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            get_data: false,
            view: 'day',
            date: new Date()
        }
    }
    componentDidMount() {
        const today = date_to_string(new Date());
        this.props.get_calendar_bookings(today, today);
    }
    componentDidUpdate() {
        if(!this.state.get_data && !this.props.loading) {
            this.setState({ get_data: true })
        }
    }
    eventStyleGetter(e) {
        if(e.hexColor)
            return { style: { backgroundColor: '#' + e.hexColor } };
        return null;
    }
    onView(view) {
        const d = this.state.date;
        if(view === 'week') {
            const e = d;
            const s = d;
            let start_date = new Date(s.setDate(s.getDate()-(6-(6-d.getDay()))));
            let end_date = new Date(e.setDate(e.getDate()+(6-d.getDay())));
            start_date = date_to_string(start_date);
            end_date = date_to_string(end_date);
            this.props.get_calendar_bookings(start_date, end_date);
        }else {
            const date = date_to_string(d);
            this.props.get_calendar_bookings(date, date);
        }
        this.setState({ view });
    }
    onNavigate(d) {
        const { view } = this.state;
        if(view === 'week') {
            const e = d;
            const s = d;
            let start_date = new Date(s.setDate(s.getDate()-(6-(6-d.getDay()))));
            let end_date = new Date(e.setDate(e.getDate()+(6-d.getDay())));
            start_date = date_to_string(start_date);
            end_date = date_to_string(end_date);
            this.props.get_calendar_bookings(start_date, end_date);
        }else {
            const date = date_to_string(d);
            this.props.get_calendar_bookings(date, date);
        }
        this.setState({ date: d });
    }
    render() {
        let allViews =  ["week", "day"];
        const data = [];
        const { bookings, loading, errors } = this.props;
        const { get_data } = this.state;
        _.map(bookings, (item, ind) => {
            _.map(item.booking_details, (booking, i) => {
                data.push({
                    id: item.id,
                    hexColor: (ind%2 === 0) ? ((i%2 === 0) ? '1565C0' : '00bcd4') : ((i%2 === 0) ? '673ab7' : '9c27b0') ,
                    title: booking.model_row.title + ' - ' + item.user.full_name + ((booking.entity_employee !== null) ? (' (' + booking.entity_employee.user.full_name + ')') : ''),
                    start: string_format_date_time(booking.book_time),
                    end: string_format_date_time(booking.end_book_time),
                })
            })
        });
        const localizer = BigCalendar.momentLocalizer(moment) // or globalizeLocalizer
        return (
            <Fragment>
                {(loading && !get_data) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="TODAY'S NEXT APPOINTMENTS">
                                <div className={styles.empty_data}>
                                    <FullLoading />
                                </div>
                            </Wrapper>
                        </Column>
                    ) :
                    (errors !== null && !get_data) ? (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]} >
                            <Wrapper title="TODAY'S NEXT APPOINTMENTS">
                                <div className={styles.empty_data}>
                                    <ErrorPage errors={errors} />
                                </div>
                            </Wrapper>
                        </Column>
                    ):
                    //     (data.length === 0) ? (
                    //     <Column xsSize={12} mSize={6} classNames={[styles.mt_1]}>
                    //         <Wrapper title="TODAY'S NEXT APPOINTMENTS">
                    //             <div className={styles.empty_data}>
                    //                 <FontAwesomeIcon icon={faCalendarWeek} />
                    //                 <h4>Your Schedule is Empty</h4>
                    //                 <p>Make some appointments for schedule data to appear</p>
                    //             </div>
                    //         </Wrapper>
                    //     </Column>
                    // ) :
                        (
                        <Column xsSize={12} mSize={6} classNames={[styles.mt_1]}>
                            <Wrapper title="TODAY'S NEXT APPOINTMENTS">
                                <div className={styles.wrapper}>
                                    {(loading) ? <div className={styles.calendar_loading}><FullLoading /></div> : null}
                                    <BigCalendar
                                        events={data}
                                        eventPropGetter={this.eventStyleGetter.bind(this)}
                                        views={allViews}
                                        defaultView="day"
                                        step={60}
                                        localizer={localizer}
                                        onNavigate={this.onNavigate.bind(this)}
                                        onView={this.onView.bind(this)}
                                    />
                                </div>
                            </Wrapper>
                        </Column>
                    )}
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {
        bookings: state.booking.calendar.data,
        loading: state.booking.calendar.loading,
        errors: state.booking.calendar.errors,
    }
};

export default connect(mapStateToProps, { get_calendar_bookings })(TodayAppointments);