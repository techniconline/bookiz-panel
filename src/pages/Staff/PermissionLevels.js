import React from 'react';
import styles from './styles.scss';
import {Button, Checkbox, Column, Grid, Table} from "../../components";

class PermissionLevels extends React.Component {
    render() {
        const BC = [
            'Access Own Calendar',
            'Access Other Staff Calendars',
            'Can Book Appointments',
            'Home',
            'Clients',
            'Can download clients',
            'Messages',
            'Services'
        ];
        const sales = [
            'Can Checkout Sales',
            'Can delete invoices',
            'Daily Sales\n',
            'Appointments',
            'Invoices',
            'Vouchers',
            'Can Edit Checkout Prices'
        ];
        const staffs = [
            'Working Hours',
            'Closed Dates',
            'Staff Members',
            'Permission Levels'
        ];
        const inventory = [
            'Products'
        ];
        const reports = [
            'All Reports'
        ];
        const setups = [
            'Account Setup',
            'Point of Sale',
            'Client Settings',
            'Online Booking',
        ];
        return (
            <div className={styles.permissions}>
                <Grid>
                    <Column xsSize={12} mSize={7}>
                        <p className={styles.p_desc}>Setup which sections are accessible to each user permission level. All logged in staff can access the calendar, and owner accounts have full system access.</p>
                        <Table bordered>
                            <thead>
                            <tr>
                                <th>BOOKINGS & CLIENTS</th>
                                <th>BASIC</th>
                                <th>LOW</th>
                                <th>MEDIUM</th>
                                <th>HIGH</th>
                                <th>OWNER</th>
                            </tr>
                            </thead>
                            <tbody>
                            {_.map(BC, (item, key) => {
                                return (
                                    <tr key={key}>
                                        <td>{item}</td>
                                        <td><Checkbox defaultChecked={true} /></td>
                                        <td><Checkbox defaultChecked={true} /></td>
                                        <td><Checkbox defaultChecked={true} /></td>
                                        <td><Checkbox defaultChecked={true} /></td>
                                        <td><Checkbox defaultChecked={true} /></td>
                                    </tr>
                                )
                            })}
                            </tbody>
                        </Table>
                        <div className={styles.mt_2}>
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>SALES</th>
                                    <th>BASIC</th>
                                    <th>LOW</th>
                                    <th>MEDIUM</th>
                                    <th>HIGH</th>
                                    <th>OWNER</th>
                                </tr>
                                </thead>
                                <tbody>
                                {_.map(sales, (item, key) => {
                                    return (
                                        <tr key={key}>
                                            <td>{item}</td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </div>
                        <div className={styles.mt_2}>
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>STAFF</th>
                                    <th>BASIC</th>
                                    <th>LOW</th>
                                    <th>MEDIUM</th>
                                    <th>HIGH</th>
                                    <th>OWNER</th>
                                </tr>
                                </thead>
                                <tbody>
                                {_.map(staffs, (item, key) => {
                                    return (
                                        <tr key={key}>
                                            <td>{item}</td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </div>
                        <div className={styles.mt_2}>
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>INVENTORY</th>
                                    <th>BASIC</th>
                                    <th>LOW</th>
                                    <th>MEDIUM</th>
                                    <th>HIGH</th>
                                    <th>OWNER</th>
                                </tr>
                                </thead>
                                <tbody>
                                {_.map(inventory, (item, key) => {
                                    return (
                                        <tr key={key}>
                                            <td>{item}</td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </div>
                        <div className={styles.mt_2}>
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>REPORTS</th>
                                    <th>BASIC</th>
                                    <th>LOW</th>
                                    <th>MEDIUM</th>
                                    <th>HIGH</th>
                                    <th>OWNER</th>
                                </tr>
                                </thead>
                                <tbody>
                                {_.map(reports, (item, key) => {
                                    return (
                                        <tr key={key}>
                                            <td>{item}</td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </div>
                        <div className={styles.mt_2}>
                            <Table bordered>
                                <thead>
                                <tr>
                                    <th>SETUP</th>
                                    <th>BASIC</th>
                                    <th>LOW</th>
                                    <th>MEDIUM</th>
                                    <th>HIGH</th>
                                    <th>OWNER</th>
                                </tr>
                                </thead>
                                <tbody>
                                {_.map(setups, (item, key) => {
                                    return (
                                        <tr key={key}>
                                            <td>{item}</td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                            <td><Checkbox defaultChecked={true} /></td>
                                        </tr>
                                    )
                                })}
                                </tbody>
                            </Table>
                        </div>
                        <Button dark classNames={[styles.mh_2]}>Save Changes</Button>
                    </Column>
                </Grid>
            </div>
        )
    }
}

export default PermissionLevels;