import React from 'react';
import {
    Button,
    ButtonGroup,
    Grid,
    Column,
    InputForm,
} from "../../../components";
import styles from '../styles.scss';

class Commission extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalAdd: false,
            description: '',
            permission: 1,
        }
    }
    render() {
        return (
            <Grid>
                <Column xsSize={12} mSize={6}>
                    <InputForm labelText="SERVICE COMMISSION" value={this.state.first_name} onChange={(first_name)=>{this.setState({first_name})}} placeholder="0.0%" />
                </Column>
                <Column xsSize={12} mSize={6}>
                    <InputForm labelText="PRODUCT COMMISSION" value={this.state.last_name} onChange={(last_name)=>{this.setState({last_name})}} placeholder="0.0%" />
                </Column>
                <Column xsSize={12} mSize={6}>
                    <InputForm labelText="VOUCHER SALES COMMISSION" value={this.state.mobile} onChange={(mobile)=>{this.setState({mobile})}} placeholder="0.0%" />
                </Column>
            </Grid>
        )
    }
}

export default Commission;