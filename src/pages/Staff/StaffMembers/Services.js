import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
    Button,
    ButtonGroup,
    Grid,
    Column,
    InputForm,
    SelectForm,
    Checkbox,
} from "../../../components";
import { get_all_services } from '../../../redux/actions';
import styles from '../styles.scss';

class Services extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalAdd: false,
            description: '',
            open_services: [],
            services: []

        }
    }
    componentDidMount() {
        this.props.get_all_services();
        if(this.props.staffData !== null) {
            let services = this.state.services;
            let user_services = this.props.staffData.entity_employee_services;
            if(user_services.length > 0) {
                _.map(user_services, service => {
                    const srvice = services[service.entity_relation_service.entity_service_id];
                    if(srvice) {
                        if(srvice.indexOf(service.entity_relation_service.id) === -1) {
                            srvice.push(service.entity_relation_service.id);
                        }
                    }else {
                        services = { ...services, [service.entity_relation_service.entity_service_id]: [service.entity_relation_service.id]}
                    }
                });
                this.setState({ services });
            }
        }
    }
    onOpenService(id) {
        let { open_services } = this.state;
        const ind = open_services.indexOf(id);
        if(ind > -1) {
            open_services = _.filter(this.state.open_services, item => item !== id);
        }else {
            open_services.push(id);
        }
        this.setState({ open_services });
    }
    changeServices(id, service_id) {
        let { services } = this.state;
        if(services[service_id]) {
            let service = services[service_id];
            const ind = service.indexOf(id);
            if(ind > -1) {
                if (this.state.services[service_id].length > 1) {
                    service = _.filter(this.state.services[service_id], item => item !== id);
                    services = { ...services, [service_id]: service };
                }else {
                    delete services[service_id];
                }
            }else {
                service.push(id);
                services = { ...services, [service_id]: service };
            }
        }else {
            services = { ...services, [service_id]: [id] };
        }

        this.setState({ services });
    }
    render() {
        const { open_services, services } = this.state;
        console.log(services);
        return (
            <Grid>
                <Column xsSize={12} classNames={[styles.services]}>
                    <p className={styles.p_desc}>Assign services this staff member can perform.</p>
                    <div className={styles.checkboxes}>
                        {_.map(this.props.data, (item, key) => {
                            if(item.entity_relation_services.length === 0)
                                return null;
                            return (
                                <Fragment key={key}>
                                    <Checkbox title={item.title}
                                              checked={services[item.id] !== undefined}
                                              disabled
                                              onClick={this.onOpenService.bind(this, item.id)}/>
                                    {(open_services.indexOf(item.id) > -1) ? _.map(item.entity_relation_services, (service, i) => (
                                        <Checkbox key={i}
                                                  classNames={[styles.sub_service]}
                                                  id={'service_'+service.id}
                                                  title={service.title}
                                                  checked={services[item.id] && services[item.id].indexOf(service.id) > -1}
                                                  onChange={this.changeServices.bind(this, service.id, item.id)}/>
                                    )) : null}
                                </Fragment>
                            )
                        })}
                    </div>
                </Column>
            </Grid>
        )
        // return (
        //     <Grid>
        //         <Column xsSize={12} classNames={[styles.services]}>
        //             <p className={styles.p_desc}>Assign services this staff member can perform.</p>
        //             <Grid classNames={[styles.checkboxes]}>
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //                 <Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={this.state.email} onChange={(email)=>{this.setState({email})}} placeholder="mail@example.com" />
        //             </Grid>
        //         </Column>
        //     </Grid>
        // )
    }
}

const mapStateToProps = state => {
    const { data, loading } = state.service;
    return {
        data,
        loading
    };
};
export default connect(mapStateToProps, { get_all_services })(Services);