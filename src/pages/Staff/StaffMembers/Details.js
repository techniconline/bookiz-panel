import React from 'react';
import {
    Grid,
    Column,
    InputForm,
    SelectForm,
    Checkbox,
} from "../../../components";
import styles from '../styles.scss';

class Details extends React.Component {
    constructor(props) {
        super(props);
    }
    render() {
        const { item, onState } = this.props;
        const {permission, first_name, last_name, password, connector, booking_staff } = item;
        const permissions = [
            { val: 1, text: 'No Access'},
            { val: 2, text: 'Basic'},
            { val: 3, text: 'Low'},
            { val: 4, text: 'Medium'},
            { val: 5, text: 'High'}
        ];
        return (
            <Grid>
                <Column xsSize={12} mSize={6}>
                    <InputForm labelText="FIRST NAME" value={first_name} onChange={(first_name)=>{onState('first_name', first_name)}} placeholder="e.g Doe" />
                </Column>
                <Column xsSize={12} mSize={6}>
                    <InputForm labelText="LAST NAME" value={last_name} onChange={(last_name)=>{onState('last_name', last_name)}} placeholder="e.g Jane" />
                </Column>
                {(this.props.staffData === null) ? (
                    <Column xsSize={12}>
                        <InputForm labelText="PASSWORD" value={password} onChange={(password)=>{onState('password', password)}} placeholder="e.g Password" />
                    </Column>
                ) : null}
                <Column xsSize={12}>
                    <InputForm labelText="EMAIL" value={connector} onChange={(connector)=>{onState('connector', connector)}} placeholder="mail@example.com" />
                </Column>
                {/*<Column xsSize={12}>*/}
                    {/*<SelectForm labelText="USER PERMISSION" selected={permission} onChange={(permission)=>{onState('permission', permission)}} options={permissions} />*/}
                {/*</Column>*/}
                {/*<Column xsSize={12} classNames={[styles.mt_1]}>*/}
                    {/*<Checkbox title="ENABLE APPOINTMENT BOOKINGS" value={booking_staff} onChange={(booking_staff)=>{onState('booking_staff', booking_staff)}} placeholder="mail@example.com" />*/}
                {/*</Column>*/}
            </Grid>
        )
    }
}

export default Details;