import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import {Tabs, Tab, Grid, Column, ButtonGroup, Button} from "../../../components";
import Details from "./Details";
import Services from "./Services";
import Commission from "./Commission";
import { save_member } from '../../../redux/actions';
import styles from '../styles.scss';


class FormStaff extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            permission: 1,
            first_name: '',
            last_name: '',
            password: '',
            connector: '',
            booking_staff: '',
        }
    }
    componentDidMount() {
        if(this.props.data !== null) {
            const {
                first_name,
                last_name,
                username,
            } = this.props.data.user;
            this.setState({
                first_name,
                last_name,
                connector: username,
            });
        }
    }
    onSave() {
        const { first_name, last_name, password, connector } = this.state;
        const body = { first_name, last_name, password, connector };
        if(this.props.data !== null) body['entity_employee_id'] = this.props.data.id;
        this.props.save_member(body);
        this.props.onDismiss();
    }
    render() {
        return (
            <Fragment>
                <Tabs>
                    <Tab heading="DETAILS">
                        <Details staffData={this.props.data} onState={(prop, val) => { this.setState({ [prop]: val }); }} item={this.state} />
                    </Tab>
                    <Tab heading="SERVICES">
                        <Services staffData={this.props.data}  />
                    </Tab>
                    {/*<Tab heading="COMMISSION">*/}
                        {/*<Commission />*/}
                    {/*</Tab>*/}
                </Tabs>
                <Grid>
                    <Column xsSize={12} classNames={[styles.mt_1]}>
                        <div>
                            <ButtonGroup>
                                <Button dark onPress={this.onSave.bind(this)} >Save</Button>
                                <Button onPress={this.props.onDismiss.bind(this)} light>Cancel</Button>
                            </ButtonGroup>
                        </div>
                    </Column>
                </Grid>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    return {}
};

export default connect(mapStateToProps, { save_member })(FormStaff);