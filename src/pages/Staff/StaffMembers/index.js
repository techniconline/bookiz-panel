import React from 'react';
import { connect } from 'react-redux';
import {Button, gender, Grid, Modal, Table} from "../../../components";
import styles from '../styles.scss';
import FormStaff from './FormStaff';

class ClosedDates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            itemEdit: null,
            modalAdd: false
        }
    }
    renderForm(){
        const times = [];
        for(let hour = 0 ; hour < 24 ; hour++) {
            for(let min = 0 ; min < 60 ; min = min+5) {
                const tHours = (hour<10)? '0'+hour : hour;
                const tMin = (min<10)? '0'+min : min;
                times.push({val: tHours+"_"+tMin, text: tHours+":"+tMin});
            }
        }
        return (
            <Modal open={this.state.modalAdd} onClose={()=>{ this.setState({ modalAdd: false }); }} title={(this.state.itemEdit === null) ? 'New Staff' : 'Edit Staff'} >
                <FormStaff onDismiss={()=>{ this.setState({ modalAdd: false }); }} data={this.state.itemEdit} />
            </Modal>
        )
    }
    render() {
        const { staffs } = this.props;
        return (
            <div className={styles.staff_members}>
                {this.renderForm()}
                <Grid end>
                    <Button onPress={()=>{ this.setState({ modalAdd: true, itemEdit: null })}} dark>New Staff</Button>
                </Grid>
                <Table headStriped rowLinked>
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th>MOBILE NUMBER</th>
                            <th>USERNAME</th>
                            <th>GENDER</th>
                            <th>OWNER SERVICES COUNT</th>
                            {/*<th>CREATED AT</th>*/}
                            {/*<th>APPOINTMENTS</th>*/}
                            {/*<th>USER PERMISSION</th>*/}
                        </tr>
                    </thead>
                    <tbody>
                        {_.map(staffs, (staff, key) => (
                            <tr key={key} onClick={()=>{ this.setState({ modalAdd: true, itemEdit: staff })}}>
                                <td>{staff.user.full_name}</td>
                                <td>+98 9123456789</td>
                                <td>{staff.user.username}</td>
                                <td>{gender(staff.user.gender)}</td>
                                <td>{staff.entity_employee_services.length}</td>
                                {/*<td>Calendar booking enabled</td>*/}
                                {/*<td>Owner Login</td>*/}
                            </tr>
                        ))}
                    </tbody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { data, get_all_loading, get_all_errors } = state.staff;
    return {
        staffs: data,
        get_loading: get_all_loading,
        errors: get_all_errors,
    }
};

export default connect(mapStateToProps, null )(ClosedDates);