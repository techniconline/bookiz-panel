import React from 'react';
import {
    Badge,
    Button,
    ButtonGroup,
    Grid,
    Column,
    InputForm,
    Modal,
    SelectForm,
    Table,
    Alert
} from "../../components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import styles from './styles.scss';

class ClosedDates extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalAdd: false,
            description: '',
        }
    }
    renderForm(){
        const times = [];
        for(let hour = 0 ; hour < 24 ; hour++) {
            for(let min = 0 ; min < 60 ; min = min+5) {
                const tHours = (hour<10)? '0'+hour : hour;
                const tMin = (min<10)? '0'+min : min;
                times.push({val: tHours+"_"+tMin, text: tHours+":"+tMin});
            }
        }
        const repeats = [
            { val: 1, text: 'Don\'t repeat'},
            { val: 2, text: 'Weekly'}
        ];
        return (
            <Modal open={this.state.modalAdd}>
                <Grid>
                    <Column xsSize={12}>
                        <Alert info>Online bookings can not be placed during closed dates.</Alert>
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <SelectForm labelText="SHIFT START" options={times} />
                    </Column>
                    <Column xsSize={12} mSize={6}>
                        <SelectForm labelText="SHIFT END" options={times} />
                    </Column>
                    <Column xsSize={12}>
                        <InputForm labelText="DESCRIPTION" value={this.state.description} onChange={(description)=>{this.setState({description})}} placeholder="e.g public holiday" />
                    </Column>
                    <Column xsSize={12} classNames={[styles.mt_1]}>
                        <div>
                            <ButtonGroup>
                                <Button dark>Save</Button>
                                <Button onPress={()=>{ this.setState({ modalAdd: false })}} light>Cancel</Button>
                            </ButtonGroup>
                        </div>
                    </Column>
                </Grid>
            </Modal>
        )
    }
    render() {
        return (
            <div className={styles.m_1}>
                {this.renderForm()}
                <Grid end>
                    <Button onPress={()=>{ this.setState({ modalAdd: true })}} dark>New Closed Date</Button>
                </Grid>
                <Table headStriped rowLinked>
                    <thead>
                        <tr>
                            <th>DATE RANGE</th>
                            <th>NO. OF DAYS</th>
                            <th>LOCATIONS</th>
                            <th>DESCRIPTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr onClick={()=>{ this.setState({ modalAdd: true })}}>
                            <td>Sat, 22 Dec 2018 - Mon, 31 Dec 2018</td>
                            <td>10 Days</td>
                            <td>beautytime</td>
                            <td>Description Text</td>
                        </tr>
                    </tbody>
                </Table>
            </div>
        )
    }
}

export default ClosedDates;