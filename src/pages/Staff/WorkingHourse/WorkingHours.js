import React from 'react';
import { connect } from 'react-redux';
import {Badge, Button, ButtonGroup, Grid, Modal, Select, Table} from "../../../components/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {faPlus} from "@fortawesome/free-solid-svg-icons";
import styles from '../styles.scss';
import Form from './Form';


class WorkingHours extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modal: false,
            item: null
        }
    }
    onFilter(val) {
        console.log(val);
    }
    onSave() {

    }
    renderForm(){
        const { modal, item } = this.state;
        return (
            <Modal open={modal} onClose={()=>{ this.setState({ modal: false }); }} title={(item === null) ? 'New Shift' : `Edit Shift (${item.staff.user.full_name})`}>
                <Form onDismiss={()=>{ this.setState({ modal: false }); }} item={item} />
            </Modal>
        )
    }
    onEdit(item) {
        this.setState({ item, modal: true });
    }
    render() {
        const staffs = this.props.staffs;
        const staffs_opts = _.map(staffs, staff => {
           return {val: staff.id, text: staff.user.full_name};
        });
        staffs_opts.unshift({val: 0, text: 'All staff'});
        return (
            <div className={styles.working_hours}>
                {this.renderForm()}
                <Grid spaceBetween>
                    <div className={styles.filter}>
                        <Select options={staffs_opts} onChange={this.onFilter.bind(this)} />
                    </div>
                    <div>
                        <ButtonGroup>
                            <Button light> &lt; </Button>
                            <Button light>Today</Button>
                            <Button light>16 - 22 Dec, 2018</Button>
                            <Button light> &gt; </Button>
                        </ButtonGroup>
                    </div>
                </Grid>
                <Table bordered>
                    <thead>
                        <tr>
                            <th>Staff</th>
                            <th>Sun 16 Dec</th>
                            <th>Mon 17 Dec</th>
                            <th>Tue 18 Dec</th>
                            <th>Wed 19 Dec</th>
                            <th>Thu 20 Dec</th>
                            <th>Fri 21 Dec</th>
                            <th>Sat 22 Dec</th>
                        </tr>
                    </thead>
                    <tbody>
                    {_.map(staffs, (staff, key) => (
                        <tr key={key}>
                            <td>{staff.user.full_name}</td>
                            <td>
                                <span className={styles.new_staff} onClick={this.onEdit.bind(this, { staff, day: 'sun' })}>
                                    <FontAwesomeIcon icon={faPlus} />
                                </span>
                            </td>
                            <td>
                                <Badge linked info onClick={this.onEdit.bind(this, { staff, day: 'mon' })}>
                                    09:00 - 17:00
                                </Badge>
                            </td>
                            <td>
                                <Badge linked info onClick={this.onEdit.bind(this, { staff, day: 'tue' })}>
                                    09:00 - 17:00
                                </Badge>
                            </td>
                            <td>
                                <Badge linked info onClick={this.onEdit.bind(this, { staff, day: 'wed' })}>
                                    09:00 - 17:00
                                </Badge>
                            </td>
                            <td>
                                <Badge linked info onClick={this.onEdit.bind(this, { staff, day: 'thu' })}>
                                    09:00 - 17:00
                                </Badge>
                            </td>
                            <td>
                                <Badge linked info onClick={this.onEdit.bind(this, { staff, day: 'fri' })}>
                                    09:00 - 17:00
                                </Badge>
                            </td>
                            <td>
                                <Badge linked info onClick={this.onEdit.bind(this, { staff, day: 'sat' })}>
                                    09:00 - 17:00
                                </Badge>
                            </td>
                        </tr>
                    ))}
                        {/*<tr>*/}
                            {/*<td>Staff 1</td>*/}
                            {/*<td>*/}
                                {/*<span className={styles.new_staff} onClick={()=>{this.setState({ modal: true })}}>*/}
                                    {/*<FontAwesomeIcon icon={faPlus} />*/}
                                {/*</span>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<Badge linked info onClick={this.onEdit.bind(this)}>*/}
                                    {/*09:00 - 17:00*/}
                                {/*</Badge>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<Badge linked info onClick={this.onEdit.bind(this)}>*/}
                                    {/*09:00 - 17:00*/}
                                {/*</Badge>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<Badge linked info onClick={this.onEdit.bind(this)}>*/}
                                    {/*09:00 - 17:00*/}
                                {/*</Badge>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<Badge linked info onClick={this.onEdit.bind(this)}>*/}
                                    {/*09:00 - 17:00*/}
                                {/*</Badge>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<Badge linked info onClick={this.onEdit.bind(this)}>*/}
                                    {/*09:00 - 17:00*/}
                                {/*</Badge>*/}
                            {/*</td>*/}
                            {/*<td>*/}
                                {/*<Badge linked info onClick={this.onEdit.bind(this)}>*/}
                                    {/*09:00 - 17:00*/}
                                {/*</Badge>*/}
                            {/*</td>*/}
                        {/*</tr>*/}
                    </tbody>
                </Table>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const { data, get_all_loading, get_all_errors } = state.staff;
    return {
        staffs: data,
        get_loading: get_all_loading,
        errors: get_all_errors,
    }
};

export default connect(mapStateToProps, null)(WorkingHours);