import React from 'react';
import { connect } from 'react-redux';
import { Column, Grid, SelectForm, ButtonGroup, Button } from "../../../components";
import styles from '../styles.scss';
import { save_employee_working_hours } from '../../../redux/actions';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            open: '',
            close: '',
            repeat: 1,
            day: 'mon',
            entity_employee_id: 0
        }
    }
    componentDidMount() {
        const {day, staff} = this.props.item;
        this.setState({
            day,
            entity_employee_id: staff.id,
        });
    }
    onSave() {
        console.log(this.state);
        const { open, close, day, entity_employee_id} = this.state;
        const body = {
            entity_employee_id,
            params: {
                days: { [day]: { open, close } }
            }
        };
        // this.props.save_employee_working_hours(body)
    }
    render() {
        const { open, close, repeat } = this.state;
        const day = this.props.item.day;
        const times = [];
        for(let hour = 0 ; hour < 24 ; hour++) {
            for(let min = 0 ; min < 60 ; min = min+5) {
                const tHours = (hour<10)? '0'+hour : hour;
                const tMin = (min<10)? '0'+min : min;
                times.push({val: tHours+"_"+tMin, text: tHours+":"+tMin});
            }
        }
        const repeats = [
            { val: 1, text: 'Repeat'},
            { val: 2, text: 'Don\'t repeat'},
            { val: 3, text: 'Weekly'},
        ];
        const days = [
            { val: 'mon', text: 'monday'},
            { val: 'sat', text: 'Saturday'},
            { val: 'sun', text: 'Sunday'},
            { val: 'tue', text: 'Tuesday'},
            { val: 'wed', text: 'Wednesday'},
            { val: 'thu', text: 'Thursday'},
            { val: 'fri', text: 'Friday'},
        ];
        return (
            <Grid>
                <Column xsSize={12} mSize={6}>
                    <SelectForm labelText="SHIFT START" options={times} selected={open} onChange={(open) => { this.setState({ open }); }} />
                </Column>
                <Column xsSize={12} mSize={6}>
                    <SelectForm labelText="SHIFT END" options={times} selected={close} onChange={(close) => { this.setState({ close }); }} />
                </Column>
                <Column xsSize={12}>
                    <SelectForm labelText="DAY" options={days} selected={day} onChange={(day) => { this.setState({ day }); }} />
                </Column>
                <Column xsSize={12}>
                    <SelectForm labelText="REPEATS" options={repeats} selected={String(repeat)} onChange={(repeat) => { this.setState({ repeat }); }} />
                </Column>
                <Column xsSize={12} classNames={[styles.mt_1]}>
                    <div>
                        <ButtonGroup>
                            <Button dark onPress={this.onSave.bind(this)}>Save</Button>
                            <Button onPress={this.props.onDismiss.bind(this)} light>Cancel</Button>
                        </ButtonGroup>
                    </div>
                </Column>
            </Grid>
        )
    }
}

const mapStateToProps = state => {
    return {}
};

export default connect(mapStateToProps, { save_employee_working_hours })(Form);