import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, Tab, Tabs} from "../../components";
import styles from './styles.scss';
import WorkingHours from "./WorkingHourse/WorkingHours";
import ClosedDates from "./ClosedDates";
import StaffMembers from "./StaffMembers/index";
import PermissionLevels from "./PermissionLevels";
import { get_members } from '../../redux/actions';

class Staff extends React.Component {
    constructor(props) {
        super(props);
    }
    componentDidMount() {
        this.props.get_members();
    }
    render() {
        return(
            <PanelContainer title="Staff">
                <div className={styles.staff}>
                    {/*<Tabs>*/}
                        {/*<Tab heading="WORKING HOURS">*/}
                            {/*<WorkingHours />*/}
                        {/*</Tab>*/}
                        {/*<Tab heading="CLOSED DATES">*/}
                            {/*<ClosedDates />*/}
                        {/*</Tab>*/}
                        {/*<Tab heading="STAFF MEMBERS">*/}
                            <StaffMembers />
                        {/*</Tab>*/}
                        {/*<Tab heading="PERMISSION LEVELS">*/}
                            {/*<PermissionLevels />*/}
                        {/*</Tab>*/}
                    {/*</Tabs>*/}
                </div>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    return {}
};

export default connect(mapStateToProps, { get_members })(Staff);