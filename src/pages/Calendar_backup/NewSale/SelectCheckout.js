import React from 'react';
import { connect } from 'react-redux';
import styles from '../styles.scss';
import { MoonLoader } from 'react-spinners';
import _ from 'lodash';
import {Input} from "../../../components/index";
import { get_product_categories, get_products, get_service_categories, get_services } from '../../../redux/actions/index';
import {ProductItem} from "./ListItems";

class SelectCheckout extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            search: '',
            selectedLvl1: '',
            selectedLvl2: ''
        }
    }

    onLvl1(slug) {
        this.setState({ selectedLvl1: slug });
        switch(slug) {
            case 'product':
                this.props.get_product_categories();
                break;
            case 'service':
                this.props.get_service_categories();
                break;
            case 'voucher':
        }
    }
    onLvl2(id) {
        this.setState({ selectedLvl2: id });
        switch(this.state.selectedLvl1) {
            case 'product':
                this.props.get_products({ category: id });
                break;
            case 'service':
                this.props.get_services({ category: id });
                break;
            case 'voucher':
        }
    }

    renderLvl1() {
        return (
            <ul className={styles.categories}>
                <li onClick={()=>{this.onLvl1('product')}}>Products</li>
                <li onClick={()=>{this.onLvl1('service')}}>Services</li>
                <li onClick={()=>{this.onLvl1('voucher')}}>Vouchers</li>
            </ul>
        )
    }
    renderLvl2() {
        const { pc_loading, product_categories, sc_loading, service_categories } = this.props;
        const { selectedLvl1 } = this.state;
        const loading = (selectedLvl1 === 'product') ? pc_loading : (selectedLvl1 === 'service') ? sc_loading : true;
        const data = (selectedLvl1 === 'product') ? product_categories : (selectedLvl1 === 'service') ? service_categories : true;
        if(loading) {
            return (
                <div className={styles.loading}>
                    <MoonLoader
                        sizeUnit={"px"}
                        size={30}
                        color={'#24334A'}
                        loading={loading}
                    />
                </div>
            )
        }
        return (
            <ul className={styles.categories}>
                {_.map(data, (item, key) => <li key={key} onClick={()=>{this.onLvl2(item.id)}}>{item.name}</li> )}
            </ul>
        )
    }
    renderLvl3() {
        const { p_loading, products, s_loading, services } = this.props;
        const { selectedLvl1 } = this.state;
        const loading = (selectedLvl1 === 'product') ? p_loading : (selectedLvl1 === 'service') ? s_loading : true;
        const data = (selectedLvl1 === 'product') ? products : (selectedLvl1 === 'service') ? services : true;
        if(loading) {
            return (
                <div className={styles.loading}>
                    <MoonLoader
                        sizeUnit={"px"}
                        size={30}
                        color={'#24334A'}
                        loading={loading}
                    />
                </div>
            )
        }
        return (
            <ul className={styles.list}>
                {_.map(data, (item, key) => <ProductItem item={item} key={key} type={this.state.selectedLvl1} onClick={(item)=>{this.props.onSelected(item)}} /> )}
            </ul>
        )
    }
    renderSearch() {
        return (
            <ul className={styles.search_results}>
                <li>
                    <span>Products</span>
                    <ul className={styles.list}>
                        <li className={styles.item} onClick={()=>{console.log('Product')}}>
                            <span>
                                <span className={styles.title}>Shampoo</span>
                                <span className={styles.subtitle}>123ABC / 6 in stock</span>
                            </span>
                            <span className={styles.prices}>
                                <span className={styles.new}>15$</span>
                                <span className={styles.old}>20$</span>
                            </span>
                        </li>
                    </ul>
                </li>
                <li>
                    <span>Services</span>
                </li>
                <li>
                    <span>Vouchers</span>
                </li>
            </ul>
        )
    }
    render() {
        return (
            <div className={styles.selectItem}>
                <div className={styles.search_container}>
                    <Input type="search" value={this.state.search} placeholder="Scan barcode or search any item" onChange={(search)=>{this.setState({ search })}} />
                </div>
                {(this.state.search !== '') ? this.renderSearch() :
                    (this.state.selectedLvl2 !== '') ?  this.renderLvl3() :
                        (this.state.selectedLvl1 !== '') ?  this.renderLvl2() : this.renderLvl1()
                }
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        product_categories: state.product.categories,
        pc_loading: state.product.category_loading,
        p_loading: state.product.loading,
        products: state.product.data,
        service_categories: state.service.categories,
        sc_loading: state.service.category_loading,
        s_loading: state.service.loading,
        services: state.service.data,
    }
};

export default connect(mapStateToProps, { get_product_categories, get_products, get_service_categories, get_services })(SelectCheckout);