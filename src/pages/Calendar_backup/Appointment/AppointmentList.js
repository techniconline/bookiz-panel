import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {Column, Grid, SelectForm} from "../../../components/index";
import '../style.css';
import "react-datepicker/dist/react-datepicker.css";
import styles from '../styles.scss';

class AppointmentList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            times: [],
            durationTime: [],
            data: [
                {
                    staff: 0,
                    service: 0,
                    service_title: '',
                    start_time: new Date().getHours() + '_' + (new Date().getMinutes() - (new Date().getMinutes()%5) + 5),
                    duration: '0_30'
                }
            ]
        }
    }
    componentWillMount() {
        this.renderSelectData();
    }
    componentDidMount() {
        if(this.props.data.length > 0)
            this.setState({ data: this.props.data });
    }
    componentDidUpdate(preProps, preState){
        if(preProps.data !== this.state.data) {
            this.props.onData(this.state.data);
        }
    }
    onChangeStaff(ind, staff) {
        let data = this.state.data;
        data[ind] = {...data[ind], staff};
        if(data.length === ind+1) {
            data.push({
                staff: 0,
                service: 0,
                service_title: '',
                // start_time: new Date().getHours() + '_' + (new Date().getMinutes() - (new Date().getMinutes()%5) + 5),
                start_time: new Date().getHours() + '_' + ((new Date().getMinutes() - (new Date().getMinutes()%5) + 5)+((ind+1)*30)),
                duration: '0_30'
            });
        }
        this.setState({ data });
    }
    onChangeService(ind, id) {
        let data = this.state.data;
        const entity_service_id = id.split('_')[0];
        const service_id = id.split('_')[1];
        let service = _.filter(this.props.services, service => {
            if(String(service.id) === entity_service_id) {
                return _.filter(service.entity_relation_services, item => String(item.id) === service_id );
            }
        });
        service = service[0];
        data[ind] = {...this.state.data[ind], service: service_id, entity_service_id, service_title: service.title};
        this.setState({ data });
    }
    renderSelectData() {
        // Times
        const times = [];
        for(let hour = 0; hour < 24 ; hour++) {
            for(let min = 0; min < 60 ; min=min+5) {
                const text = ((hour < 10) ? '0' + hour : hour) + ':' + ((min < 10) ? '0' + min : min);
                const val = hour + '_' + min;
                times.push({val ,text})
            }
        }

        // Duration
        const durationTime = [];
        for(let min = 5 ; min < 60 ; min=min+5) durationTime.push({val: '0_'+min, text: `${min}min`});
        for(let min = 0 ; min < 60 ; min=min+5) durationTime.push({val: '1_'+min, text: `1h ${(min>0)?min+'min':''}`});
        for(let min = 0 ; min < 60 ; min=min+15) durationTime.push({val: '2_'+min, text: `2h ${(min>0)?min+'min':''}`});
        for(let h = 3 ; h <= 12 ; h++) {
            if(h < 8) {
                durationTime.push({val: h+'_0', text: `${h}h`});
                durationTime.push({val: h+'_30', text: `${h}h 30min`});
            }else {
                durationTime.push({val: h+'_0', text: `${h}h`});
            }
        }
        this.setState({ times, durationTime })

    }
    renderItem(item, ind) {
        const { start_time, service, duration, staff } = item;
        const { times, durationTime } = this.state;
        const staffs = _.map(this.props.staffs, item => {
            return { val: item.id, text: item.user.full_name };
        });
        staffs.unshift({ val: 0, text: 'Select staff', hide: true });
        const services = _.map(this.props.services, service => {
            let data = [];
            if(service.entity_relation_services.length > 0)
                data = _.map(service.entity_relation_services, item => {
                    return { val: item.entity_service_id+'_'+item.id, text: item.title };
                });
            return { label: service.title, data };
        });
        services.unshift({ val: 0, text: 'Choose a service', hide: true });
        return (
            <Grid classNames={[styles.item]} key={ind}>
                <Column  xsSize={12} mSize={2}>
                    <SelectForm labelText="Start time"
                                selected={start_time}
                                options={times}
                                onChange={(start_time) => {
                                    let data = this.state.data;
                                    data[ind] = {...this.state.data[ind], start_time};
                                    this.setState({ data })
                                }} />
                </Column>
                <Column xsSize={12} mSize={10}>
                    <SelectForm group
                                labelText="Service"
                                selected={service}
                                options={services}
                                onChange={this.onChangeService.bind(this, ind)} />
                </Column>
                <Column xsSize={12} mSize={2}>
                    <SelectForm labelText="Duration"
                                selected={duration}
                                options={durationTime}
                                // disabled={service === 0 }
                                disabled
                                onChange={(duration) => {
                                    let data = this.state.data;
                                    data[ind] = {...this.state.data[ind], duration};
                                    this.setState({ data })
                                }} />
                </Column>
                <Column xsSize={12} mSize={10}>
                    <SelectForm labelText="Staff"
                                selected={staff}
                                options={staffs}
                                disabled={duration === 0 || service === 0 }
                                onChange={this.onChangeStaff.bind(this, ind)} />
                </Column>
            </Grid>
        )
    }
    render() {
        const { data } = this.state;
        return (
            <div className={styles.appointment_list}>
                {_.map(data, (item, key) => {
                    return this.renderItem(item, key)
                })}
            </div>
        )
    }
}
const mapStateToProps = state => {
    return {
        services: state.service.data,
        staffs: state.staff.data,
    }
};

export default connect(mapStateToProps, null)(AppointmentList);