import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { MoonLoader } from 'react-spinners';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSearch, faAngleDown } from "@fortawesome/free-solid-svg-icons";
import {Button, Column, Grid, Input, TextAreaForm} from "../../../components/index";
import '../style.css';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import styles from '../styles.scss';
import AppointmentList from "./AppointmentList";
import {add_appointment, get_members, get_all_services} from "../../../redux/actions";

class AppointmentForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            name: '',
            search: '',
            note: '',
            date: new Date(),
            onSave: false,
            data: []
        }
    }
    componentDidUpdate() {
        if(!this.props.add_loading && this.state.onSave) {
            if(this.props.add_error === null) {
                this.props.onDismiss();
            }else {
                this.setState({ onSave: false });
            }
        }

    }
    componentDidMount() {
        this.props.get_members();
        this.props.get_all_services();
    }
    handleChange(date) {
        this.setState({ date });
    }
    onSave() {
        const { data, date } = this.state;
        console.log(data);
        console.log(date);
        data.pop();
        const newAppointment = _.map(data, item => {
            const { start_time, duration, staff, service_title, entity_service_id, service } = item;
            let start = date.setHours(start_time.split('_')[0]);
            start = new Date(start).setMinutes(start_time.split('_')[1]);
            let end = date.setHours(new Date(start).getHours() + Number(duration.split('_')[0]));
            end = new Date(end).setMinutes(new Date(start).getMinutes() + Number(duration.split('_')[1]));

           // return {
           //     start: new Date(start),
           //     end: new Date(end),
           //     service: {
           //         id: item.service,
           //         title: item.service_title,
           //     },
           //     title: item.service_title,
           //     hexColor: '1565C0',
           //     description: this.state.note,
           // }
            return {
                date,
                time: start_time,
                entity_employee_id: staff,
                entity_service_id,
                entity_relation_service_id: service,
                params: {
                    title: service_title,
                    description: this.state.note
                },
            }
        });
        // const newAppointment = {
        //     title: 'Multi-day Event',
        //     description: this.state.note,
        //     start: this.state.date,
        //     end: new Date(new Date().setHours(new Date().getHours()+1)),
        //     // start: new Date(),
        //     // end: new Date(new Date().setHours(new Date().getHours()+1)),
        // };
        this.props.add_appointment(newAppointment);
        this.setState({ onSave: true });
    }
    render() {
        const { date } = this.state;
        const { service_loading, staff_loading } = this.props;
        const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
        const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        const day = days[date.getDay()];
        const month = months[date.getMonth()];
        const dateText = `${day}, ${date.getDate()} ${month} ${date.getFullYear()}`;
        const loading = service_loading || staff_loading;
        return (
            <div className={styles.appointment_section}>
                <Grid end>
                    <Column xsSize={12} mSize={3}>
                        <div className={styles.sidebar}>
                            <div className={styles.search_container}>
                                <Input type="search" value={this.state.search} placeholder="Search client" onChange={(search)=>{this.setState({ search })}} />
                            </div>
                            <div className={styles.clients}>
                                <div className={styles.empty_client}>
                                    <FontAwesomeIcon icon={faSearch} />
                                    <p>Use the search to add a client, or keep empty to save as walk-in.</p>
                                </div>
                            </div>
                            <div className={styles.buttons}>
                                <Button dark onPress={this.onSave.bind(this)}>SAVE APPOINTMENT</Button>
                                <Button danger onPress={()=>{}}>CANCEL</Button>
                            </div>
                        </div>
                    </Column>
                    <Column xsSize={12} mSize={9}>
                        <div className={styles.content}>
                            <div className={styles.appointments}>
                                {(loading) ? (
                                        <div className={styles.loading}>
                                            <MoonLoader sizeUnit={"px"} size={30} color={'#24334A'} loading={loading} />
                                        </div>
                                    ) : (
                                        <Fragment>
                                            <div className={styles.day} onClick={()=>{ this._calendar.setOpen(true) }}>
                                                <span className={styles.text}>{dateText}</span>
                                                <FontAwesomeIcon icon={faAngleDown} />
                                            </div>
                                            <DatePicker
                                                selected={date}
                                                customInput={<span />}
                                                ref={(c) => this._calendar = c}
                                                onChange={this.handleChange.bind(this)}
                                                dateFormat="DD MMM YYYY"
                                            />
                                            <AppointmentList onData={(data)=>{this.setState({ data })}} data={this.state.data}  />
                                            <div className={styles.note}>
                                                <TextAreaForm labelText="Appointment notes" placeholder="Add an appointment note (visible to staff only) " value={this.state.note} onChange={(note)=>{this.setState({ note })}} />
                                            </div>
                                        </Fragment>
                                    )
                                }
                            </div>
                        </div>
                    </Column>
                </Grid>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        add_loading: state.appointment.add_loading,
        add_error: state.appointment.add_error,
    }
};

export default connect(mapStateToProps, { add_appointment, get_members, get_all_services })(AppointmentForm);