import React from 'react';
import { connect } from 'react-redux';
import {Button, FAB, FABItem, InputForm, Modal, PanelContainer} from "../../components";
import BigCalendar from "react-big-calendar";
import moment from "moment/moment";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import styles from './styles.scss';
import SaleForm from "./NewSale/SaleForm";
import BlockedTimeForm from "./BlockedTime/BlockedTimeForm";
import AppointmentForm from "./Appointment/AppointmentForm";
import BlockedTimeCalendar from "./BlockedTime/BlockedTimeCalendar";
import { get_appointments } from '../../redux/actions';

class CalendarComponent extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAppointment: false,
            modalCalendarBlockedTime: false,
            modalBlockedTime: false,
            modalSale: false,
            blockTimeEvent: null,
            blockedTimeEdit: false,
            blocked_times: [],
            name: ''
        }
    }
    componentDidMount() {
        this.props.get_appointments();
    }
    onBlockedTime(item, isEdit) {
        const { blocked_times } = this.state;
        console.log(item)
        if(isEdit) {
            let indItem = -1;
            blocked_times.map((b_item, ind) => {
                if (b_item !== item.id) {
                    indItem = ind;
                }
            });
            blocked_times.splice(indItem, 1);
        }
        blocked_times.push(item);
        this.setState({ modalBlockedTime: false, blockTimeEvent: null, blockedTimeEdit: false,  blocked_times });
    }
    renderModalAppointment(){
        return (
            <Modal no_padding title="New Appointment" onClose={()=>{ this.setState({ modalAppointment: false })}} full open={this.state.modalAppointment} >
                <AppointmentForm onDismiss={()=>{this.setState({ modalAppointment: false })}} />
            </Modal>
        )
    }
    renderModalCalendarBlockedTime(){
        return (
            <Modal no_padding title="Click the calendar to add blocked time" onClose={()=>{ this.setState({ modalCalendarBlockedTime: false })}} full open={this.state.modalCalendarBlockedTime} >
                <BlockedTimeCalendar onDismiss={()=>{this.setState({ modalCalendarBlockedTime: false })}} events={[]} onBlockedTime={(blockTimeEvent)=>{ this.setState({ modalCalendarBlockedTime: false, blockTimeEvent, modalBlockedTime: true }); }} />
            </Modal>
        )
    }
    renderModalBlockedTime(){
        return (
            <Modal no_padding title={(this.state.blockedTimeEdit) ? "Edit Blocked Time" : "New Blocked Time"} onClose={()=>{ this.setState({ modalBlockedTime: false, blockedTimeEdit: false })}} open={this.state.modalBlockedTime} >
                <BlockedTimeForm isEdit={this.state.blockedTimeEdit} onSave={this.onBlockedTime.bind(this)} onDismiss={()=>{this.setState({ modalBlockedTime: false, blockedTimeEdit: false })}} event={this.state.blockTimeEvent} onBlockedTime={this.onBlockedTime.bind(this)} />
            </Modal>
        )
    }
    renderModalSale(){
        return (
            <Modal no_padding title="Checkout" onClose={()=>{ this.setState({ modalSale: false })}} full open={this.state.modalSale}>
                <SaleForm onDismiss={()=>{ this.setState({ modalSale: false })}} />
            </Modal>
        )
    }
    eventStyleGetter(e) {
        if(e.hexColor)
            return { style: { backgroundColor: '#' + e.hexColor } };
        return null;
    }
    onSelectEvent(e) {
        if(e.type === 'blocked_time') {
            this.setState({ modalBlockedTime: true, blockedTimeEdit: true, blockTimeEvent: e })
        } else {

        }
    }
    render() {
        const allViews =  ["week", "day"];
        const localizer = BigCalendar.momentLocalizer(moment);
        const { blocked_times } = this.state;
        const { appointments } = this.props;
        const events = appointments.concat(blocked_times);
        console.log('events', events, appointments);
        const fabSubs = [
            (
                <FABItem label="New Blocked Time" onClick={()=>{this.setState({ modalCalendarBlockedTime: true })}}>
                    <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                </FABItem>
            ),
            (
                <FABItem label="New Sale" onClick={()=>{this.setState({ modalSale: true })}}>
                    <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                </FABItem>
            )
        ];
        return (
            <PanelContainer title="Calendar">
                <div className={styles.calendar}>
                    <BigCalendar
                        events={events}
                        onSelectEvent={this.onSelectEvent.bind(this)}
                        eventPropGetter={this.eventStyleGetter.bind(this)}
                        views={allViews}
                        defaultView="day"
                        step={30}
                        localizer={localizer}
                    />
                    {this.renderModalAppointment()}
                    {this.renderModalBlockedTime()}
                    {this.renderModalCalendarBlockedTime()}
                    {this.renderModalSale()}
                    <FAB subs={fabSubs} dark>
                        <FABItem label="New Appointment" onClick={()=>{this.setState({ modalAppointment: true })}}>
                            <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                        </FABItem>
                    </FAB>
                </div>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {
        appointments: state.appointment.data
    }
};

export default connect(mapStateToProps, { get_appointments })(CalendarComponent);