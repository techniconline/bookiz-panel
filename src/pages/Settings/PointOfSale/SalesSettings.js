import React from 'react';
import { connect } from 'react-redux';
import {Button, Column, Grid, PanelContainer, SelectForm, Checkbox } from "../../../components";
import styles from './../styles.scss';

class SalesSettings extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            period: 1
        }
    }
    render() {
        const periods = [
            { val: 1, text: 'Other' },
            { val: 2, text: 'Beauty Salon' },
            { val: 3, text: 'Spa' }
        ];
        return (
            <PanelContainer title="Settings / Sales Settings">
                <Grid >
                    <Column xsSize={12} mSize={6}>
                        <form>
                            <h4>Staff Commissions</h4>
                            <div className={styles.mt_1}>
                                <Checkbox title="CALCULATE BY ITEM SALE PRICE BEFORE DISCOUNT" />
                            </div>
                            <div className={styles.mt_1}>
                                <Checkbox title="CALCULATE BY ITEM SALE PRICE INCLUDING TAX" />
                            </div>
                            <div className={styles.divider} />
                            <h4>Vouchers</h4>
                            <SelectForm labelText="DEFAULT EXPIRY PERIOD" selected={this.state.period} options={periods} onChange={(period)=>{this.setState({ period });}} />
                            <div className={styles.mt_1}>
                                <Button dark>Save Changes</Button>
                            </div>
                        </form>
                    </Column>
                </Grid>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(SalesSettings);