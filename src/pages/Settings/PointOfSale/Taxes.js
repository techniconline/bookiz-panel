import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, Grid, Column, Button, Modal, InputForm, Table, Wrapper} from "../../../components";
import styles from './../styles.scss';

class Taxes extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAdd: false,
            name: ''
        }
    }
    renderAdd(){
        return (
            <Modal open={this.state.modalAdd}>
                <form>
                    <InputForm labelText="PAYMENT TYPE NAME" value={this.state.name} placeholder="e.g. Mastercard" onChange={(name)=>{this.setState({ name });}} />
                    <div className={styles.mt_1}>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalAdd: false })}} dark>Cancel</Button>
                    </div>
                </form>
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer hasContent={false} title="Settings / Taxes">
                <Grid>
                    <Column xsSize={12} mSize={3} />
                    <Column xsSize={12} mSize={6} >
                        <Wrapper classNames={[styles.mt_1]}>
                            <h4>Tax Rates</h4>
                            <p className={styles.mt_1}>Add your tax rates and use groups for multiple taxes, for example combining city and state taxes</p>
                            <Table>
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Tax Name</th>
                                        <th>Tax Rate</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>TaxName</td>
                                        <td>1%</td>
                                    </tr>
                                </tbody>
                            </Table>
                        </Wrapper>
                        <Wrapper classNames={[styles.mt_1]}>
                            <h4>Tax Defaults</h4>
                            <p className={styles.mt_1}>Setup the default taxes for your business, you can still override defaults in the settings of individual products and services</p>
                            <Table>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Location</th>
                                    <th>Products Default</th>
                                    <th>Services Default</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>beautytime</td>
                                    <td>No tax</td>
                                    <td>No tax</td>
                                </tr>
                                </tbody>
                            </Table>
                        </Wrapper>
                        <Wrapper classNames={[styles.mt_1]}>
                            <h4>Tax Calculation</h4>
                            <p className={styles.mt_1}>Your retail prices are including taxes. <a href="#">Change this setting</a></p>
                        </Wrapper>
                    </Column>
                </Grid>

            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(Taxes);