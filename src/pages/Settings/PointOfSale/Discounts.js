import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, Grid, Column, Button, FAB, Modal, InputForm, Table, Wrapper} from "../../../components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import styles from './../styles.scss';

class Discounts extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAdd: false,
            name: '',
            phone: '',
            email: '',
            street: '',
            options: '',
            city: '',
            state: '',
            zip_code: ''
        }
    }
    renderAdd(){
        return (
            <Modal open={this.state.modalAdd}>
                <form>
                    <InputForm labelText="LOCATION NAME" value={this.state.name} placeholder="Downtown Branch" onChange={(name)=>{this.setState({ name });}} />
                    <InputForm labelText="CONTACT NUMBER" type="tel" value={this.state.phone} onChange={(phone)=>{this.setState({ phone });}} />
                    <InputForm labelText="CONTACT EMAIL" type="email" value={this.state.email} onChange={(email)=>{this.setState({ email });}} />
                    <InputForm labelText="STREET ADDRESS" type="text" value={this.state.street} onChange={(street)=>{this.setState({ street });}} />
                    <InputForm labelText="APT, SUITE, BLDG. (OPTIONAL)" type="text" value={this.state.options} onChange={(options)=>{this.setState({ options });}} />
                    <Grid classNames={[styles.grid]}>
                        <Column xsSize={12} mSize={4}>
                            <InputForm labelText="CITY" type="text" value={this.state.city} onChange={(city)=>{this.setState({ city });}} />
                        </Column>
                        <Column xsSize={12} mSize={4}>
                            <InputForm labelText="STATE" type="text" value={this.state.state} onChange={(state)=>{this.setState({ state });}} />
                        </Column>
                        <Column xsSize={12} mSize={4}>
                            <InputForm labelText="ZIP CODE" type="text" value={this.state.zip_code} onChange={(zip_code)=>{this.setState({ zip_code });}} />
                        </Column>
                    </Grid>
                    <div className={styles.mt_1}>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalAdd: false })}} dark>Cancel</Button>
                    </div>
                </form>
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer title="Settings / Discounts">
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>DISCOUNT TYPE</th>
                        <th>VALUE</th>
                        <th>DATE ADDED</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Something</td>
                        <td>10% off</td>
                        <td>Tuesday, 18 Dec 2018</td>
                    </tr>
                    </tbody>
                </Table>
                {this.renderAdd()}
                <FAB>
                    <div className={styles.fab_link} onClick={()=>{this.setState({ modalAdd: true })}}>
                        <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                    </div>
                </FAB>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(Discounts);