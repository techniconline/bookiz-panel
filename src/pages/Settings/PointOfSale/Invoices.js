import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, Grid, Column, Button, Modal, InputForm, Table, Wrapper, Checkbox} from "../../../components";
import styles from './../styles.scss';

class Invoices extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAdd: false,
            title: '',
            rcl1: '',
            rcl2: '',
            receipt_footer: ''
        }
    }
    renderAdd(){
        return (
            <Modal open={this.state.modalAdd}>
                <form>
                    <InputForm labelText="PAYMENT TYPE NAME" value={this.state.name} placeholder="e.g. Mastercard" onChange={(name)=>{this.setState({ name });}} />
                    <div className={styles.mt_1}>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalAdd: false })}} dark>Cancel</Button>
                    </div>
                </form>
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer hasContent={false} title="Settings / Taxes">
                <Grid>
                    <Column xsSize={12} mSize={3} />
                    <Column xsSize={12} mSize={6} >
                        <Wrapper classNames={[styles.mt_1]}>
                            <h4>Invoices & Receipts</h4>
                            <div className={styles.mt_1}>
                                <Checkbox title="Automatically print receipts upon sale completion" />
                            </div>
                            <div className={styles.mt_1}>
                                <Checkbox title="Show client mobile and email on invoices" />
                            </div>
                            <div className={styles.mt_1}>
                                <Checkbox title="Show client address on invoices" />
                            </div>

                            <InputForm labelText="Invoice Title" value={this.state.title} onChange={(title)=>{this.setState({ title });}} />
                            <InputForm labelText="Receipt Custom Line 1" value={this.state.rcl1} onChange={(rcl1)=>{this.setState({ rcl1 });}} />
                            <InputForm labelText="Receipt Custom Line 2" value={this.state.rcl2} onChange={(rcl2)=>{this.setState({ rcl2 });}} />
                            <InputForm labelText="Receipt Footer" value={this.state.receipt_footer} onChange={(receipt_footer)=>{this.setState({ receipt_footer });}} />
                            <div className={styles.mt_1}>
                                <Button dark>Save</Button>
                            </div>
                        </Wrapper>
                        <Wrapper classNames={[styles.mt_1]}>
                            <h4>Invoice Number Sequencing</h4>
                            <Table>
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>LOCATION NAME</th>
                                    <th>INVOICE NO. PREFIX</th>
                                    <th>NEXT INVOICE NUMBER</th>
                                    <th />
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>1</td>
                                    <td>beautytime</td>
                                    <td></td>
                                    <td>4</td>
                                    <td>Change</td>

                                </tr>
                                </tbody>
                            </Table>
                        </Wrapper>
                    </Column>
                </Grid>

            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(Invoices);