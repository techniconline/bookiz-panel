import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, FAB, Button, Modal, InputForm, Table} from "../../../components";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import styles from './../styles.scss';

class CancellationReasons extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAdd: false,
            name: ''
        }
    }
    renderAdd(){
        return (
            <Modal open={this.state.modalAdd}>
                <form>
                    <InputForm labelText="PAYMENT TYPE NAME" value={this.state.name} placeholder="e.g. Mastercard" onChange={(name)=>{this.setState({ name });}} />
                    <div className={styles.mt_1}>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalAdd: false })}} dark>Cancel</Button>
                    </div>
                </form>
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer title="Settings / Payment Types">
                <Table>
                    <thead>
                    <tr>
                        <th>#</th>
                        <th>PAYMENT TYPE</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>Card</td>
                    </tr>
                    </tbody>
                </Table>
                {this.renderAdd()}
                <FAB>
                    <div className={styles.fab_link} onClick={()=>{this.setState({ modalAdd: true })}}>
                        <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                    </div>
                </FAB>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(CancellationReasons);