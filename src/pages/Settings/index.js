import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {PanelContainer, Grid, Column, Wrapper} from "../../components";
import styles from './styles.scss';
import CompanyDetails from "./AccountSetup/CompanyDetails";

class Index extends React.Component {
    render() {
        // return <CompanyDetails />;
        return (
            <PanelContainer hasContent={false} title="Settings">
                <Grid classNames={[styles.settings_page, styles.container]}>
                    <Column xsSize={12} mSize={8} >
                        <Grid>
                            <Column xsSize={12} mSize={6} classNames={[styles.mt_2]} >
                                <Wrapper title="Account Setup">
                                    <div className={styles.links}>
                                        <Link to="/settings/company_details" className={styles.link}>Company Details</Link>
                                        <Link to="/settings/gallery" className={styles.link}>Gallery</Link>
                                        <Link to="/settings/workingtimes" className={styles.link}>Working Times</Link>
                                        {/*<Link to="/locations" className={styles.link}>Locations</Link>*/}
                                        {/*<Link to="/resources" className={styles.link}>Resources</Link>*/}
                                        {/*<Link to="/calendar_settings" className={styles.link}>Calendar Settings</Link>*/}
                                        {/*<Link to="/online_booking" className={styles.link}>Online Booking Settings</Link>*/}
                                        {/*<Link to="/staff_notifications" className={styles.link}>Staff Notifications</Link>*/}
                                    </div>
                                </Wrapper>
                            </Column>
                            <Column xsSize={12} mSize={6} classNames={[styles.mt_2]} >
                                <Wrapper title="Point of Sale">
                                    <div className={styles.links}>
                                        {/*<Link to="/payment_methods" className={styles.link}>Payment Types</Link>*/}
                                        <Link to="/settings/taxes" className={styles.link}>Taxes</Link>
                                        {/*<Link to="/discounts" className={styles.link}>Discount Types</Link>*/}
                                        {/*<Link to="/sales_settings" className={styles.link}>Sales Settings</Link>*/}
                                        <Link to="/settings/invoices" className={styles.link}>Invoices & Receipts</Link>
                                    </div>
                                </Wrapper>
                            </Column>
                            {/*<Column xsSize={12} mSize={6} classNames={[styles.mt_2]} >*/}
                                {/*<Wrapper title="Client Settings">*/}
                                    {/*<div className={styles.links}>*/}
                                        {/*<Link to="/client_notifications" className={styles.link}>Client Notifications</Link>*/}
                                        {/*<Link to="/referral_sources" className={styles.link}>Referral Sources</Link>*/}
                                        {/*<Link to="/cancellation_reasons" className={styles.link}>Cancellation Reasons</Link>*/}
                                    {/*</div>*/}
                                {/*</Wrapper>*/}
                            {/*</Column>*/}
                        </Grid>
                    </Column>
                </Grid>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
  return {};
};

export default connect(mapStateToProps, null)(Index);