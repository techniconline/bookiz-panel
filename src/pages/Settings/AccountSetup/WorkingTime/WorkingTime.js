import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, Table} from "../../../../components/index";
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { MoonLoader } from 'react-spinners';
import styles from '../../styles.scss';
import {get_single_entity} from "../../../../redux/actions/index";

class WorkingTime extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            days: []
        }
    }
    componentDidMount() {
        this.props.get_single_entity();
    }
    render() {
        const {entity, loading } = this.props;
        return (
            <PanelContainer title="Settings / Working Time">
                {(loading) ? (
                    <div className={styles.loading}>
                        <MoonLoader
                            sizeUnit={"px"}
                            size={30}
                            color={'#24334A'}
                            loading={loading}
                        />
                    </div>
                ) : (entity !== null) ? (
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Sunday</th>
                            <th>Monday</th>
                            <th>Tuesday</th>
                            <th>Wednesday</th>
                            <th>Thursday</th>
                            <th>Friday</th>
                            <th>Saturday</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>From</th>
                                <td>10:00</td>
                                <td>10:00</td>
                                <td>10:00</td>
                                <td>10:00</td>
                                <td>10:00</td>
                                <td>10:00</td>
                                <td>10:00</td>
                            </tr>
                            <tr>
                                <th>To</th>
                                <td>16:00</td>
                                <td>16:00</td>
                                <td>16:00</td>
                                <td>16:00</td>
                                <td>16:00</td>
                                <td>16:00</td>
                                <td>16:00</td>
                            </tr>
                        </tbody>
                    </Table>
                ) : null}
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    const {
        entity,
        get_loading
    } = state.entity;
    return {
        entity,
        loading: get_loading
    };
};

export default connect(mapStateToProps, { get_single_entity })(WorkingTime);