import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import styles from "../../styles.scss";
import {Alert, Button, ButtonGroup, Checkbox, SelectForm} from "../../../../components";
import { save_entity_image } from "../../../../redux/actions";
import {PulseLoader} from "react-spinners";
import _ from "lodash";
import ReactCrop from "react-image-crop";

class UploadModal extends Component {
    constructor(props) {
        super(props);
        this.state = {
            croppedImageUrl: null,
            crop: {
                x: 20,
                y: 10,
                aspect: 16/9,
                width: 30,
            },
            slider: true,
            service: null
        }
    }
    componentDidUpdate() {
        const { loading, errors, data, onDismiss } = this.props;
        if(!loading && data !== null && errors === null) {
            onDismiss();
        }
    }
    renderErrors() {
        const { errors } = this.props;
        if(errors !== null) {
            return (
                <Alert danger>
                    {_.map(errors, (error, key) => <p key={key} className={styles.errorAlert}>* {error[0]}</p> )}
                </Alert>
            )
        }
        return null;
    }
    getCroppedImg(image, pixelCrop, fileName) {
        const canvas = document.createElement('canvas');
        canvas.width = pixelCrop.width;
        canvas.height = pixelCrop.height;
        const ctx = canvas.getContext('2d');

        ctx.drawImage(
            image,
            pixelCrop.x,
            pixelCrop.y,
            pixelCrop.width,
            pixelCrop.height,
            0,
            0,
            pixelCrop.width,
            pixelCrop.height,
        );

        return new Promise((resolve) => {
            canvas.toBlob((blob) => {
                blob.name = fileName; // eslint-disable-line no-param-reassign
                window.URL.revokeObjectURL(this.fileUrl);
                resolve(blob);
            }, 'image/jpeg');
        });
    }
    makeClientCrop(crop, pixelCrop) {
        if (this.imageRef && crop.width && crop.height) {
            this.getCroppedImg(
                this.imageRef,
                pixelCrop,
                'newFile.jpeg',
            ).then(croppedImageUrl => {
                this.setState({ croppedImageUrl });
            })
        }
    }
    onCropComplete = (crop, pixelCrop) => {
        this.makeClientCrop(crop, pixelCrop);
    };
    onImageLoaded = (image, pixelCrop) => {
        this.imageRef = image;
        this.makeClientCrop(this.state.crop, pixelCrop);
    };
    onSubmit(){
        const { croppedImageUrl, slider, service} = this.state;
        this.props.save_entity_image([{file: croppedImageUrl}], slider, service);
    }
    renderButton() {
        const {  } = this.props;
        return (this.props.loading) ? (
            <Button dark>
                <PulseLoader
                    sizeUnit={"px"}
                    size={10}
                    color={'#fff'}
                    loading
                />
            </Button>
        ) : (
            <Button classNames={[styles.upload_btn]}
                    onPress={this.onSubmit.bind(this)}
                    dark title="UPLOAD IMAGE" />
        )
    }
    render() {
        const { image, onDismiss, services, services_loading } = this.props;
        const services_options = [];
        if(image === null) return null;
        if(services.length > 0) {
            _.map(services, main_service => {
                const opt = { label: main_service.title, data: [] };
                _.map(main_service.entity_relation_services, service => {
                    opt.data.push({ text: service.title, val: service.id  });
                });
                services_options.push(opt);
            });
        }
        return (
            <Fragment>
                {this.renderErrors()}
                <div className={styles.crop_container}>
                    <ReactCrop onChange={(crop)=>{ this.setState({ crop }); }}
                               onComplete={this.onCropComplete.bind(this)}
                               onImageLoaded={this.onImageLoaded.bind(this)}
                               src={image.src}
                               crop={this.state.crop} />
                </div>
                <div className={styles.bottom_modal}>
                    <Checkbox title="Is Slider"
                              checked={this.state.slider}
                              onChange={() => this.setState({ slider: !this.state.slider })} />
                    {(services_loading) ? (
                        <SelectForm disabled labelText="SELECT SERVICE" />
                    ) : (
                        <SelectForm labelText="SELECT SERVICE"
                                    disabled={this.state.slider}
                                    options={services_options}
                                    selected={this.state.service}
                                    onChange={service => this.setState({ service })} />
                    )}
                </div>
                <ButtonGroup>
                    {this.renderButton()}
                    <Button onPress={onDismiss.bind(this)} title="CANCEL" />
                </ButtonGroup>
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data } = state.gallery.save;
    return {
        loading,
        errors,
        data,
        services_loading: state.service.loading,
        services: state.service.data,
    }
};

export default connect(mapStateToProps, { save_entity_image })(UploadModal);