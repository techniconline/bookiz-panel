import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import {save_entity_image, get_single_entity, get_all_services} from "../../../../redux/actions";
import {
    Button,
    ButtonGroup,
    Column,
    ErrorPage,
    FullLoading,
    Grid,
    Label,
    Modal,
    PanelContainer, SelectForm
} from "../../../../components";
import styles from "../../styles.scss";
import {DEFAULT_IMAGE} from "../../../../res/Images";
import 'react-image-crop/dist/ReactCrop.css';
import 'react-image-crop/lib/ReactCrop.scss';
import _ from "lodash";
import DeleteModal from "./DeleteModal";
import UploadModal from "./UploadModal";

class Gallery extends Component{
    constructor(props) {
        super(props);
        this.state = {
            delete_item: null,
            delete_modal: false,
            modal: false,
            image: null,
            type: 1
        }
    }
    componentDidMount() {
        this.props.get_single_entity();
        this.props.get_all_services();
    }

    renderModal() {
        const { image, modal } = this.state;
        return (
            <Modal open={modal} onClose={()=>{ this.setState({ modal: false, image: null })}} >
                <UploadModal image={image} onDismiss={()=>{ this.setState({ modal: false })}} />
            </Modal>
        )
    }
    newImage(event) {
        const image = { src: URL.createObjectURL(event.target.files[0]), file: event.target.files[0] };
        this.setState({ modal: true, image });
    }
    renderDelete() {
        const { delete_modal, delete_item } = this.state;
        return (
            <Modal width="20rem" open={delete_modal} title="DELETE IMAGE" onClose={()=>{ this.setState({ delete_modal: false })}} >
                <DeleteModal item={delete_item} onDismiss={()=>{ this.setState({ delete_modal: false })}} />
            </Modal>
        )
    }
    renderSliderImgs() {
        const { entity_slider_medias } = this.props.entity;
        return (
            <Column xsSize={12} >
                <Label>Images</Label>
                <div className={styles.gallery}>
                    <div className={styles.default_img_container}>
                        <img src={DEFAULT_IMAGE} className={styles.img_default} />
                        <ButtonGroup classNames={[styles.actions]}>
                            <Button refs={this.fileUpload}
                                    classNames={[styles.upload_btn]}
                                    onChange={this.newImage.bind(this)}
                                    type="file"
                                    accept="image/*"
                                    title="NEW IMAGE" />
                        </ButtonGroup>
                    </div>
                    {_.map(entity_slider_medias, (media, key) => (
                        <div className={styles.img_container} key={key}>
                            <img src={media.url_thumbnail} />
                            <ButtonGroup classNames={[styles.actions]} >
                                <Button danger title="DELETE" onPress={() => { this.setState({ delete_item: media, delete_modal: true })}} />
                            </ButtonGroup>
                        </div>
                    ))}
                </div>
            </Column>
        )
    }
    renderServicesImgs() {
        const { entity_relation_services } = this.props.entity;
        return (
            <Fragment>
                <Column xsSize={12} >
                    <Label>Services</Label>
                    <div className={styles.gallery}>
                        <div className={styles.default_img_container}>
                            <img src={DEFAULT_IMAGE} className={styles.img_default} />
                            <ButtonGroup classNames={[styles.actions]}>
                                <Button refs={this.fileUpload}
                                        classNames={[styles.upload_btn]}
                                        onChange={this.newImage.bind(this)}
                                        type="file"
                                        accept="image/*"
                                        title="NEW IMAGE" />
                            </ButtonGroup>
                        </div>
                    </div>
                </Column>
                {_.map(entity_relation_services, (service, key) => (service.entity_medias.length === 0) ? null : (
                    <Column xsSize={12} key={key} >
                        <Label>{service.title}</Label>
                        <div className={styles.gallery}>
                            {_.map(service.entity_medias, (media, ind) => (
                                <div className={styles.img_container} key={ind}>
                                    <img src={media.url_thumbnail} />
                                    <ButtonGroup classNames={[styles.actions]} >
                                        <Button danger title="DELETE" onPress={() => { this.setState({ delete_item: media, delete_modal: true })}} />
                                    </ButtonGroup>
                                </div>
                            ))}
                        </div>
                    </Column>
                ))}
            </Fragment>
        )
    }
    render() {
        const { entity, errors, loading } = this.props;
        const gallery_type_options = [
            { val: 1, text: 'Slider' },
            { val: 2, text: 'Services' }
        ];
        return (
            <PanelContainer title="Settings / Gallery">
                {(loading) ? (<FullLoading />)
                    : (errors) ? <ErrorPage errors={errors}/>
                    : (entity) ? (
                        <Grid classNames={[styles.settings_page]}>
                            <Column xsSize={12} >
                                <SelectForm labelText="SELECT TYPE"
                                            options={gallery_type_options}
                                            selected={this.state.type}
                                            onChange={(type) => { this.setState({ type }); }}/>
                            </Column>
                            {(Number(this.state.type) === 1) ? this.renderSliderImgs() : this.renderServicesImgs()}
                            {this.renderModal()}
                            {this.renderDelete()}
                        </Grid>) : null}
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    return {
        loading: state.entity.get_loading,
        entity: state.entity.entity
    };
};

export default connect(mapStateToProps, { save_entity_image , get_single_entity, get_all_services })(Gallery);