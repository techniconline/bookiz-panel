import React, {Component, Fragment} from 'react';
import { connect } from 'react-redux';
import styles from "../../styles.scss";
import {Alert, Button} from "../../../../components";
import { delete_entity_image } from "../../../../redux/actions";
import {PulseLoader} from "react-spinners";
import _ from "lodash";

class DeleteModal extends Component {
    constructor(props) {
        super(props);
    }
    componentDidUpdate() {
        const { loading, errors, data, onDismiss } = this.props;
        if(!loading && data !== null && errors === null) {
            onDismiss();
        }
    }
    renderErrors() {
        const { errors } = this.props;
        if(errors !== null) {
            return (
                <Alert danger>
                    {_.map(errors, (error, key) => <p key={key} className={styles.errorAlert}>* {error[0]}</p> )}
                </Alert>
            )
        }
        return null;
    }
    renderButton() {
        const { item } = this.props;
        return (this.props.loading) ? (
            <Button danger>
                <PulseLoader
                    sizeUnit={"px"}
                    size={10}
                    color={'#fff'}
                    loading
                />
            </Button>
        ) : (
            <Button onPress={() => { this.props.delete_entity_image(item.id)}} danger title="DELETE" />
        )
    }
    render() {
        const { item, onDismiss } = this.props;
        if(item === null) return null;
        return (
            <Fragment>
                {this.renderErrors()}
                <div className={styles.delete_modal}>
                    <p>Are you sure to delete this image??</p>
                    <img src={item.url_thumbnail} />
                </div>
                {this.renderButton()}
                <Button onPress={onDismiss.bind(this)} title="CANCEL" />
            </Fragment>
        )
    }
}

const mapStateToProps = state => {
    const { loading, errors, data } = state.gallery.delete;
    return { loading, errors, data }
};

export default connect(mapStateToProps, { delete_entity_image })(DeleteModal);