import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, FAB, Button, Modal, InputForm, Grid, Column, Table} from "../../../components";
import _ from 'lodash';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import { MoonLoader } from 'react-spinners';
import styles from '../styles.scss';
import {get_single_entity, save_entity_address} from "../../../redux/actions";

class Locations extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAdd: false,
            name: '',
            phone: '',
            email: '',
            street: '',
            options: '',
            city: '',
            state: '',
            zip_code: ''
        }
    }
    componentDidMount() {
        this.props.get_single_entity();
    }
    renderAdd(){
        return (
            <Modal open={this.state.modalAdd}>
                <form>
                    <Grid classNames={[styles.grid]}>
                        <Column>
                            <InputForm labelText="LOCATION NAME" value={this.state.name} placeholder="Downtown Branch" onChange={(name)=>{this.setState({ name });}} />
                            <InputForm labelText="CONTACT NUMBER" type="tel" value={this.state.phone} onChange={(phone)=>{this.setState({ phone });}} />
                            <InputForm labelText="CONTACT EMAIL" type="email" value={this.state.email} onChange={(email)=>{this.setState({ email });}} />
                            <InputForm labelText="STREET ADDRESS" type="text" value={this.state.street} onChange={(street)=>{this.setState({ street });}} />
                            <InputForm labelText="APT, SUITE, BLDG. (OPTIONAL)" type="text" value={this.state.options} onChange={(options)=>{this.setState({ options });}} />
                        </Column>
                        <Column xsSize={12} mSize={4}>
                            <InputForm labelText="CITY" type="text" value={this.state.city} onChange={(city)=>{this.setState({ city });}} />
                        </Column>
                        <Column xsSize={12} mSize={4}>
                            <InputForm labelText="STATE" type="text" value={this.state.state} onChange={(state)=>{this.setState({ state });}} />
                        </Column>
                        <Column xsSize={12} mSize={4}>
                            <InputForm labelText="ZIP CODE" type="text" value={this.state.zip_code} onChange={(zip_code)=>{this.setState({ zip_code });}} />
                        </Column>
                    </Grid>
                    <div className={styles.mt_1}>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalAdd: false })}} dark>Cancel</Button>
                    </div>
                </form>
            </Modal>
        )
    }
    render() {
        // const subs = [
        //     (<FontAwesomeIcon style={{fontSize: 25}} icon={faPlus} />),
        //     (<FontAwesomeIcon style={{fontSize: 25}} icon={faPlus} />),
        //     (<FontAwesomeIcon style={{fontSize: 25}} icon={faPlus} />),
        //     (<FontAwesomeIcon style={{fontSize: 25}} icon={faPlus} />),
        //     (<FontAwesomeIcon style={{fontSize: 25}} icon={faPlus} />),
        // ];
        const {entity, loading } = this.props;
        return (
            <PanelContainer title="Settings / Locations">
                {(loading) ? (
                    <div className={styles.loading}>
                        <MoonLoader
                            sizeUnit={"px"}
                            size={30}
                            color={'#24334A'}
                            loading={loading}
                        />
                    </div>
                ) : (entity !== null) ? (
                    <Table rowLinked>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>ADDRESS</th>
                        </tr>
                        </thead>
                        <tbody>
                        { (entity.entity_addresses.length > 0) ? _.map(entity.entity_addresses, (address, key) => (
                            <tr key={key} onClick={()=>{ this.setState({ modalAdd: true, itemEdit: staff })}}>
                                <td>{key+1}</td>
                                <td>{address.address}</td>
                            </tr>
                        )) : <p>داده ای یافت نشد.</p>}
                        </tbody>
                    </Table>
                ) : null}

                {this.renderAdd()}
                {/*<FAB subs={subs}>*/}
                <FAB dark>
                    <div className={styles.fab_link} onClick={()=>{this.setState({ modalAdd: true })}}>
                        <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                    </div>
                </FAB>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    const {
        entity,
        get_loading
    } = state.entity;
    return {
        entity,
        loading: get_loading
    };
};

export default connect(mapStateToProps, { get_single_entity, save_entity_address })(Locations);