import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import { PulseLoader } from 'react-spinners';
import _ from "lodash";
import {
    Button,
    ButtonGroup,
    Column,
    Grid,
    InputForm, Label,
    PanelContainer,
    SelectForm,
    TextAreaForm
} from "../../../components";
import styles from '../styles.scss';
import {get_single_entity, get_entity_categories, save_entity, save_entity_address} from "../../../redux/actions";
import {DEFAULT_IMAGE} from "../../../res/Images";

class CompanyDetails extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            setData: false,
            title: '',
            description: '',
            short_description: '',
            category_id: 0,
            address_id: 0,
            address: '',
            website: '',
            phone: '',
            country: 0,
            currency: 0,
            medias: [],
            images: [],
        };
        this.fileUpload = React.createRef();
    }
    componentDidMount() {
        this.props.get_single_entity();
        this.props.get_entity_categories();
    }
    componentDidUpdate() {
        const { entity } = this.props;
        if(entity !== null && !this.state.setData) {
            const {title, description, short_description, category_id, entity_addresses, entity_slider_medias} = entity;
            if(entity_addresses.length > 0) {
                this.setState({
                    address: entity_addresses[0].address,
                    address_id: entity_addresses[0].id,
                })
            }
            if(entity_slider_medias.length > 0) {
                this.setState({
                    medias: entity_slider_medias
                })
            }
            this.setState({
                title,
                description,
                short_description,
                category_id,
                setData: true
            }, () => {
                console.log(entity);
            });
        }
    }
    onSubmit() {
        const {
            title,
            description,
            short_description,
            category_id,
            address,
            address_id,
        } = this.state;
        const body = {
            title,
            description,
            short_description,
            category_id,
        };
        this.props.save_entity_address({ entity_address_id: address_id, address });
        this.props.save_entity(body);
        // if(this.state.images.length > 0)
        //     this.props.save_entity_image(this.state.images);
    }
    // removeImage(img, ind) {
    //     const { images } = this.state;
    //     images.splice(ind, 1);
    //     this.setState({ images })
    // }
    // addImage(event) {
    //     const { images } = this.state;
    //     console.log(event.target.files[0])
    //     images.push({ src: URL.createObjectURL(event.target.files[0]), file: event.target.files[0] });
    //     this.setState({ images });
    // }
    render() {
        const countries = [
            { val: 1, text: 'Iran' },
            { val: 2, text: 'America' }
        ];
        const currencies = [
            { val: 1, text: 'ریال' },
            { val: 2, text: 'dollar' }
        ];
        const { loading, category_loading } = this.props;
        const categories = _.map(this.props.categories, category => {
            return { val: category.id, text: category.title};
        });
        console.log(this.state.medias)
        return (
            <PanelContainer title="Settings / Company Details">
                <Grid classNames={[styles.settings_page]}>
                    <Column xsSize={12} mSize={6}>
                        <InputForm labelText="BUSINESS NAME" value={this.state.title} onChange={(title)=>{this.setState({ title });}} />
                        <TextAreaForm labelText="DESCRIPTION" value={this.state.description} onChange={(description)=>{this.setState({ description });}} />
                        <TextAreaForm labelText="SHORT DESCRIPTION" rows={2} value={this.state.short_description} onChange={(short_description)=>{this.setState({ short_description });}} />
                        <TextAreaForm labelText="ADDRESS" value={this.state.address} onChange={(address)=>{this.setState({ address });}} />
                        <InputForm type="url" labelText="WEBSITE" placeholder="https://example.com" value={this.state.website} onChange={(website)=>{this.setState({ website });}} />
                        <InputForm type="tel" labelText="CONTACT NUMBER" placeholder="Business Contact Number" value={this.state.phone} onChange={(phone)=>{this.setState({ phone });}} />
                        <SelectForm  labelText="BUSINESS TYPE" id="category_id" disabled={category_loading} options={categories} onChange={(category_id)=>{ this.setState({ category_id })}} selected={this.state.category_id}  />
                        <SelectForm labelText="COUNTRY" selected={this.state.country} options={countries} onChange={(country)=>{this.setState({ country });}} />
                        <SelectForm labelText="CURRENCY" selected={this.state.currency} options={currencies} onChange={(currency)=>{this.setState({ currency });}} />
                    </Column>
                    {/*<Column xsSize={12} mSize={6} >*/}
                        {/*<Label>GALLERY</Label>*/}
                        {/*<div className={styles.gallery}>*/}
                            {/*<div className={styles.default_img_container}>*/}
                                {/*<img src={DEFAULT_IMAGE} className={styles.img_default} />*/}
                                {/*<ButtonGroup classNames={[styles.actions]}>*/}
                                    {/*<Button refs={this.fileUpload}*/}
                                            {/*classNames={[styles.upload_btn]}*/}
                                            {/*onChange={this.addImage.bind(this)}*/}
                                            {/*type="file"*/}
                                            {/*accept="image/*"*/}
                                            {/*title="CHOOSE IMAGE" />*/}
                                {/*</ButtonGroup>*/}
                            {/*</div>*/}
                            {/*{_.map(this.state.medias, (media, key) => (*/}
                                {/*<div className={styles.img_container} key={key}>*/}
                                    {/*<img src={media.url_thumbnail} />*/}
                                    {/*<ButtonGroup classNames={[styles.actions]}>*/}
                                        {/*<Button small danger title="DELETE" onPress={this.removeMedia.bind(this, media, key)} />*/}
                                    {/*</ButtonGroup>*/}
                                {/*</div>*/}
                            {/*))}*/}
                            {/*{_.map(this.state.images, (img, key) => (*/}
                                {/*<div className={styles.img_container} key={key}>*/}
                                    {/*<img src={img.src} />*/}
                                    {/*<ButtonGroup classNames={[styles.actions]}>*/}
                                        {/*<Button small danger title="DELETE" onPress={this.removeImage.bind(this, img, key)} />*/}
                                        {/*<Button classNames={[styles.upload_btn]} type="file" small title="CHOOSE IMAGE" />*/}
                                    {/*</ButtonGroup>*/}
                                {/*</div>*/}
                            {/*))}*/}
                        {/*</div>*/}
                    {/*</Column>*/}
                    <Column classNames={[styles.mt_1]}>
                        {(loading) ?
                            <Button type="button" full dark classNames={[styles.btnLoading]}>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>
                            :
                            <Button dark type="button" onPress={this.onSubmit.bind(this)}>Save Changes</Button>
                        }
                    </Column>
                </Grid>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const {
        category_loading,
        category_error,
        categories,
        edit_loading,
        entity
    } = state.entity;
    return {
        entity_loading: edit_loading,
        entity,
        category_loading,
        category_error,
        categories,
    };
};

export default connect(mapStateToProps, { get_single_entity, save_entity, get_entity_categories, save_entity_address })(CompanyDetails);