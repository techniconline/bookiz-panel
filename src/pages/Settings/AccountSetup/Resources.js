import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, FAB, Button, Modal, InputForm, Table, TextAreaForm} from "../../../components/index";
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import styles from '../styles.scss';

class Resources extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            modalAdd: false,
            name: '',
            description: ''
        }
    }
    renderAdd(){
        return (
            <Modal open={this.state.modalAdd}>
                <form>
                    <InputForm labelText="RESOURCE NAME" value={this.state.name} placeholder="Downtown Branch" onChange={(name)=>{this.setState({ name });}} />
                    <TextAreaForm labelText="DESCRIPTION" value={this.state.description} onChange={(description)=>{this.setState({ description });}} />
                    <div className={styles.mt_1}>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalAdd: false })}} light>Cancel</Button>
                    </div>
                </form>
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer title="Settings / Resources">
                <div classNames={[styles.settings_page]} >
                    <Table>
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>RESOURCE NAME</th>
                            <th>DESCRIPTION</th>
                            <th>SERVICES ASSIGNED</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td>1</td>
                            <td>beautytime</td>
                            <td>Default Address, Default City</td>
                            <td>0</td>
                        </tr>
                        </tbody>
                    </Table>
                    {this.renderAdd()}
                    <FAB>
                        <div className={styles.fab_link} onClick={()=>{this.setState({ modalAdd: true })}}>
                            <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                        </div>
                    </FAB>
                </div>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(Resources);