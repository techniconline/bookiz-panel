import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, SelectForm, Button, Column, Grid, Checkbox, Select, TextAreaForm} from "../../../components/index";
import styles from '../styles.scss';

class OnlineBooking extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            color: 1,
            time: 1,
            defaultView: 1,
            startDay: 1,
            display: ''
        }
    }
    render() {
        const colors = [
            {val: 1, text: 'By service group'},
            {val: 2, text: 'By service group 2'}
        ];
        const timeSlotInterval = [
            {val: 1, text: '15 Minutes'},
            {val: 2, text: '30 Minutes'}
        ];
        const defaultView = [
            {val: 1, text: 'Day'},
            {val: 2, text: 'Week'}
        ];
        const startDay = [
            {val: 1, text: 'Sunday'},
            {val: 2, text: 'Monday'},
            {val: 3, text: 'Tuesday'},
            {val: 4, text: 'Wednesday'},
            {val: 5, text: 'Thursday'},
            {val: 6, text: 'Friday'},
            {val: 7, text: 'Saturday'}
        ];
        return (
            <PanelContainer title="Settings / Online Booking">
                <Grid classNames={[styles.settings_page]} >
                    <Column xsSize={12} mSize={6}>
                        <form>
                            <Grid classNames={[styles.box]}>
                                <Column xsSize={12} mSize={2} lSize={1}>
                                    <Checkbox />
                                </Column>
                                <Column xsSize={12} mSize={10} lSize={11}>
                                    <h2>Online Booking Tools</h2>
                                    <p>Receive online bookings from clients directly through your website, Facebook and more.</p>
                                </Column>
                            </Grid>

                            <div className={styles.mt_1}>
                                <h4>Booking Availability</h4>
                                <div className={styles.OB_text}>
                                    <p>Clients can book appointments up to </p>
                                    <div>
                                        <Select selected={this.state.time} options={timeSlotInterval} onChange={(time)=>{this.setState({ time });}}/>
                                    </div>
                                    <p> in advance.</p>
                                </div>
                            </div>
                            <div className={styles.OB_text}>
                                <p>Clients can book appointments no more than </p>
                                <div>
                                    <Select selected={this.state.time} options={timeSlotInterval} onChange={(time)=>{this.setState({ time });}}/>
                                </div>
                                <p>  in the future.</p>
                            </div>

                            <div className={styles.mt_1}>
                                <h4>Cancellation Notice</h4>
                                <div className={styles.OB_text}>
                                    <p>Allow clients to cancel up to </p>
                                    <div>
                                        <Select selected={this.state.time} options={timeSlotInterval} onChange={(time)=>{this.setState({ time });}}/>
                                    </div>
                                    <p> in advance.</p>
                                </div>
                            </div>

                            <div className={styles.mt_1}>
                                <h4>Booking Policy</h4>
                                <TextAreaForm labelText="Display custom booking policy info" placeholder="e.g please arrive 10 mins early" value={this.state.display} onChange={(display)=>{this.setState({ display });}} />
                            </div>

                            <div className={styles.mt_1}>
                                <Checkbox title="ALLOW CLIENTS TO SELECT STAFF MEMBERS" defaultChecked />
                            </div>

                            <div className={styles.mt_1}>
                                <Button dark>Save Changes</Button>
                            </div>
                        </form>
                    </Column>
                </Grid>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(OnlineBooking);