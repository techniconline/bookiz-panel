import React from 'react';
import { connect } from 'react-redux';
import {PanelContainer, SelectForm, Button, Column, Grid } from "../../../components/index";
import styles from '../styles.scss';

class CalendarSettings extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            color: 1,
            time: 1,
            defaultView: 1,
            startDay: 1
        }
    }
    render() {
        const colors = [
            {val: 1, text: 'By service group'},
            {val: 2, text: 'By service group 2'}
        ];
        const timeSlotInterval = [
            {val: 1, text: '15 Minutes'},
            {val: 2, text: '30 Minutes'}
        ];
        const defaultView = [
            {val: 1, text: 'Day'},
            {val: 2, text: 'Week'}
        ];
        const startDay = [
            {val: 1, text: 'Sunday'},
            {val: 2, text: 'Monday'},
            {val: 3, text: 'Tuesday'},
            {val: 4, text: 'Wednesday'},
            {val: 5, text: 'Thursday'},
            {val: 6, text: 'Friday'},
            {val: 7, text: 'Saturday'}
        ];
        return (
            <PanelContainer title="Settings / Calendar">
                <Grid >
                    <Column xsSize={12} mSize={6} lSize={4}>
                        <form>
                            <SelectForm labelText="APPOINTMENT COLORS" selected={this.state.color} options={colors} onChange={(color)=>{this.setState({ color });}} />
                            <SelectForm labelText="TIME SLOT INTERVAL" selected={this.state.time} options={timeSlotInterval} onChange={(time)=>{this.setState({ time });}} />
                            <SelectForm labelText="DEFAULT VIEW" selected={this.state.defaultView} options={defaultView} onChange={(defaultView)=>{this.setState({ defaultView });}} />
                            <SelectForm labelText="WEEK START DAY" selected={this.state.startDay} options={startDay} onChange={(startDay)=>{this.setState({ startDay });}} />
                            <div className={styles.mt_1}>
                                <Button dark>Save Changes</Button>
                            </div>
                        </form>
                    </Column>
                </Grid>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(CalendarSettings);