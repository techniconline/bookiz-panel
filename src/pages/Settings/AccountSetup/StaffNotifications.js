import React from 'react';
import { connect } from 'react-redux';
import {
    PanelContainer,
    Button,
    Column,
    Grid,
    Checkbox,
    Input
} from "../../../components/index";
import styles from '../styles.scss';

class StaffNotifications extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            email: ''
        }
    }
    render() {
        return (
            <PanelContainer title="Settings / Online Booking">
                <Grid classNames={[styles.settings_page]} >
                    <Column xsSize={12} mSize={6}>
                        <form>
                            <Grid classNames={[styles.box]}>
                                <Column xsSize={12} mSize={2} lSize={1}>
                                    <Checkbox />
                                </Column>
                                <Column xsSize={12} mSize={10} lSize={11}>
                                    <h2>Enable Staff Notifications</h2>
                                    <p>Automatically send booking notifications to staff members.</p>
                                </Column>
                            </Grid>
                            <h4 className={styles.mt_1}>New online bookings</h4>
                            <p >Send email notification with booking details each time an online booking is made</p>

                            <div className={styles.mt_1}>
                                <Checkbox title="SEND TO STAFF MEMBERS BOOKED" />
                            </div>

                            <div className={styles.mt_1}>
                                <Checkbox title="SEND TO SPECIFIC EMAIL ADDRESS" defaultChecked />
                            </div>

                            <div className={styles.mt_1}>
                                <Input type="email" title={this.state.email} onChange={(email)=>{ this.setState({ email }); }} />
                            </div>

                            <div className={styles.mt_1}>
                                <Button dark>Save Changes</Button>
                            </div>
                        </form>
                    </Column>
                </Grid>
            </PanelContainer>
        );
    }
}

const mapStateToProps = state => {
    return {};
};

export default connect(mapStateToProps, null)(StaffNotifications);