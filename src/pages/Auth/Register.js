import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { PulseLoader } from 'react-spinners';
import _ from 'lodash';
import { register } from '../../redux/actions';
import styles from './styles.scss';
import {Alert, Button, InputForm} from "../../components";
import {faTachometerAlt} from "@fortawesome/free-solid-svg-icons/index";


class Register extends React.Component {
    state = {
        connector: '',
        password: '',
        password_confirmation: '',
        first_name: '',
        last_name: '',
        push_id: '',
    };
    onSubmit() {
        const {connector , password, password_confirmation, first_name, last_name, wstoken, push_id} = this.state;
        this.props.register({connector , password,  password_confirmation, first_name, last_name, wstoken, push_id});
    }
    renderErrors() {
        const { errors } = this.props;
        return (errors !== null) ? (
            <Alert danger>
                {_.map(errors, (error, key) => <p className={styles.alert_item} key={key}>{error[0]}</p>)}
            </Alert>
        ) : null;
    }
    render() {
        const {status, loading, token} = this.props;
        if(status === 'dashboard' && !loading && token !== null) {
            return <Redirect to='/#/dashboard'/>;
        }

        return (
            <div className={styles.loginPage}>
                <div className={styles.container}>
                    <h1>Dashboard Account</h1>
                    <h3>Register</h3>
                    <form className={styles.formContainer}>
                        {this.renderErrors()}
                        <InputForm id="connector" onChange={(connector)=>{ this.setState({ connector })}} value={this.state.connector} type="email" labelText="Email Address"  />
                        <InputForm id="firstName" onChange={(first_name)=>{ this.setState({ first_name })}} value={this.state.first_name} type="text" labelText="First Name"  />
                        <InputForm id="lastName" onChange={(last_name)=>{ this.setState({ last_name })}} value={this.state.last_name} type="text" labelText="Last Name"  />
                        <InputForm id="password" onChange={(password)=>{ this.setState({ password })}} value={this.state.password} type="password" labelText="Password"  />
                        <InputForm id="confirmPassword" onChange={(password_confirmation)=>{ this.setState({ password_confirmation })}} value={this.state.password_confirmation} type="password" labelText="Confirm Password"  />
                        {(loading) ?
                            <Button type="button" full dark classNames={[styles.btnLoading]}>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>
                            :
                            <Button type="button" onPress={this.onSubmit.bind(this)} title="Submit" full dark />
                        }
                    </form>
                    <p className={styles.link}>Already have an account? <Link to="/login">Login</Link></p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        loading,
        status,
        token,
        register_errors
    } = state.auth;
    return {
        loading,
        status,
        token,
        errors: register_errors
    }
};

export default connect(mapStateToProps, { register })(Register);