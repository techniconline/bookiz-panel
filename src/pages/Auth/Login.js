import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { PulseLoader } from 'react-spinners';
import { login } from '../../redux/actions';
import styles from './styles.scss';
import {Button, InputForm, Alert} from "../../components";
import _ from "lodash";
import {HAS_ENTITY} from "../../redux/types";

class Login extends React.Component {
    state = {
        connector: '',
        password: '',
        on_submit: false
    };
    onSubmit() {
        this.setState({ on_submit: true });
        this.props.login(this.state.connector, this.state.password, true);

    }
    renderErrors() {
        const { errors } = this.props;
        return (errors !== null) ? (
            <Alert danger>
                {_.map(errors, (error, key) => <p className={styles.alert_item} key={key}>{error[0]}</p>)}
            </Alert>
        ) : null;
    }
    render() {
        const {status, loading, token} = this.props;
        if(!loading && token !== null) {

            if(status === 'dashboard' && localStorage.getItem(HAS_ENTITY) === 'true') {
                return (!this.state.on_submit) ? <Redirect to='/dashboard'/> : null;
            }else if(status === 'register_entity' || localStorage.getItem(HAS_ENTITY)) {
                return <Redirect to='/register_entity'/>;
            }
        }

        return (
            <div className={styles.loginPage}>
                <div className={styles.container}>
                    <h1>Dashboard Account</h1>
                    <h3>Login</h3>
                    <form className={styles.formContainer}>
                        {this.renderErrors()}
                        <InputForm id="connector" onChange={(connector)=>{ this.setState({ connector })}} value={this.state.connector} type="email" labelText="Email Address"  />
                        <InputForm id="password" onChange={(password)=>{ this.setState({ password })}} value={this.state.password} type="password" labelText="Password"  />
                        {(this.props.loading) ?
                            <Button type="button" full dark classNames={[styles.btnLoading]}>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>
                        :
                            <Button type="button" onPress={this.onSubmit.bind(this)} title="Submit" full dark />
                        }
                    </form>
                    <Link to="/forgot" className={styles.link}>Forgot Password?</Link>
                    <p className={styles.link}>Don’t have an account? <Link to="/register">Register</Link></p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        loading,
        status,
        token,
        login_errors
    } = state.auth;
    return {
        loading,
        status,
        token,
        errors: login_errors
    }
};

export default connect(mapStateToProps, { login })(Login);