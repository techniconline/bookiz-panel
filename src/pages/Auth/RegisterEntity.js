import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { PulseLoader } from 'react-spinners';
import { logout, save_entity, get_entity_categories } from '../../redux/actions';
import styles from './styles.scss';
import {Button, InputForm, Alert, TextAreaForm, SelectForm} from "../../components";
import _ from "lodash";
import {HAS_ENTITY} from "../../redux/types";

class RegisterEntity extends React.Component {
    state = {
        title: '',
        description: '',
        short_description: ''
    };
    componentDidMount() {
        this.props.get_entity_categories();
    }
    onSubmit() {
        const { title, description, short_description} = this.state;
        const body = {
            category_id: 1,
            parent_id: '',
            title,
            description,
            short_description,
            categories: [2]
        };
        this.props.save_entity(body, true);
    }
    renderErrors() {
        const { errors } = this.props;
        return (errors !== null) ? (
            <Alert danger>
                {_.map(errors, (error, key) => <p className={styles.alert_item} key={key}>{error[0]}</p>)}
            </Alert>
        ) : null;
    }
    onLogout() {
        this.props.logout();
    }
    render() {
        const {status, loading, token, category_loading, category_error} = this.props;
        if(localStorage.getItem(HAS_ENTITY) === 'true' && token !== null) {
            return <Redirect to='/dashboard'/>;
        }else if(token === null) {
            return <Redirect to='/login'/>;
        }
        if(status === 'dashboard' && !loading && token !== null) {
            return <Redirect to='/dashboard'/>;
        }
        const categories = _.map(this.props.categories, category => {
            return { val: category.id, text: category.title};
        });
        return (!this.props.loggedIn) ? <Redirect to="/login" /> : (
            <div className={styles.loginPage}>
                <div className={styles.container}>
                    <h1>Register Entity</h1>
                    <form className={styles.formContainer}>
                        {this.renderErrors()}
                        <InputForm id="title" onChange={(title)=>{ this.setState({ title })}} value={this.state.title} labelText="BUSINESS NAME"  />
                        <TextAreaForm id="description" rows={5} onChange={(description)=>{ this.setState({ description })}} value={this.state.description} labelText="DESCRIPTION"  />
                        <TextAreaForm id="short_description" rows={2} onChange={(short_description)=>{ this.setState({ short_description })}} value={this.state.short_description} labelText="SHORT DESCRIPTION"  />
                        <SelectForm id="category_id" disabled={category_loading} options={categories} onChange={(category_id)=>{ this.setState({ category_id })}} selected={this.state.category_id} labelText="CATEGORY"  />
                        {(this.props.loading) ?
                            <Button type="button" full dark classNames={[styles.btnLoading]}>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>
                            :
                            <Button type="button" onPress={this.onSubmit.bind(this)} title="Submit" full dark />
                        }
                        <Button type="button" onPress={this.onLogout.bind(this)} title="Logout" full dark />
                    </form>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        save_loading,
        status,
        save_errors,
        category_loading,
        category_error,
        categories,
    } = state.entity;
    const { token } = state.auth;
    return {
        loading: save_loading,
        loggedIn: state.auth.loggedIn,
        status,
        token,
        errors: save_errors,
        category_loading,
        category_error,
        categories,
    }
};

export default connect(mapStateToProps, { logout, save_entity, get_entity_categories })(RegisterEntity);