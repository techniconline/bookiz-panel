import React from 'react';
import { connect } from 'react-redux';
import { Link, Redirect } from 'react-router-dom';
import { PulseLoader } from 'react-spinners';
import { forgot } from '../../redux/actions';
import styles from './styles.scss';
import {Button, InputForm, Alert} from "../../components";
import _ from "lodash";

class Forgot extends React.Component {
    state = {
        connector: ''
    };
    onSubmit() {
        this.props.forgot(this.state.connector);
    }
    renderAlert() {
        const { errors, success_message } = this.props;
        return (errors !== null) ? (
            <Alert danger>
                {_.map(errors, (error, key) => <p className={styles.alert_item} key={key}>{error[0]}</p>)}
            </Alert>
        ) : (success_message !== null) ?
            <Alert success>
                <p className={styles.alert_item}>{success_message}</p>
            </Alert>
        : null;
    }
    render() {
        const {status, loading, token} = this.props;
        if(status === 'dashboard' && !loading && token !== null) {
            return <Redirect to='/dashboard'/>;
        }

        return (
            <div className={styles.loginPage}>
                <div className={styles.container}>
                    <h1>Dashboard Account</h1>
                    <h3>Reset Password</h3>
                    <form className={styles.formContainer}>
                        {this.renderAlert()}
                        <InputForm id="connector" onChange={(connector)=>{ this.setState({ connector })}} value={this.state.connector} type="email" labelText="Email Address"  />
                        {(this.props.loading) ?
                            <Button type="button" full dark classNames={[styles.btnLoading]}>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>
                        :
                            <Button type="submit" onPress={this.onSubmit.bind(this)} title="Submit" full dark />
                        }
                    </form>
                    <p className={styles.link}>Don’t want to reset password? Go to <Link to="/login">Login</Link></p>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        loading,
        status,
        token,
        forgot_errors,
        success_message
    } = state.auth;
    return {
        loading,
        status,
        token,
        success_message,
        errors: forgot_errors
    }
};

export default connect(mapStateToProps, { forgot })(Forgot);