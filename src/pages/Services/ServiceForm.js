import React, {Fragment} from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {
    Button,
    ButtonGroup,
    Checkbox,
    FullLoading,
    InputForm,
    SelectForm,
    Tab,
    Tabs,
    TextAreaForm
} from "../../components";
import styles from './styles.scss';
import { get_service_categories, save_service, delete_service, get_members } from '../../redux/actions';
import {PulseLoader} from "react-spinners";

class ServiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            onSave: false,
            onDelete: false,
            isEdit: false,
            title: '',
            price: 0,
            entity_service_id: 1,
            employees: []
        }
    }
    componentDidMount() {
        if(this.props.categories.length === 0)
            this.props.get_service_categories();
        if(this.props.staffs.length === 0)
            this.props.get_members();
        if(this.props.data !== null) {
            const { title, price, entity_service_id } = this.props.data;
            this.setState({ isEdit: true, title, price, entity_service_id });
        }
    }
    componentDidUpdate() {
        if(this.state.onDelete && this.props.delete_status)
            this.props.onDismiss();
        if(this.state.onSave && !this.props.save_loading) {
            this.props.onDismiss();
        }
    }
    onSave() {
        const { isEdit, title, price, entity_service_id, employees } = this.state;
        const body = {
            title,
            price,
            entity_service_id
        };
        if(isEdit) body['entity_relation_service_id'] = this.props.data.id;
        setTimeout(() => {
            this.setState({ onSave: true });
        }, 100);
        this.props.save_service(body, employees, isEdit);
    }
    onDelete() {
        this.props.delete_service(this.props.data.id);
        this.setState({ onDelete: true });
    }
    allStaff() {
        const { staffs } = this.props;
        let { employees } = this.state;
        if(staffs.length > 0) {

            if(staffs.length !== employees.length) {
                employees = [];
                _.map(staffs, ({id}) => {
                    employees.push(id);
                });
            }else {
                employees = [];
            }
            this.setState({ employees });
        }
    }
    selectStaff(item) {
        let { employees } = this.state;
        if(employees.indexOf(item.id) === -1) {
            employees.push(item.id);
        }else {
            employees = _.filter(employees, id => id !== item.id);
        }
        this.setState({ employees });
    }
    render() {
        const { categories, category_loading, delete_loading, save_loading, staff_loading, staffs } = this.props;
        const { employees } = this.state;
        // const types = _.map(categories , category => {
        //     return { val: category.id,  text: category.title, hide: category.parent_id === null };
        // });
        const types = _.map(categories , category => {
            const data = _.map(category.childes , category => {
                return { val: category.id,  text: category.title };
            });
            return { label: category.title, data };
        });
        return (
            <div>
                <Tabs>
                    <Tab heading="DETAILS">
                        <div className={styles.pt_1}>
                            <InputForm labelText="SERVICE NAME" value={this.state.title} placeholder="e.g Hair Services" onChange={(title)=>{this.setState({ title });}} />
                            <InputForm labelText="SERVICE PRICE" value={this.state.price} placeholder="10.0" onChange={(price)=>{this.setState({ price });}} />
                            <SelectForm labelText="SERVICE TYPE" disabled={category_loading} options={types} selected={this.state.entity_service_id} onChange={(entity_service_id)=>{this.setState({ entity_service_id });}} />
                            <p className={styles.p_desc}>Choose the most relevant type, for internal reference and not currently visible to clients.</p>
                        </div>
                    </Tab>
                    <Tab heading="STAFF">
                        {(staff_loading) ? (<FullLoading />) : (
                            <Fragment>
                                <p className={styles.p_desc}>Select staff who perform this service.</p>
                                <div className={styles.staffs}>
                                    <Checkbox title="Select All"
                                              id="all_staff"
                                              checked={employees.length > 0 && employees.length === staffs.length}
                                              onChange={this.allStaff.bind(this)} />
                                    <div className={styles.divider} />
                                    {_.map(staffs, (staff, key) => (
                                        <Checkbox key={key} title={staff.user.full_name}
                                                  id={`staff_${staff.id}`}
                                                  checked={employees.indexOf(staff.id) !== -1}
                                                  onChange={this.selectStaff.bind(this, staff)} />
                                    ))}
                                </div>
                            </Fragment>
                        )}
                    </Tab>
                </Tabs>
                <div className={styles.mt_2}>
                    <ButtonGroup>
                        {(!save_loading) ? <Button dark disabled={delete_loading} onPress={this.onSave.bind(this)}>SAVE</Button> :
                            (<Button dark>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>)}
                        {(this.state.isEdit) ? (!delete_loading) ? <Button danger onPress={this.onDelete.bind(this)}>DELETE</Button> :
                            (<Button danger>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>) : null}
                        <Button disabled={delete_loading} onPress={this.props.onDismiss.bind(this)} >CANCEL</Button>
                    </ButtonGroup>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        category_loading,
        save_loading,
        categories,
        delete_loading,
        delete_errors,
        delete_status
    } = state.service;
    return {
        category_loading,
        save_loading,
        categories,
        delete_loading,
        delete_errors,
        delete_status,
        staff_loading: state.staff.loading,
        staff_errors: state.staff.errors,
        staffs: state.staff.data,
    }
};

export default connect(mapStateToProps, { get_service_categories, save_service, delete_service, get_members })(ServiceForm);