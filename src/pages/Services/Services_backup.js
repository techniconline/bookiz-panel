import React from 'react';
import {Grid, Column, PanelContainer, FAB, FABItem, Modal, Button, InputForm, ButtonGroup, TextAreaForm} from "../../components";
import styles from './styles.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import ServiceForm from "./ServiceForm";

class Services extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            modalNewGroup: false,
            modalNewService: false
        }
    }
    renderNewGroup(){
        return (
            <Modal open={this.state.modalNewGroup} onClose={()=>{ this.setState({ modalNewGroup: false })}} title="New Service Group">
                <InputForm labelText="GROUP NAME" value={this.state.name} placeholder="e.g Hair Services" onChange={(name)=>{this.setState({ name });}} />
                <TextAreaForm labelText="GROUP DESCRIPTION" value={this.state.description} onChange={(description)=>{this.setState({ description });}} />
                <div className={styles.mt_1}>
                    <ButtonGroup>
                        <Button dark>Save</Button>
                        <Button onPress={()=>{ this.setState({ modalNewGroup: false })}} dark>Cancel</Button>
                    </ButtonGroup>
                </div>
            </Modal>
        )
    }
    renderNewService(){
        return (
            <Modal open={this.state.modalNewService} onClose={()=>{ this.setState({ modalNewService: false })}} title="New Service">
                <ServiceForm onDismiss={()=>{ this.setState({ modalNewService: false })}} />
            </Modal>
        )
    }
    render() {
        const fabSubs = [
            // (
            //     <FABItem label="New Service Group" onClick={()=>{this.setState({ modalNewGroup: true })}}>
            //         <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
            //     </FABItem>
            // )
        ];
        return (
            <PanelContainer title="Services">
                <Grid spaceAround classNames={[styles.service]}>
                    {this.renderNewGroup()}
                    {this.renderNewService()}
                    <Column xsSize={12} mSize={6}>
                        <div className={styles.list}>
                            <div className={styles.group}>
                                <h4>Hair</h4>
                                <span>More</span>
                            </div>
                            <ul>
                                <li onClick={()=>{this.setState({ modalNewService: true })}}>
                                    <span className="name">Blow Dry</span>
                                    <span className="name">2018/11/01 12:10:11</span>
                                    <span className="name">25$</span>
                                </li>
                                <li onClick={()=>{this.setState({ modalNewService: true })}}>
                                    <span className="name">Haircut</span>
                                    <span className="name">2018/11/01 12:10:11</span>
                                    <span className="name">25$</span>
                                </li>
                            </ul>
                        </div>
                        <div className={styles.list}>
                            <div className={styles.group}>
                                <h4>Hair</h4>
                                <span>More</span>
                            </div>
                            <ul>
                                <li onClick={()=>{this.setState({ modalNewService: true })}}>
                                    <span className="name">Blow Dry</span>
                                    <span className="name">2018/11/01 12:10:11</span>
                                    <span className="name">25$</span>
                                </li>
                                <li onClick={()=>{this.setState({ modalNewService: true })}}>
                                    <span className="name">Haircut</span>
                                    <span className="name">2018/11/01 12:10:11</span>
                                    <span className="name">25$</span>
                                </li>
                            </ul>
                        </div>
                    </Column>
                </Grid>
                <FAB dark subs={fabSubs}>
                    <FABItem label="New Service" onClick={()=>{this.setState({ modalNewService: true })}}>
                        <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                    </FABItem>
                </FAB>
            </PanelContainer>
        )
    }
}

export default Services;