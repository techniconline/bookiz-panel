import React from 'react';
import { connect } from 'react-redux';
import { faPlus } from '@fortawesome/free-solid-svg-icons';
import {Grid, Column, PanelContainer, FAB, FABItem, Modal, Button, InputForm, ButtonGroup, TextAreaForm} from "../../components";
import styles from './styles.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import ServiceForm from "./ServiceForm";
import { get_all_services } from '../../redux/actions';

class Services extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            editData: null,
            modalNewService: false,
            modalEditService: false
        }
    }
    componentDidMount() {
        this.props.get_all_services();
    }
    renderNewService(){
        const { editData } = this.state;
        return (
            <Modal open={this.state.modalNewService} onClose={()=>{ this.setState({ modalNewService: false })}} title={(editData === null) ? 'New Service' : 'Edit Service'}>
                <ServiceForm onDismiss={()=>{ this.setState({ modalNewService: false })}} data={editData} />
            </Modal>
        )
    }
    render() {
        return (
            <PanelContainer title="Services">
                <Grid spaceAround classNames={[styles.service]}>
                    {this.renderNewService()}
                    <Column xsSize={12} mSize={6}>
                        {_.map(this.props.data, (item, key) => {
                            if(item.entity_relation_services.length === 0)
                               return null;
                            return (
                                <div className={styles.list} key={key}>
                                    <div className={styles.group}>
                                        <h4>{item.title}</h4>
                                        <span>More</span>
                                    </div>
                                    <ul>
                                        {_.map(item.entity_relation_services, (service, i) => (
                                            <li key={i} onClick={() => { this.setState({ modalNewService: true, editData: service }); }}>
                                                <span className="name">{service.title}</span>
                                                <span className="name">2018/11/01 12:10:11</span>
                                                <span className="name">{service.amount_text}</span>
                                            </li>
                                        ))}
                                    </ul>
                                </div>
                            )
                        })}
                    </Column>
                </Grid>
                <FAB dark>
                    <FABItem label="New Service" onClick={()=>{this.setState({ modalNewService: true, editData: null })}}>
                        <FontAwesomeIcon style={{fontSize: 20}} icon={faPlus} />
                    </FABItem>
                </FAB>
            </PanelContainer>
        )
    }
}

const mapStateToProps = state => {
    const { data, loading } = state.service;
    return {
        data,
        loading
    };
};

export default connect(mapStateToProps, { get_all_services })(Services);