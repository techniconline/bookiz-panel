import React from 'react';
import { connect } from 'react-redux';
import _ from 'lodash';
import {Button, ButtonGroup, Checkbox, InputForm, SelectForm, Tab, Tabs, TextAreaForm} from "../../components";
import styles from './styles.scss';
import { get_service_categories, save_service, delete_service } from '../../redux/actions';
import {PulseLoader} from "react-spinners";

class ServiceForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            onSave: false,
            onDelete: false,
            isEdit: false,
            title: '',
            price: 0,
            entity_service_id: 1
        }
    }
    componentDidMount() {
        this.props.get_service_categories();
        if(this.props.data !== null) {
            const { title, price, entity_service_id } = this.props.data;
            this.setState({ isEdit: true, title, price, entity_service_id });
        }
    }
    componentDidUpdate() {
        if(this.state.onDelete && this.props.delete_status)
            this.props.onDismiss();

    }
    onSave() {
        const { isEdit, title, price, entity_service_id } = this.state;
        const body = {
            title,
            price,
            entity_service_id
        };
        if(isEdit) body['entity_relation_service_id'] = this.props.data.id;

        this.props.save_service(body, isEdit);
        this.props.onDismiss();
    }
    onDelete() {
        this.props.delete_service(this.props.data.id);
        this.setState({ onDelete: true });
    }
    render() {
        const { categories, category_loading, delete_loading, delete_errors } = this.props;
        const types = _.map(categories , category => {
            return { val: category.id,  text: category.title };
        });

        const users = [
            { val: 1, text: 'Everyone' },
            { val: 2, text: 'Males only' },
            { val: 3, text: 'Females only' }
        ];
        return (
            <div>
                <Tabs>
                    <Tab heading="DETAILS">
                        <div className={styles.pt_1}>
                            <InputForm labelText="SERVICE NAME" value={this.state.title} placeholder="e.g Hair Services" onChange={(title)=>{this.setState({ title });}} />
                            <InputForm labelText="SERVICE PRICE" value={this.state.price} placeholder="10.0" onChange={(price)=>{this.setState({ price });}} />
                            <SelectForm labelText="SERVICE TYPE" disabled={category_loading} options={types} selected={this.state.entity_service_id} onChange={(entity_service_id)=>{this.setState({ entity_service_id });}} />
                            <p className={styles.p_desc}>Choose the most relevant type, for internal reference and not currently visible to clients.</p>
                        </div>
                    </Tab>
                    <Tab heading="STAFF">
                        <p className={styles.p_desc}>Select staff who perform this service.</p>
                        <div className={styles.staffs}>
                            <Checkbox title="Select All" />
                            <div className={styles.divider} />
                            <Checkbox title="ahmad khorshidi" />
                            <Checkbox title="Wendy Smith" />
                        </div>
                    </Tab>
                    <Tab heading="RESOURCES">
                        <p className={styles.p_desc}>Assign resources required to perform this service.</p>
                        <div className={styles.resources}>
                            <Checkbox title="Resource required" />
                            <p className={styles.p_desc}>When enabled, a combination of staff and resource will be needed to book the service</p>
                            <div className={styles.divider} />
                            <h4>beautytime</h4>
                            <Checkbox title="Massge Room" />
                        </div>
                    </Tab>
                    <Tab heading="ONLINE BOOKING">
                        <p className={styles.p_desc}>Allow clients to book this service online, switch off to hide this service from your online bookings menu.</p>
                        <div className={styles.online_booking}>
                            <Checkbox title="Enable online bookings" />
                            <TextAreaForm labelText="Service description" placeholder="Description will be displayed on your Fresha profile" value={this.state.description} onChange={(description)=>{this.setState({ description });}} />
                            <SelectForm labelText="Service available for" options={users} onChange={(user)=>{this.setState({ user });}} />
                        </div>
                    </Tab>
                </Tabs>
                <div className={styles.mt_2}>
                    <ButtonGroup>
                        <Button dark disabled={delete_loading} onPress={this.onSave.bind(this)}>SAVE</Button>
                        {(this.state.isEdit) ? (!delete_loading) ? <Button danger onPress={this.onDelete.bind(this)}>DELETE</Button> :
                            (<Button danger>
                                <PulseLoader
                                    sizeUnit={"px"}
                                    size={10}
                                    color={'#fff'}
                                    loading
                                />
                            </Button>) : null}
                        <Button disabled={delete_loading} dark onPress={this.props.onDismiss.bind(this)} >CANCEL</Button>
                    </ButtonGroup>
                </div>
            </div>
        )
    }
}

const mapStateToProps = state => {
    const {
        category_loading,
        categories,
        delete_loading,
        delete_errors,
        delete_status
    } = state.service;
    return {
        category_loading,
        categories,
        delete_loading,
        delete_errors,
        delete_status
    }
};

export default connect(mapStateToProps, { get_service_categories, save_service, delete_service })(ServiceForm);