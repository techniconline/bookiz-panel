import React, { Component } from 'react';
import { Link } from "react-router-dom";
import styles from "./styles.scss";
import { connect } from "react-redux";

class NotFound extends Component {
    constructor(props) {
        super(props);
    }
    render() {
        return (
            <div className={styles.not_found_page}>
                <div className={styles.container}>
                    <h1>404</h1>
                    <h3>PAGE NOT FOUND</h3>
                    {(this.props.loggedIn) ? (
                        <Link to="/dashboard" className={styles.link}>Go Dashboard</Link>
                    ) :  (
                        <Link to="/login" className={styles.link}>Go Login Page</Link>
                    )}
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => {
    const { loggedIn } = state.auth;
    return { loggedIn };
};

export default connect(mapStateToProps, null)(NotFound);