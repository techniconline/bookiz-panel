import React from 'react';
import { Link } from 'react-router-dom';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faEnvelope } from '@fortawesome/free-regular-svg-icons';
import styles from './styles.scss';
import {PanelContainer, Table} from "../../components";


class Messages extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            empty: false
        }
    }
    renderEmpty(){
        return (
            <div className={styles.empty}>
                <FontAwesomeIcon icon={faEnvelope} />
                <h1>No Recent Messages</h1>
                <p>Shedul will send automated customer notifications, reminders, confirmations and much more. Make the most of this feature by saving customer contact details in Shedul and give it a try!</p>
                <p>You can manage these features on the <Link to="/client_notifications">Client Notifications</Link> settings page.</p>
            </div>
        )
    }
    renderData() {
        return (
            <Table rowLinked headStriped>
                <thead>
                    <tr>
                        <th>ID #</th>
                        <th>Subject</th>
                        <th>Message</th>
                        <th>Date</th>
                    </tr>
                </thead>
                <tbody>
                    <tr onClick={()=>{console.log('clicked')}}>
                        <td>1</td>
                        <td>Something</td>
                        <td>Describe Something</td>
                        <td>2018/12/1</td>
                    </tr>
                </tbody>
            </Table>
        )
    }
    render(){
        return (
            <PanelContainer hasContent={false} title="Messages">
                <div className={styles.messages}>
                    {(this.state.empty) ? this.renderEmpty() : this.renderData()}
                </div>
            </PanelContainer>
        )
    }
}

export default Messages;