import React from 'react';
import {Button, PanelContainer, Grid, Column, Wrapper} from '../../components';
import styles from './styles.scss';

export default  () => (
    <PanelContainer title="Buttons" hasContent={false}>
        <Grid classNames={[styles.mt_1]}>
             <Column col={6} xsSize={12} mSize={6}>
                <Wrapper>
                    <h4 className={styles.mb_1}>Simple Button Example</h4>
                    <Button title="Primary" primary classNames={[styles.mr_1]} />
                    <Button title="Success" success classNames={[styles.mr_1]} />
                    <Button title="Dark" dark classNames={[styles.mr_1]} />
                    <Button title="Warning" warning classNames={[styles.mr_1]} />
                    <Button title="Danger" danger classNames={[styles.mr_1]} />
                    <Button title="Info" info classNames={[styles.mr_1]} />
                </Wrapper>
            </Column>
            <Column col={6} xsSize={12} mSize={6}>
                <Wrapper>
                    <h4 className={styles.mb_1}>Outline Button Example</h4>
                    <Button title="Primary" outline primary classNames={[styles.mr_1]} />
                    <Button title="Success" outline success classNames={[styles.mr_1]} />
                    <Button title="Dark" outline dark classNames={[styles.mr_1]} />
                    <Button title="Warning" outline warning classNames={[styles.mr_1]} />
                    <Button title="Danger" outline danger classNames={[styles.mr_1]} />
                    <Button title="Info" outline info classNames={[styles.mr_1]} />
                </Wrapper>
            </Column>
        </Grid>
        <Grid classNames={[styles.mt_1]}>
            <Column col={4} xsSize={12} smSize={6} mSize={4}>
                <Wrapper>
                    <h4 className={styles.mb_1}>Simple Button Example</h4>
                    <Button title="Primary" primary classNames={[styles.mr_1]} />
                    <Button title="Success" success classNames={[styles.mr_1]} />
                    <Button title="Dark" dark classNames={[styles.mr_1]} />
                    <Button title="Warning" warning classNames={[styles.mr_1]} />
                    <Button title="Danger" danger classNames={[styles.mr_1]} />
                    <Button title="Info" info classNames={[styles.mr_1]} />
                </Wrapper>
            </Column>
            <Column col={4} xsSize={12} smSize={6} mSize={4}>
                <Wrapper>
                    <h4 className={styles.mb_1}>Simple Button Example</h4>
                    <Button title="Primary" primary classNames={[styles.mr_1]} />
                    <Button title="Success" success classNames={[styles.mr_1]} />
                    <Button title="Dark" dark classNames={[styles.mr_1]} />
                    <Button title="Warning" warning classNames={[styles.mr_1]} />
                    <Button title="Danger" danger classNames={[styles.mr_1]} />
                    <Button title="Info" info classNames={[styles.mr_1]} />
                </Wrapper>
            </Column>
            <Column col={4} xsSize={12} smSize={6} mSize={4}>
                <Wrapper>
                    <h4 className={styles.mb_1}>Simple Button Example</h4>
                    <Button title="Primary" primary classNames={[styles.mr_1]} />
                    <Button title="Success" success classNames={[styles.mr_1]} />
                    <Button title="Dark" dark classNames={[styles.mr_1]} />
                    <Button title="Warning" warning classNames={[styles.mr_1]} />
                    <Button title="Danger" danger classNames={[styles.mr_1]} />
                    <Button title="Info" info classNames={[styles.mr_1]} />
                </Wrapper>
            </Column>
        </Grid>
    </PanelContainer>
)
