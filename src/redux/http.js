import React from 'react';
import PropTypes from 'prop-types';
import axios from 'axios';
import {ACCESS_TOKEN} from "./types";

export const API_URL = 'http://api.beautyday.ir/';
export const wstoken = 'e8c37c4ac2a3ab2d202cc8bc12584fd05521ee69';
export const application = 'owner_panel';
const Authorization = localStorage.getItem(ACCESS_TOKEN);

const getAuthorization = () => {
    return 'bearer ' + ((Authorization === null) ? localStorage.getItem(ACCESS_TOKEN) : Authorization);
};

export const x_get = (route, url = null) => {
    url = ((url !== null) ? url : API_URL + route) + '?wstoken=' + wstoken + '&application=' + application;
    return axios.get(url, { headers: {Authorization: getAuthorization()} });
};
export const get = (route, url = null, data = {}) => {
    url = (url !== null) ? url : API_URL + route;
    return axios.post(url, { _method: 'GET', wstoken, application, ...data}, { headers: {Authorization: getAuthorization()} });
};
get.propTypes = {
    route: PropTypes.string.isRequired,
    url: PropTypes.string,
};

export const post = (route, data = {}, url = null) => {
    url = (url !== null) ? url : API_URL + route;
    return axios.post(url, { ...data, wstoken, application}, { headers: {Authorization: getAuthorization()} });
};
post.propTypes = {
    route: PropTypes.string.isRequired,
    data: PropTypes.object,
    url: PropTypes.string,
};


export const upload = (route, url = null, file = null, body = {}) => {
    url = (url !== null) ? url : API_URL + route;
    const formData = new FormData();
    formData.append('file', file);
    formData.append('_method', 'POST');
    formData.append('wstoken', wstoken);
    formData.append('application', application);
    Object.keys(body).forEach(key => {
        formData.append(key, body[key]);
    });
    return axios.post(url, formData, { headers: {Authorization: getAuthorization(), 'content-type': 'multipart/form-data'} });
};
post.propTypes = {
    route: PropTypes.string.isRequired,
    data: PropTypes.object,
    url: PropTypes.string,
};

export const x_delete = (route, url = null) => {
    url = (url !== null) ? url : API_URL + route;
    return axios.post(url, { _method: 'DELETE', wstoken, application}, { headers: {Authorization: getAuthorization()} });
};
x_delete.propTypes = {
    route: PropTypes.string.isRequired,
    url: PropTypes.string,
};