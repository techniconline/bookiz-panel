/* ==========  Auth ========== */
export const ON_LOGOUT = 'ON_LOGOUT';
export const ON_LOGOUT_SUCCESS = 'ON_LOGOUT_SUCCESS';
export const SET_LOGIN = 'SET_LOGIN';
export const LOGIN_SUCCESS = 'LOGIN_SUCCESS';
export const LOGIN_ERROR = 'LOGIN_ERROR';
export const SET_REGISTER = 'SET_REGISTER';
export const REGISTER_SUCCESS = 'REGISTER_SUCCESS';
export const REGISTER_ERROR = 'REGISTER_ERROR';
export const SET_FORGOT = 'SET_FORGOT';
export const FORGOT_SUCCESS = 'FORGOT_SUCCESS';
export const FORGOT_ERROR = 'FORGOT_ERROR';
export const SET_TOKEN = 'SET_TOKEN';

export const ACCESS_TOKEN = '_token';
export const HAS_ENTITY = '_has_entity';
export const CURRENT_ENTITY = '_current_entity';
