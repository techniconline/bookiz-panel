import {
    DELETE_IMAGE,
    DELETE_IMAGE_SUCCESS,
    DELETE_IMAGE_FAILED,
    SAVE_IMAGE,
    SAVE_IMAGE_SUCCESS,
    SAVE_IMAGE_FAILED, CLEAN_GALLERY_ACTIONS
} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: [],
    errors: null,
    delete: {
        loading: false,
        data: null,
        errors: null
    },
    save: {
        loading: false,
        data: null,
        errors: null
    }
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case SAVE_IMAGE:
            return {
                ...state,
                save: {
                    ...state.save,
                    loading: true,
                    errors: null
                }
            };
        case SAVE_IMAGE_SUCCESS:
            return {
                ...state,
                save: {
                    ...state.save,
                    loading: false,
                    data: action.payload
                }
            };
        case SAVE_IMAGE_FAILED:
            return {
                ...state,
                save: {
                    ...state.save,
                    loading: false,
                    errors: action.payload
                }
            };
        case DELETE_IMAGE:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    loading: true,
                    errors: null
                }
            };
        case DELETE_IMAGE_SUCCESS:
            return {
                ...state,

                delete: {
                    ...state.delete,
                    loading: false,
                    data: action.payload
                }
            };
        case DELETE_IMAGE_FAILED:
            return {
                ...state,
                delete: {
                    ...state.delete,
                    loading: false,
                    errors: action.payload
                }
            };
        case CLEAN_GALLERY_ACTIONS:
            return {
                ...state,
                save: INITIAL_STATE.save,
                delete: INITIAL_STATE.delete,
            };
        default:
            return state;
    }
}
