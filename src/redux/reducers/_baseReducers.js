import {
    GET_ALL,
    GET_ALL_SUCCESS,
    GET_ALL_FAILED
} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: [],
    errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_ALL:
            return { ...state, loading: true };
        case GET_ALL_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_ALL_FAILED:
            return { ...state, loading: false, errors: action.payload };
        default:
            return state;
    }
}
