import {
    DASHBOARD_RECENT,
    DASHBOARD_RECENT_SUCCESS,
    DASHBOARD_RECENT_FAILED,
    DASHBOARD_UPCOMING,
    DASHBOARD_UPCOMING_SUCCESS,
    DASHBOARD_UPCOMING_FAILED, DASHBOARD_TOP_STAFF, DASHBOARD_TOP_STAFF_SUCCESS, DASHBOARD_TOP_STAFF_FAILED
} from "../types";

const INITIAL_STATE = {
    recent: {
        loading: false,
        data: [],
        errors: null
    },
    upcoming: {
        loading: false,
        data: [],
        errors: null
    },
    staff: {
        loading: false,
        data: [],
        errors: null
    }
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case DASHBOARD_RECENT:
            return {...state, recent: {...state.recent, loading: true}};
        case DASHBOARD_RECENT_SUCCESS:
            return {...state, recent: {...state.recent, loading: false, data: action.payload}};
        case DASHBOARD_RECENT_FAILED:
            return {...state, recent: {...state.recent, loading: false, errors: action.payload}};
        case DASHBOARD_UPCOMING:
            return {...state, upcoming: {...state.upcoming, loading: true}};
        case DASHBOARD_UPCOMING_SUCCESS:
            return {...state, upcoming: {...state.upcoming, loading: false, data: action.payload}};
        case DASHBOARD_UPCOMING_FAILED:
            return {...state, upcoming: {...state.upcoming, loading: false, errors: action.payload}};
        case DASHBOARD_TOP_STAFF:
            return {...state, staff: {...state.staff, loading: true}};
        case DASHBOARD_TOP_STAFF_SUCCESS:
            return {...state, staff: {...state.staff, loading: false, data: action.payload}};
        case DASHBOARD_TOP_STAFF_FAILED:
            return {...state, staff: {...state.staff, loading: false, errors: action.payload}};
        default:
            return state;
    }
}
