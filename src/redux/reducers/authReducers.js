import {
    SET_LOGIN,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    SET_TOKEN,
    SET_REGISTER,
    REGISTER_SUCCESS,
    REGISTER_ERROR, SET_FORGOT, FORGOT_SUCCESS, FORGOT_ERROR, ON_LOGOUT, ACCESS_TOKEN, ON_LOGOUT_SUCCESS
} from "../types";

const INITIAL_STATE = {
    loading: false,
    loggedIn: true,
    register_errors: null,
    login_errors: null,
    forgot_errors: null,
    success_message: null,
    token: null,
    user: null,
    status: 'login'
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case SET_LOGIN:
            return { ...state, login_errors: null, loading: true };
        case LOGIN_SUCCESS:
            return { ...state, loading: false, status: action.status, loggedIn: true, token: action.access_token };
        case LOGIN_ERROR:
            return { ...state, login_errors: action.payload, loading: false };
        case SET_REGISTER:
            return { ...state, register_errors: null, loading: true };
        case REGISTER_SUCCESS:
            return { ...state, loading: false, status: action.status, loggedIn: true, token: action.access_token };
        case REGISTER_ERROR:
            return { ...state, register_errors: action.payload, loading: false };
        case SET_FORGOT:
            return { ...state, forgot_errors: null, loading: true };
        case FORGOT_SUCCESS:
            return { ...state, loading: false, success_message: action.success_message };
        case FORGOT_ERROR:
            return { ...state, forgot_errors: action.payload, loading: false };
        case SET_TOKEN:
            return { ...state, token: action.payload, loggedIn: (action.payload), status: (action.payload) ? 'dashboard' : 'login'  };
        case ON_LOGOUT_SUCCESS:
            localStorage.removeItem(ACCESS_TOKEN);
            return { ...state, token: null, loggedIn: false, status: 'login' };
        default: 
            return state;
    }
}
