import {
    GET_INVOICE,
    GET_INVOICES,
    GET_INVOICES_SUCCESS,
    GET_INVOICES_FAILED,
    GET_INVOICE_SUCCESS,
    GET_INVOICE_FAILED
} from "../types";

const INITIAL_STATE = {
    loading: true,
    data: [],
    errors: null,
    pagination: {
        total: null,
        first_page_url: null,
        last_page_url: null,
        next_page_url: null,
        prev_page_url: null
    },
    single: {
        loading: false,
        data: null,
        errors: null
    },
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_INVOICES:
            return { ...state, loading: true };
        case GET_INVOICES_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_INVOICES_FAILED:
            return { ...state, loading: false, errors: action.payload };
        case GET_INVOICE:
            return {
                ...state,
                single: {
                    ...state.single,
                    loading: true,
                    data: null,
                    errors: null
                }
            };
        case GET_INVOICE_SUCCESS:
            return {
                ...state,
                single: {
                    ...state.single,
                    loading: false,
                    data: action.payload
                }
            };
        case GET_INVOICE_FAILED:
            return {
                ...state,
                single: {
                    ...state.single,
                    loading: false,
                    errors: action.payload
                }
            };
        default:
            return state;
    }
}
