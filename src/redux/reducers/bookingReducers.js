import {
    ACCEPT_BOOKING, ACCEPT_BOOKING_FAILED, ACCEPT_BOOKING_SUCCESS, CLEAN_BOOKING_ACTIONS,
    GET_BOOKING,
    GET_BOOKING_FAILED,
    GET_BOOKING_SUCCESS,
    GET_BOOKINGS,
    GET_BOOKINGS_FAILED,
    GET_BOOKINGS_SUCCESS,
    GET_CALENDAR_BOOKINGS,
    GET_CALENDAR_BOOKINGS_FAILED,
    GET_CALENDAR_BOOKINGS_SUCCESS, REJECT_BOOKING, REJECT_BOOKING_FAILED, REJECT_BOOKING_SUCCESS
} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: [],
    pagination: {
        total: null,
        first_page_url: null,
        last_page_url: null,
        next_page_url: null,
        prev_page_url: null
    },
    errors: null,
    single: {
        loading: false,
        data: null,
        errors: null
    },
    calendar: {
        loading: true,
        data: [],
        errors: null
    },
    accept: {
        loading: false,
        data: null,
        errors: null
    },
    reject: {
        loading: false,
        data: null,
        errors: null
    }
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_CALENDAR_BOOKINGS:
            return {
                ...state,
                calendar: {
                    ...state.calendar,
                    loading: true,
                    errors: null
                }
            };
        case GET_CALENDAR_BOOKINGS_SUCCESS:
            return {
                ...state,
                calendar: {
                    ...state.calendar,
                    loading: false,
                    data: action.payload
                }
            };
        case GET_CALENDAR_BOOKINGS_FAILED:
            return {
                ...state,
                calendar: {
                    ...state.calendar,
                    loading: false,
                    errors: action.payload
                }
            };
        case GET_BOOKINGS:
            return { ...state, loading: true, errors: null };
        case GET_BOOKINGS_SUCCESS:
            return { ...state, loading: false, data: action.payload, pagination: action.pagination };
        case GET_BOOKINGS_FAILED:
            return { ...state, loading: false, errors: action.payload };
        case GET_BOOKING:
            return {
                ...state,
                single: {
                    ...state.single,
                    loading: true,
                    errors: null
                }
            };
        case GET_BOOKING_SUCCESS:
            return {
                ...state,
                single: {
                    ...state.single,
                    loading: false,
                    data: action.payload
                }
            };
        case GET_BOOKING_FAILED:
            return {
                ...state,
                single: {
                    ...state.single,
                    loading: false,
                    errors: action.payload
                }
            };
        case REJECT_BOOKING:
            return {
                ...state,
                reject: {
                    ...state.reject,
                    loading: true,
                    errors: null
                }
            };
        case REJECT_BOOKING_SUCCESS:
            return {
                ...state,
                reject: {
                    ...state.reject,
                    loading: false,
                    data: action.payload
                }
            };
        case REJECT_BOOKING_FAILED:
            return {
                ...state,
                reject: {
                    ...state.reject,
                    loading: false,
                    errors: action.payload
                }
            };
        case ACCEPT_BOOKING:
            return {
                ...state,
                accept: {
                    ...state.accept,
                    loading: true,
                    errors: null
                }
            };
        case ACCEPT_BOOKING_SUCCESS:
            return {
                ...state,
                accept: {
                    ...state.accept,
                    loading: false,
                    data: action.payload
                }
            };
        case ACCEPT_BOOKING_FAILED:
            return {
                ...state,
                accept: {
                    ...state.accept,
                    loading: false,
                    errors: action.payload
                }
            };
        case CLEAN_BOOKING_ACTIONS:
            return {
                ...state,
                accept: INITIAL_STATE.accept,
                reject: INITIAL_STATE.reject,
            };
        default:
            return state;
    }
}
