import {
    GET_ENTITY,
    GET_ENTITY_FAILED,
    GET_ENTITY_SUCCESS,
    GET_ENTITIES,
    GET_ENTITIES_FAILED,
    GET_ENTITIES_SUCCESS,
    GET_ENTITY_CATEGORY,
    GET_ENTITY_CATEGORY_FAILED,
    GET_ENTITY_CATEGORY_SUCCESS,
    SAVE_ENTITY,
    SAVE_ENTITY_FAILED,
    SAVE_ENTITY_SUCCESS
} from "../types";

const INITIAL_STATE = {
    status: 'register_entity',
    // Data
    categories: [],
    entities: [],
    entity: null,
    // Loading
    category_loading: false,
    get_all_loading: false,
    get_loading: false,
    save_loading: false,
    // Error
    category_errors: null,
    get_all_errors: null,
    get_errors: null,
    save_errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case GET_ENTITIES:
            return { ...state, get_all_loading: true, get_all_errors: null };
        case GET_ENTITIES_SUCCESS:
            return { ...state, get_all_loading: false, entities: action.payload };
        case GET_ENTITIES_FAILED:
            return { ...state, get_all_loading: false, get_all_errors: action.payload };
        case GET_ENTITY:
            return { ...state, get_loading: true, get_errors: null };
        case GET_ENTITY_SUCCESS:
            return { ...state, get_loading: false, entity: action.payload };
        case GET_ENTITY_FAILED:
            return { ...state, get_loading: false, get_errors: action.payload };
        case GET_ENTITY_CATEGORY:
            return { ...state, category_loading: true, category_errors: null };
        case GET_ENTITY_CATEGORY_SUCCESS:
            return { ...state, category_loading: false, categories: action.payload };
        case GET_ENTITY_CATEGORY_FAILED:
            return { ...state, category_loading: false, category_errors: action.payload };
        case SAVE_ENTITY:
            return { ...state, save_loading: true, save_errors: null };
        case SAVE_ENTITY_SUCCESS:
            return { ...state, save_loading: false, entities: [action.payload], status: 'dashboard' };
        case SAVE_ENTITY_FAILED:
            return { ...state, save_loading: false, save_errors: action.payload };
        default:
            return state;
    }
}