import { combineReducers } from 'redux';
import authReducers from './authReducers';
import saleReducers from "./saleReducers";
import appointmentReducers from "./appointmentReducers";
import productReducers from "./productReducers";
import serviceReducers from "./serviceReducers";
import staffReducers from "./staffReducers";
import entityReducers from "./entityReducers";
import bookingReducers from "./bookingReducers";
import commentReducers from "./commentReducers";
import dashboardReducers from "./dashboardReducers";
import galleryReducers from "./galleryReducers";
import clientReducers from "./clientReducers";
import bookingOrderReducers from "./bookingOrderReducers";
import invoiceReducers from "./invoiceReducers";

export default combineReducers({
    auth: authReducers,
    entity: entityReducers,
    client: clientReducers,
    booking_order: bookingOrderReducers,
    sale: saleReducers,
    appointment: appointmentReducers,
    product: productReducers,
    service: serviceReducers,
    staff: staffReducers,
    booking: bookingReducers,
    invoice: invoiceReducers,
    comment: commentReducers,
    dashboard: dashboardReducers,
    gallery: galleryReducers
})