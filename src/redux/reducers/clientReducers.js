import {
    GET_CLIENT, GET_CLIENT_FAILED, GET_CLIENT_SUCCESS,
    GET_CLIENTS,
    GET_CLIENTS_FAILED,
    GET_CLIENTS_SUCCESS,
    SAVE_CLIENT,
    SAVE_CLIENT_FAILED,
    SAVE_CLIENT_SUCCESS, SEARCH_CLIENTS, SEARCH_CLIENTS_FAILED, SEARCH_CLIENTS_SUCCESS,
    SELECTED_CLIENT
} from "../types";

const INITIAL_STATE = {
    single_loading: false,
    search_loading: false,
    search_data: [],
    loading: false,
    data: [],
    single: null,
    single_errors: null,
    search_errors: null,
    errors: null,
    selected: null
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case SELECTED_CLIENT:
            return { ...state, selected: action.payload };
        case GET_CLIENTS:
            return { ...state, loading: true };
        case GET_CLIENTS_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_CLIENTS_FAILED:
            return { ...state, loading: false, errors: action.payload };
        case SEARCH_CLIENTS:
            return { ...state, search_loading: true };
        case SEARCH_CLIENTS_SUCCESS:
            return { ...state, search_loading: false, search_data: action.payload };
        case SEARCH_CLIENTS_FAILED:
            return { ...state, search_loading: false, search_errors: action.payload };
        case GET_CLIENT:
            return { ...state, single_loading: true };
        case GET_CLIENT_SUCCESS:
            return { ...state, single_loading: false, single: action.payload };
        case GET_CLIENT_FAILED:
            return { ...state, single_loading: false, single_errors: action.payload };
        case SAVE_CLIENT:
            return { ...state, save_loading: true };
        case SAVE_CLIENT_SUCCESS:
            return { ...state, save_loading: false, selected: action.payload };
        case SAVE_CLIENT_FAILED:
            return { ...state, save_loading: false, save_errors: action.payload };
        default:
            return state;
    }
}
