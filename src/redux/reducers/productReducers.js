import {
    GET_PRODUCT_CATEGORIES,
    GET_PRODUCT_CATEGORIES_FAILED,
    GET_PRODUCT_CATEGORIES_SUCCESS,
    GET_PRODUCTS,
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAILED, SAVE_PRODUCT_SUCCESS, SAVE_PRODUCT, SAVE_PRODUCT_FAILED
} from "../types";

const INITIAL_STATE = {
    loading: false,
    category_loading: false,
    data: [],
    categories: [],
    save_loading: false,
    save_errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_PRODUCTS:
            return { ...state, loading: true };
        case GET_PRODUCTS_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_PRODUCTS_FAILED:
            return { ...state, loading: false };
        case GET_PRODUCT_CATEGORIES:
            return { ...state, category_loading: true };
        case GET_PRODUCT_CATEGORIES_SUCCESS:
            return { ...state, categories: action.payload, category_loading: false };
        case GET_PRODUCT_CATEGORIES_FAILED:
            return { ...state, category_loading: false };
        case SAVE_PRODUCT:
            return { ...state, save_loading: true };
        case SAVE_PRODUCT_SUCCESS:
            const save_data = state.data;
            if(!action.edit)
                save_data.push(action.payload);
            return { ...state, save_loading: false, data: save_data };
        case SAVE_PRODUCT_FAILED:
            return { ...state, save_loading: false, save_errors: action.payload };
        default:
            return state;
    }
}
