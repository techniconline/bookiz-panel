import {GET_COMMENTS, GET_COMMENTS_SUCCESS, GET_COMMENTS_FAILED} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: [],
    errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_COMMENTS:
            return { ...state, loading: true };
        case GET_COMMENTS_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_COMMENTS_FAILED:
            return { ...state, loading: false, errors: action.payload };
        default:
            return state;
    }
}
