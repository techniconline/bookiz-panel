import {
    GET_ORDER, GET_ORDER_FAILED, GET_ORDER_SUCCESS,
    ON_CART, ON_CART_FAILED, ON_CART_SUCCESS, ON_PAYMENT, ON_PAYMENT_FAILED, ON_PAYMENT_SUCCESS, ON_SERVICE_CART
} from "../types";

const INITIAL_STATE = {
    cart_loading: false,
    cart_client_id: null,
    cart_submit: false,
    loading: false,
    data: null,
    errors: null,
    payment_loading: false,
    payment_errors: null
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case ON_CART:
            return { ...state, cart_loading: true, cart_submit: false };
        case ON_CART_SUCCESS:
            return { ...state, cart_loading: false, cart_submit: true, cart_client_id: action.client_id};
        case ON_CART_FAILED:
            return { ...state, cart_loading: false };
        case GET_ORDER:
            return { ...state, loading: true, errors: null };
        case GET_ORDER_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_ORDER_FAILED:
            return { ...state, loading: false, errors: action.payload };
        case ON_PAYMENT:
            return { ...state, payment_loading: true, payment_errors: null };
        case ON_PAYMENT_SUCCESS:
            return { ...state, payment_loading: false };
        case ON_PAYMENT_FAILED:
            return { ...state, payment_loading: false, payment_errors: action.payload };
        default:
            return state;
    }
}
