import _ from 'lodash';
import {
    GET_SERVICE_CATEGORIES,
    GET_SERVICE_CATEGORIES_FAILED,
    GET_SERVICE_CATEGORIES_SUCCESS,
    GET_SERVICES,
    GET_SERVICES_SUCCESS,
    GET_SERVICES_FAILED,
    SAVE_SERVICE,
    SAVE_SERVICE_SUCCESS,
    SAVE_SERVICE_FAILED, ON_DELETE_SERVICE, ON_DELETE_SERVICE_SUCCESS, ON_DELETE_SERVICE_FAILED
} from "../types";

const INITIAL_STATE = {
    loading: false,
    category_loading: false,
    save_loading: false,
    data: [],
    errors: null,
    categories: [],
    delete_loading: false,
    delete_status: false,
    delete_errors: null
};

export default (state = INITIAL_STATE, action) => {
    let data = state.data;
    switch(action.type){
        case GET_SERVICES:
            return { ...state, loading: true };
        case GET_SERVICES_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_SERVICES_FAILED:
            return { ...state, loading: false, errors: action.payload };
        case GET_SERVICE_CATEGORIES:
            return { ...state, category_loading: true };
        case GET_SERVICE_CATEGORIES_SUCCESS:
            return { ...state, categories: action.payload, category_loading: false };
        case GET_SERVICE_CATEGORIES_FAILED:
            return { ...state, category_loading: false };
        case SAVE_SERVICE:
            return { ...state, save_loading: true };
        case SAVE_SERVICE_SUCCESS:
            return { ...state, save_loading: false, data };
        case SAVE_SERVICE_FAILED:
            return { ...state, save_loading: false, save_errors: action.payload };
        case ON_DELETE_SERVICE:
            return { ...state, delete_loading: true, delete_status: false };
        case ON_DELETE_SERVICE_SUCCESS:
            const data_dlt = _.map(state.data, item => {
                let body = {
                    ...item,
                    entity_relation_services: _.filter(item.entity_relation_services, service => service.id !== action.id)
                };
                return body;
            });
            return { ...state, delete_loading: false, delete_status: true, data: data_dlt };
        case ON_DELETE_SERVICE_FAILED:
            return { ...state, delete_loading: false, delete_status: false, delete_errors: action.payload };
        default:
            return state;
    }
}
