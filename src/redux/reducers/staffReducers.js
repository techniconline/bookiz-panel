import {
    GET_STAFFS,
    GET_STAFFS_SUCCESS,
    GET_STAFFS_FAILED
} from "../types";

const INITIAL_STATE = {
    get_all_loading: false,
    get_all_errors: null,
    data: [],
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_STAFFS:
            return { ...state, get_all_loading: true };
        case GET_STAFFS_SUCCESS:
            return { ...state, get_all_loading: false, data: action.payload };
        case GET_STAFFS_FAILED:
            return { ...state, get_all_loading: false, get_all_errors: action.payload };
        default:
            return state;
    }
}
