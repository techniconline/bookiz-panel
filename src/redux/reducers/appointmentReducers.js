import {
    GET_APPOINTMENTS,
    GET_APPOINTMENTS_SUCCESS,
    GET_APPOINTMENTS_FAILED,
    ADD_APPOINTMENT,
    ADD_APPOINTMENT_SUCCESS,
    ADD_APPOINTMENT_FAILED
} from "../types";

const INITIAL_STATE = {
    loading: false,
    add_loading: false,
    add_error: null,
    data: [],
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_APPOINTMENTS:
            return { ...state, loading: true };
        case GET_APPOINTMENTS_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_APPOINTMENTS_FAILED:
            return { ...state, loading: false };
        case ADD_APPOINTMENT:
            return { ...state, add_loading: true, add_error: null };
        case ADD_APPOINTMENT_SUCCESS:
            let add_data = state.data;
            if(Array.isArray(action.payload))
                add_data = [ ...add_data, ...action.payload];
            else
                add_data.push(action.payload);
            return { ...state, add_loading: false, data: add_data };
        case ADD_APPOINTMENT_FAILED:
            return { ...state, add_loading: false };
        default:
            return state;
    }
}
