import {
    GET_SALES,
    GET_SALES_SUCCESS,
    GET_SALES_FAILED
} from "../types";

const INITIAL_STATE = {
    loading: false,
    data: [],
};

export default (state = INITIAL_STATE, action) => {
    switch(action.type){
        case GET_SALES:
            return { ...state, loading: true };
        case GET_SALES_SUCCESS:
            return { ...state, loading: false, data: action.payload };
        case GET_SALES_FAILED:
            return { ...state, loading: false };
        default:
            return state;
    }
}
