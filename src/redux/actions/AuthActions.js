import axios from 'axios';
import {wstoken, API_URL, post, get} from './../http';
import { Redirect } from 'react-router-dom';
import {
    SET_LOGIN,
    LOGIN_SUCCESS,
    LOGIN_ERROR,
    SET_TOKEN,
    ACCESS_TOKEN,
    SET_REGISTER,
    REGISTER_SUCCESS,
    REGISTER_ERROR,
    SET_FORGOT,
    FORGOT_ERROR,
    FORGOT_SUCCESS,
    ON_LOGOUT,
    ON_LOGOUT_SUCCESS,
    GET_ENTITIES_SUCCESS,
    GET_ENTITIES_FAILED,
    HAS_ENTITY,
    CURRENT_ENTITY
} from '../types';

export const login = (connector , password, reload = false) => {
    return (dispatch) => {
        dispatch({ type: SET_LOGIN });
        const url = API_URL + 'user/jwt/login';
        axios.post(url, {connector , password, wstoken}, { headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'dataType': 'json'}}).then((result) => {
            if(result.data.action) {
                const user = result.data.data;
                localStorage.setItem(ACCESS_TOKEN, user.token);
                axios.post(`${API_URL}entity/admin/list_entity`, { _method: 'GET', wstoken}, { headers: {Authorization: 'bearer '+user.token} })
                    .then(resultEntity => {
                        if(resultEntity.data.action) {
                            if(resultEntity.data.data.length > 0) {
                                localStorage.setItem(HAS_ENTITY, 'true');
                                localStorage.setItem(CURRENT_ENTITY, resultEntity.data.data[1].id);
                                if(reload) location.reload();
                                dispatch({
                                    type: LOGIN_SUCCESS,
                                    status: 'dashboard',
                                    access_token: user.token,
                                    user
                                });
                            }else {
                                localStorage.setItem(HAS_ENTITY, 'false');
                                dispatch({
                                    type: LOGIN_SUCCESS,
                                    status: 'register_entity',
                                    access_token: user.token,
                                    user
                                });
                            }
                        }else {

                        }
                    }).catch(error => {
                        console.log(error);
                    });
                // dispatch({
                //     type: LOGIN_SUCCESS,
                //     status: 'dashboard',
                //     access_token: user.token,
                //     user
                // });
            }else{
                console.log(result.data.message);
                dispatch({ type: LOGIN_ERROR, payload: (result.data.errors) ? result.data.errors : [[result.data.message]] });
            }
        }).catch((error) => {
            dispatch({ type: LOGIN_ERROR, payload: error.response.data.errors });
        });
    }
};

export const register = (body) => {
    return (dispatch) => {
        dispatch({ type: SET_REGISTER });
        const url = API_URL + 'user/jwt/register';
        axios.post(url, {...body, wstoken}, { headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'dataType': 'json'}}).then((result) => {
            if(result.data.action) {
                const user = result.data.data;
                localStorage.setItem(ACCESS_TOKEN, user.token);
                dispatch({
                    type: REGISTER_SUCCESS,
                    status: 'dashboard',
                    access_token: user.token,
                    user
                });
            }else{
                dispatch({ type: REGISTER_ERROR, payload: (result.data.errors) ? result.data.errors : [[result.data.message]] });
            }

        }).catch((error) => {
            dispatch({ type: REGISTER_ERROR, payload: error.response.data.errors });
        });
    }
};

export const forgot = (connector) => {
    return (dispatch) => {
        dispatch({ type: SET_FORGOT });
        const url = API_URL + 'user/jwt/forgot';
        axios.post(url, {connector , wstoken}, { headers: {'Content-Type': 'application/json', 'Access-Control-Allow-Origin': '*', 'dataType': 'json'}}).then((result) => {
            if(result.data.action) {
                dispatch({
                    type: FORGOT_SUCCESS,
                    success_message: result.data.message
                });
            }else{
                dispatch({ type: FORGOT_ERROR, payload: (result.data.errors) ? result.data.errors : [[result.data.message]] });
            }

        }).catch((error) => {
            dispatch({ type: FORGOT_ERROR, payload: error.response.data.errors });
        });
    }
};

export const logout = () => {
    return (dispatch) => {
        dispatch({ type: ON_LOGOUT });
        post('user/jwt/logout').then(result => {
            dispatch({ type: ON_LOGOUT_SUCCESS });
        }).catch(error => {
            console.log(error);
        });
    }

};

export const set_token = (token) => {
    return {
        type: SET_TOKEN,
        payload: token
    }
};