import { post, get } from '../http';
import {
    GET_ALL,
    GET_ALL_SUCCESS,
    GET_ALL_FAILED,
    ADD,
    ADD_SUCCESS,
    ADD_FAILED,
    GET_SINGLE,
    GET_SINGLE_SUCCESS,
    GET_SINGLE_FAILED,
    GET_SALES,
    GET_SALES_SUCCESS,
    GET_SALES_FAILED, GET_SALE_ITEMS, GET_SALE_ITEMS_SUCCESS, GET_SALE_ITEMS_FAILED
} from '../types';

export const get_sales = (days = 30) => {
    return (dispatch) => {
        dispatch({ type: GET_SALES });
        const url = 'https://www.mocky.io/v2/' + ((Number(days) === 30) ? '5c12ac733300008f48999082' : '5c12ac1f3300001e4699907d');
        get(null, url).then(result => {
            dispatch({ type: GET_SALES_SUCCESS, payload: result.data });
        }).catch(error => {
            dispatch({ type: GET_SALES_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};

// export const get_single = (id) => {
//     return (dispatch) => {
//         dispatch({ type: GET_SINGLE });
//         get(`get/${id}`).then(result => {
//             dispatch({ type: GET_SINGLE_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: GET_SINGLE_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
//
// export const add = (data) => {
//     return (dispatch) => {
//         dispatch({ type: ADD });
//         post('add', { data: 'data' }).then(result => {
//             dispatch({ type: ADD_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: ADD_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };

export const get_sale_items = () => {
    return (dispatch) => {
        dispatch({ type: GET_SALE_ITEMS });
        const url = (Number(days) === 30) ? '5c12ac733300008f48999082' : '5c12ac1f3300001e4699907d';
        get(url).then(result => {
            dispatch({ type: GET_SALE_ITEMS_SUCCESS, payload: [] });
        }).catch(error => {
            dispatch({ type: GET_SALE_ITEMS_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};
