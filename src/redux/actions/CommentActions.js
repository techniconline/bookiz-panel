import {
    CURRENT_ENTITY,
    GET_COMMENTS,
    GET_COMMENTS_FAILED,
    GET_COMMENTS_SUCCESS
} from "../types";
import { get } from '../http';

export const get_comments = () => {
    return dispatch => {
        dispatch({ type: GET_COMMENTS });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        get(`/feedback/comment/entities/${entity_id}/get`).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_COMMENTS_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_COMMENTS_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: GET_COMMENTS_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: GET_COMMENTS_FAILED, payload: errors.response});
            }
        });
    }
};