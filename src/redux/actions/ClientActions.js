import { post, get } from '../http';
import {
    SAVE_CLIENT,
    SAVE_CLIENT_SUCCESS,
    SAVE_CLIENT_FAILED,
    GET_CLIENTS,
    GET_CLIENTS_SUCCESS,
    GET_CLIENTS_FAILED,
    GET_CLIENT,
    GET_CLIENT_SUCCESS,
    GET_CLIENT_FAILED, CURRENT_ENTITY, SELECTED_CLIENT, SEARCH_CLIENTS, SEARCH_CLIENTS_SUCCESS, SEARCH_CLIENTS_FAILED
} from '../types';

const call_clients = (dispatch, text = '') => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    // get(`entity/admin/client/show/${entity_id}`, null, { text }).then(result => {
    get(`entity/admin/client/show/${entity_id}`, null, { text }).then(result => {
        if(result.data.action) {
            dispatch({ type: GET_CLIENTS_SUCCESS, payload: result.data.data.data });
        }else {
            dispatch({ type: GET_CLIENTS_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
        }
    }).catch(error => {
        dispatch({ type: GET_CLIENTS_FAILED, payload: 'FAILED MESSAGE!' });
    })
};
export const get_clients = (text) => {
    return (dispatch) => {
        dispatch({ type: GET_CLIENTS });
        call_clients(dispatch, text);
    }
};
export const search_clients = (q = '') => {
    return (dispatch) => {
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        dispatch({ type: SEARCH_CLIENTS });
        get(`entity/admin/client/search`, null, { q, entity_id }).then(result => {
            if(result.data.action) {
                dispatch({ type: SEARCH_CLIENTS_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: SEARCH_CLIENTS_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: SEARCH_CLIENTS_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};
export const get_client = (id) => {
    return (dispatch) => {
        dispatch({ type: GET_CLIENT });
        get(`entity/admin/client/info/${id}`).then(result => {
            dispatch({ type: GET_CLIENT_SUCCESS, payload: result.data.data });
        }).catch(error => {
            dispatch({ type: GET_CLIENT_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};
export const save_client = (data) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return (dispatch) => {
        dispatch({ type: SAVE_CLIENT });
        post(`entity/admin/client/${entity_id}/save`, data).then(result => {
            if(result.data.action){
                dispatch({ type: SAVE_CLIENT_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: SAVE_CLIENT_FAILED, payload: [[result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: SAVE_CLIENT_FAILED, payload: error.response.data });
        });
    }
};

export const selected_client = (payload) => {
    return { type: SELECTED_CLIENT, payload };
};
