import {get, post, x_get} from "../http";
import {
    CURRENT_ENTITY,
    GET_BOOKING,
    GET_BOOKING_SUCCESS,
    GET_BOOKING_FAILED,
    GET_BOOKINGS,
    GET_BOOKINGS_FAILED,
    GET_BOOKINGS_SUCCESS,
    ACCEPT_BOOKING,
    ACCEPT_BOOKING_SUCCESS,
    ACCEPT_BOOKING_FAILED,
    REJECT_BOOKING,
    REJECT_BOOKING_SUCCESS,
    REJECT_BOOKING_FAILED,
    GET_CALENDAR_BOOKINGS_SUCCESS,
    GET_CALENDAR_BOOKINGS_FAILED,
    GET_CALENDAR_BOOKINGS,
    CLEAN_BOOKING_ACTIONS,
    ADD_CART_SUCCESS,
    REMOVE_CART_SUCCESS,
    ON_CART,
    ON_CART_FAILED,
    ON_SERVICE_CART,
    ON_PAYMENT,
    ON_CART_SUCCESS, GET_ORDER, GET_ORDER_SUCCESS, GET_ORDER_FAILED, ON_PAYMENT_SUCCESS, ON_PAYMENT_FAILED
} from "../types";
import _ from 'lodash';
// http://api.beautyday.ir/booking/admin/booking/{model_id}/{model_type}/list

export const get_calendar_bookings = (start_date = null, end_date = null) => {
    return dispatch => {
        dispatch({ type: GET_CALENDAR_BOOKINGS });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        const body = {
            with_out_paginate: 1,
            start_time: '00:00:01',
            end_time: '23:59:59',
            start_date,
            end_date
        };
        get(`booking/admin/booking/${entity_id}/entities/list`, null, body).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_CALENDAR_BOOKINGS_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_CALENDAR_BOOKINGS_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: GET_CALENDAR_BOOKINGS_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: GET_CALENDAR_BOOKINGS_FAILED, payload: errors.response});
            }
        });
    }
};

const call_bookings = (dispatch) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    get(`booking/admin/booking/${entity_id}/entities/list`).then(result => {
        if(result.data.action) {
            const pagination = {
                total: result.data.data.total,
                first_page_url: result.data.data.first_page_url,
                last_page_url: result.data.data.last_page_url,
                next_page_url: result.data.data.next_page_url,
                prev_page_url: result.data.data.prev_page_url
            };
            dispatch({ type: GET_BOOKINGS_SUCCESS, payload: result.data.data.data, pagination });
        }else {
            dispatch({ type: GET_BOOKINGS_FAILED, payload: [[result.data.message]] });
        }
    }).catch(errors => {
        if(errors.request.status === 500) {
            dispatch({ type: GET_BOOKINGS_FAILED, payload: [[errors.request.statusText]] });
        }else {
            dispatch({type: GET_BOOKINGS_FAILED, payload: errors.response});
        }
    });
};

export const get_bookings = () => {
    return dispatch => {
        dispatch({ type: GET_BOOKINGS });
        call_bookings(dispatch);
    }
};

export const get_booking = (id) => {
    return dispatch => {
        dispatch({ type: GET_BOOKING });
        get(`booking/admin/booking/show/${id}`).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_BOOKING_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_BOOKING_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request && errors.request.status === 500) {
                dispatch({ type: GET_BOOKING_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: GET_BOOKING_FAILED, payload: errors.response});
            }
        });
    }
};

export const booking_accept = (id, note) => {
    return dispatch => {
        dispatch({ type: ACCEPT_BOOKING });
        post(`booking/admin/booking/manager/${id}/acceptEntity`, { note }).then(result => {
            if(result.data.action) {
                call_bookings(dispatch);
                dispatch({ type: ACCEPT_BOOKING_SUCCESS, payload: result.data });
                setTimeout(() => {
                    dispatch({ type: CLEAN_BOOKING_ACTIONS });
                }, 1000);
            }else {
                dispatch({ type: ACCEPT_BOOKING_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: ACCEPT_BOOKING_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: ACCEPT_BOOKING_FAILED, payload: errors.response});
            }
        });
    }
};

export const booking_reject = (id, reason_reject, reason_note_reject) => {
    return dispatch => {
        dispatch({ type: REJECT_BOOKING });
        post(`booking/admin/booking/manager/${id}/rejectEntity`, { reason_reject, reason_note_reject }).then(result => {
            if(result.data.action) {
                call_bookings(dispatch);
                dispatch({ type: REJECT_BOOKING_SUCCESS, payload: result.data });
                setTimeout(() => {
                    dispatch({ type: CLEAN_BOOKING_ACTIONS });
                }, 1000);
            }else {
                dispatch({ type: REJECT_BOOKING_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: REJECT_BOOKING_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: REJECT_BOOKING_FAILED, payload: errors.response});
            }
        });
    }
};

export const on_service_cart = (payload) => {
    return { type: ON_SERVICE_CART, payload};
};

export const booking_carts = (services, client) => {
    return dispatch => {
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        dispatch({ type: ON_CART });
        const body = { entity_client_id: client.id, entity_id: entity_id };
        get('sale/cart/delete/items/all', null, body).then(result => {
            _.map(services, (service_id, ind) => {
                post(`sale/cart/add/entity_relation_services/${service_id}`, body).then(result => {
                    // if(result.data.action) {
                    if(services.length === ind+1) {
                        dispatch({ type: ON_CART_SUCCESS, service_id, client_id: client.id, response: result.data.data });
                    }
                    // }else {
                    //     if(result.data.remove_cart_item) {
                    //         dispatch({ type: REMOVE_CART_SUCCESS, service_id, response: result.data.data });
                    //     }else {
                    //         dispatch({ type: ON_CART_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]]});
                    //     }
                    // }
                }).catch(errors => {
                    dispatch({ type: ON_CART_FAILED, payload: errors.response});
                });
            });
        });
    }
};

export const get_order = (client_id) => {
    return dispatch => {
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        dispatch({ type: GET_ORDER });
        get('sale/cart/booking', null, { entity_client_id: client_id, entity_id }).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_ORDER_SUCCESS, payload: result.data.data});
            }else {
                dispatch({ type: GET_ORDER_FAILED, payload: [[(result.data.error) ? result.data.error : result.data.message]], empty: true});
            }
        }).catch(errors => {
            dispatch({ type: GET_ORDER_FAILED, payload: errors.response});
        });
    }
};

export const on_payment = (data) => {
    return dispatch => {
        dispatch({ type: ON_PAYMENT });
        post('sale/cart/booking/save', data ).then(result => {
            dispatch({ type: ON_PAYMENT_SUCCESS });
            const url = result.data.redirect.replace('api.','www.');
            if(result.data.action) {
                location.replace(url);
            }
        }).catch(errors => {
            dispatch({ type: ON_PAYMENT_FAILED, payload: errors.response });
            console.log(errors.response);
            console.log(errors.request);
        });
    }
}
//
// export const get_order = (client_id) => {
//     return dispatch => {
//         dispatch({ type: GET_ORDER });
//         post(`sale/cart/booking/save`, data, { headers }).then(result => {
//             console.log(result);
//             const url = result.data.redirect.replace('api.','www.');
//
//         }).catch(errors => {
//             console.log(errors.response);
//             console.log(errors.request);
//         });
//     }
// };