import { post, get } from '../http';
import {
    GET_ALL,
    GET_ALL_SUCCESS,
    GET_ALL_FAILED,
    ADD,
    ADD_SUCCESS,
    ADD_FAILED,
    GET_SINGLE,
    GET_SINGLE_SUCCESS,
    GET_SINGLE_FAILED
} from '../types';

export const get_all = () => {
    return (dispatch) => {
        dispatch({ type: GET_ALL });
        get('get_all').then(result => {
            dispatch({ type: GET_ALL_SUCCESS, payload: result.data });
        }).catch(error => {
            dispatch({ type: GET_ALL_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};

export const get_single = (id) => {
    return (dispatch) => {
        dispatch({ type: GET_SINGLE });
        get(`get/${id}`).then(result => {
            dispatch({ type: GET_SINGLE_SUCCESS, payload: result.data });
        }).catch(error => {
            dispatch({ type: GET_SINGLE_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};

export const add = (data) => {
    return (dispatch) => {
        dispatch({ type: ADD });
        post('add', { data: 'data' }).then(result => {
            dispatch({ type: ADD_SUCCESS, payload: result.data });
        }).catch(error => {
            dispatch({ type: ADD_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};
