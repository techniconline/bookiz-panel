import {
    CURRENT_ENTITY,
    DASHBOARD_RECENT,
    DASHBOARD_RECENT_FAILED,
    DASHBOARD_RECENT_SUCCESS, DASHBOARD_TOP_SERVICE, DASHBOARD_TOP_SERVICE_FAILED, DASHBOARD_TOP_SERVICE_SUCCESS,
    DASHBOARD_TOP_STAFF, DASHBOARD_TOP_STAFF_FAILED, DASHBOARD_TOP_STAFF_SUCCESS,
    DASHBOARD_UPCOMING,
    DASHBOARD_UPCOMING_FAILED,
    DASHBOARD_UPCOMING_SUCCESS,
} from "../types";
import { get } from '../http';
export const get_dashboard_chart = (type = 'recent', data) => {
    return dispatch => {
        const type_loading = (type === 'recent') ? DASHBOARD_RECENT : DASHBOARD_UPCOMING;
        const type_success = (type === 'recent') ? DASHBOARD_RECENT_SUCCESS : DASHBOARD_UPCOMING_SUCCESS;
        const type_failed = (type === 'recent') ? DASHBOARD_RECENT_FAILED : DASHBOARD_UPCOMING_FAILED;
        dispatch({ type: type_loading });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        get(`booking/admin/booking/${entity_id}/entities/list_booking_details_chart`, null, data).then(result => {
            if(result.data.action) {
                const payload = result.data.data;
                dispatch({ type: type_success, payload: (typeof payload === "undefined") ? [] : payload });
            }else {
                dispatch({ type: type_failed, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: type_failed, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: type_failed, payload: errors.response});
            }
        });
    }
};

export const get_dashboard_list = (type = 'top_staff', data) => {
    return dispatch => {
        const type_loading = (type === 'top_staff') ? DASHBOARD_TOP_STAFF : DASHBOARD_TOP_SERVICE;
        const type_success = (type === 'top_staff') ? DASHBOARD_TOP_STAFF_SUCCESS : DASHBOARD_TOP_SERVICE_SUCCESS;
        const type_failed = (type === 'top_staff') ? DASHBOARD_TOP_STAFF_FAILED : DASHBOARD_TOP_SERVICE_FAILED;
        dispatch({ type: type_loading });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        get(`booking/admin/booking/${entity_id}/entities/list_booking_details_chart`, null, data).then(result => {
            if(result.data.action) {
                const payload = result.data.data;
                dispatch({ type: type_success, payload: (typeof payload === "undefined") ? [] : payload });
            }else {
                dispatch({ type: type_failed, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: type_failed, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: type_failed, payload: errors.response});
            }
        });
    }
};