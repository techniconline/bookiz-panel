import PropTypes from 'prop-types';
import {post, get, x_delete} from '../http';
import _ from 'lodash';
import {
    GET_SERVICES,
    GET_SERVICES_SUCCESS,
    GET_SERVICE_CATEGORIES,
    GET_SERVICE_CATEGORIES_SUCCESS,
    GET_SERVICES_FAILED,
    GET_SERVICE_CATEGORIES_FAILED,
    CURRENT_ENTITY,
    SAVE_SERVICE,
    SAVE_SERVICE_SUCCESS,
    SAVE_SERVICE_FAILED, ON_DELETE_SERVICE, ON_DELETE_SERVICE_SUCCESS, ON_DELETE_SERVICE_FAILED
} from '../types';


/* ===== Get All Services ===== */
export const get_all_services = () => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return (dispatch) => {
        dispatch({type: GET_SERVICES});
        get(`entity/user/show_services_entity/${entity_id}`).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_SERVICES_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_SERVICES_FAILED, payload: [[ result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: GET_SERVICES_FAILED, payload: error.response.data });
        });
    }
};

export const add_employees = (dispatch, edit, employees, service) => {
    let count = 0;
    _.map(employees, id => {
        post(`entity/admin/employee/service/${id}/save`, {
            for_gender: 'all',
            entity_relation_service_ids: [service.id]
        }).then(response => {
            count++;
            if(count === employees.length) {
                const entity_id = localStorage.getItem(CURRENT_ENTITY);
                dispatch({type: GET_SERVICES});
                get(`entity/user/show_services_entity/${entity_id}`).then(result => {
                    if(result.data.action) {
                        dispatch({ type: SAVE_SERVICE_SUCCESS, payload: service, edit });
                        dispatch({ type: GET_SERVICES_SUCCESS, payload: result.data.data });
                    }else {
                        dispatch({ type: GET_SERVICES_FAILED, payload: [[ result.data.message]] });
                    }
                }).catch(error => {
                    dispatch({ type: GET_SERVICES_FAILED, payload: error.response.data });
                });
            }
        })
    });
};

export const save_service = (data, employees = [], edit = false) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return (dispatch) => {
        dispatch({type: SAVE_SERVICE});
        post(`entity/admin/service/${entity_id}/save`, data).then(result => {
            if(result.data.action) {
                if(employees.length > 0 && result.data.data) {
                    add_employees(dispatch, edit, employees, result.data.data);
                }else {
                    dispatch({ type: SAVE_SERVICE_SUCCESS, payload: result.data.data, edit });
                }
            }else {
                dispatch({ type: SAVE_SERVICE_FAILED, payload: [[ result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: SAVE_SERVICE_FAILED, payload: error.response.data });
        });
    }
};

export const get_service_categories = () => {
    return (dispatch) => {
        dispatch({type: GET_SERVICE_CATEGORIES});
        get(`entity/user/service/list`, null , { decorate: 1 }).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_SERVICE_CATEGORIES_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_SERVICE_CATEGORIES_FAILED, payload: [[ 'Get Services Problem. Try Again.']] });
            }
        }).catch(error => {
            dispatch({ type: GET_SERVICE_CATEGORIES_FAILED, payload: error.response.data });
        });
    }
};

export const get_services = (props) => {
    return (dispatch) => {
        dispatch({ type: GET_SERVICES });
        setTimeout(()=>{
            const payload = [
                {
                    id: 1,
                    name: 'Blow Dry',
                    price: 25,
                    discount: 0,
                    time: '01:30',
                    staff: 'ahmad khorshidi'
                },
                {
                    id: 2,
                    name: 'Haircut',
                    price: 25,
                    discount: 0,
                    time: '01:30',
                    staff: 'ahmad khorshidi'
                }
            ];
            dispatch({ type: GET_SERVICES_SUCCESS, payload });
        }, 1000);
    }
};
get_services.defaultProps ={
    category: ''
};
get_services.propTypes = {
    category: PropTypes.string
};

export const delete_service = (id) => {
    return dispatch => {
        dispatch({ type: ON_DELETE_SERVICE });
        x_delete(`entity/admin/service/${id}/delete`).then(result => {
            if(result.data.action) {
                dispatch({ type: ON_DELETE_SERVICE_SUCCESS, payload: result.data.data, id });
            }else {
                dispatch({ type: ON_DELETE_SERVICE_FAILED, payload: [[(result.data.errors) ? result.data.errors : result.data.message]] });
            }
        }).catch(errors => {
            dispatch({ type: ON_DELETE_SERVICE_FAILED, payload: errors.response });
        })
    }
};

// export const get_single = (id) => {
//     return (dispatch) => {
//         dispatch({ type: GET_SINGLE });
//         get(`get/${id}`).then(result => {
//             dispatch({ type: GET_SINGLE_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: GET_SINGLE_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
//
// export const add = (data) => {
//     return (dispatch) => {
//         dispatch({ type: ADD });
//         post('add', { data: 'data' }).then(result => {
//             dispatch({ type: ADD_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: ADD_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
//


// export const get_service_categories = () => {
//     return (dispatch) => {
//         dispatch({ type: GET_SERVICE_CATEGORIES });
//         setTimeout(()=>{
//             const payload = [
//                 { id: 1, name: 'Hair'},
//                 { id: 2, name: 'Body'},
//                 { id: 3, name: 'Face'},
//                 { id: 4, name: 'no-category'}
//             ];
//             dispatch({ type: GET_SERVICE_CATEGORIES_SUCCESS, payload });
//         }, 1000);
//     }
// };
