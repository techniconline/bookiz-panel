import { post, get } from '../http';
import uuid from 'uuid';
import {
    GET_APPOINTMENTS,
    GET_APPOINTMENTS_SUCCESS,
    GET_APPOINTMENTS_FAILED,
    ADD_APPOINTMENT,
    ADD_APPOINTMENT_SUCCESS, CURRENT_ENTITY
} from '../types';

const appointments = [
    {
        id: 13,
        title: 'Multi-day Event',
        hexColor: '1565C0',
        start: new Date(2015, 3, 20, 19, 30, 0),
        end: new Date(2015, 3, 22, 2, 0, 0),
    },
    {
        id: 14,
        title: 'Today 1',
        hexColor: '1565C0',
        start: new Date(new Date().setHours(12, 0 ,0)),
        end: new Date(new Date().setHours(15, 0 ,0))
    },
    {
        id: 14,
        title: 'Today 2',
        hexColor: '1565C0',
        start: new Date(new Date().setHours(12, 0 ,0)),
        end: new Date(new Date().setHours(14, 30 ,0)),
    },
];
export const get_appointments = (days = 30) => {
    return (dispatch) => {
        dispatch({ type: GET_APPOINTMENTS });
        // const url = 'https://www.mocky.io/v2/' + ((days === 30) ? '5c12ae4c3300003c48999087' : '5c12b021330000734599908d');
        // get(null, url).then(result => {
        //     dispatch({ type: GET_APPOINTMENTS_SUCCESS, payload: result.data });
        // }).catch(error => {
        //     dispatch({ type: GET_APPOINTMENTS_FAILED, payload: 'FAILED MESSAGE!' });
        // })
        setTimeout(() => {
            dispatch({ type: GET_APPOINTMENTS_SUCCESS, payload: appointments });
        }, 1000);
    }
};
export const add_appointment = (data) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return dispatch => {
        dispatch({ type: ADD_APPOINTMENT });
        post(`booking/user/booking/${entity_id}/{entity_relation_service_id}/{entity_service_id}/save`)
    }
    // return (dispatch) => {
    //     dispatch({ type: ADD_APPOINTMENT });
    //     setTimeout(() => {
    //         dispatch({ type: ADD_APPOINTMENT_SUCCESS, payload: data });
    //     }, 1000);
    // }
};

// export const get_single = (id) => {
//     return (dispatch) => {
//         dispatch({ type: GET_SINGLE });
//         get(`get/${id}`).then(result => {
//             dispatch({ type: GET_SINGLE_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: GET_SINGLE_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
//
// export const add = (data) => {
//     return (dispatch) => {
//         dispatch({ type: ADD });
//         post('add', { data: 'data' }).then(result => {
//             dispatch({ type: ADD_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: ADD_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
