import {
    CURRENT_ENTITY,
    GET_ENTITIES,
    GET_ENTITIES_FAILED,
    GET_ENTITIES_SUCCESS,
    GET_ENTITY_CATEGORY,
    GET_ENTITY_CATEGORY_FAILED,
    GET_ENTITY_CATEGORY_SUCCESS,
    HAS_ENTITY,
    SAVE_ENTITY,
    SAVE_ENTITY_FAILED,
    SAVE_ENTITY_SUCCESS,
    SAVE_ENTITY_ADDRESS,
    SAVE_ENTITY_ADDRESS_FAILED,
    SAVE_ENTITY_ADDRESS_SUCCESS,
    GET_ENTITY_FAILED,
    GET_ENTITY_SUCCESS,
    GET_ENTITY,
    SAVE_IMAGE,
    SAVE_IMAGE_FAILED,
    SAVE_IMAGE_SUCCESS, DELETE_IMAGE, DELETE_IMAGE_SUCCESS, DELETE_IMAGE_FAILED, CLEAN_GALLERY_ACTIONS
} from "../types";
import {get, post, upload, x_delete, x_get} from "../http";

/* ===== Get Entity For User ===== */
export const save_entity = (data, reload = false) => {
    return dispatch => {
        dispatch({ type: SAVE_ENTITY });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        post('entity/admin/save', { ...data, entity_id }).then(result => {
            if(result.data.action) {
                localStorage.setItem(HAS_ENTITY, 'true');
                localStorage.setItem(CURRENT_ENTITY, result.data.data.id);
                if(reload) location.reload();
                dispatch({ type: SAVE_ENTITY_SUCCESS, payload: result.data.data });
            }else{
                dispatch({ type: SAVE_ENTITY_FAILED, payload: [['Register Problem. Please Try Again.']] });
            }

        }).catch(error => {
            // console.log('error.request', error.request);
            // console.log('error.response', error.response);
            // console.log('error.config', error.config);
            if(error.request.status === 500) {
                dispatch({ type: SAVE_ENTITY_FAILED, payload: [[error.request.statusText]] });
            }else{
                dispatch({ type: SAVE_ENTITY_FAILED, payload: error.response });
            }
        });
    }
};

/* ===== Get User Entities ===== */
export const get_entities_user = () => {
    return dispatch => {
        dispatch({ type: GET_ENTITIES });
        get(`entity/admin/list_entity`).then(result => {
            console.log(result);
            if(result.data.action) {
                dispatch({ type: GET_ENTITIES_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_ENTITIES_FAILED, payload: [['Edit Request Problem. Please Try Again.']] });
            }
        }).catch(error => {
            console.log(error);
            dispatch({ type: GET_ENTITIES_FAILED, payload: error.response });
        });
    }
};

/* ===== Get Single Entity ===== */
const call_entity = (dispatch) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    get(`entity/admin/edit/${entity_id}`).then(result => {
        if(result.data.action) {
            dispatch({ type: GET_ENTITY_SUCCESS, payload: result.data.data });
        }else {
            dispatch({ type: GET_ENTITY_FAILED, payload: [['Edit Request Problem. Please Try Again.']] });
        }
    }).catch(error => {
        console.log(error);
        dispatch({ type: GET_ENTITY_FAILED, payload: error.response });
    });
};
export const get_single_entity = () => {
    return dispatch => {
        dispatch({ type: GET_ENTITY });
        call_entity(dispatch);
    }
};

/* ===== Save Entity Address ===== */
export const save_entity_address = (data) => {
    return dispatch => {
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        dispatch({ type: SAVE_ENTITY_ADDRESS });
        post(`entity/admin/address/${entity_id}/save`, data).then(result => {
            if(result.data.action) {
                // dispatch({ type: SAVE_ENTITY_ADDRESS_SUCCESS, payload: result.data.data });
            }else{
                // dispatch({ type: SAVE_ENTITY_ADDRESS_FAILED, payload: [['Edit Request Problem. Please Try Again.']] });
            }
        }).catch(error => {
            console.log(error);
            dispatch({ type: SAVE_ENTITY_ADDRESS_FAILED, payload: error.response });
        });
    }
};

const createFormData = (photo, body) => {
    const data = new FormData();
    // data.append("file", photo);
    // Object.keys(body).forEach(key => {
    //     data.append(key, body[key]);
    // });
    // const formData = new FormData();
    // formData.append('myImage',this.state.file);
    // const config = {
    //     headers: {
    //         'content-type': 'multipart/form-data'
    //     }
    // };
    // axios.post("/upload",formData,config)
    //     .then((response) => {
    //         alert("The file is successfully uploaded");
    //     }).catch((error) => {
    // });
    return data;
};

export const save_entity_image = (data, is_slider = true, service_id = null) => {
    return dispatch => {
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        const upload_url = is_slider ? `entity/admin/media/image/${entity_id}/entity/upload` : `entity/admin/media/image/${service_id}/entity_relation_service/upload`;
        dispatch({ type: SAVE_IMAGE });
        _.map(data, img => {
            upload(upload_url, null, img.file).then(result => {
                console.log(result)
                call_entity(dispatch);
                dispatch({ type: SAVE_IMAGE_SUCCESS, payload: result });
            }).catch(errors => {
                dispatch({ type: SAVE_IMAGE_FAILED, payload: errors.response });
            });
        });
    }
};

export const delete_entity_image = (id) => {
    return dispatch => {
        dispatch({ type: DELETE_IMAGE });
        x_delete(`entity/admin/media/${id}/delete`).then(result => {
            console.log(result);
            call_entity(dispatch);
            dispatch({ type: DELETE_IMAGE_SUCCESS, payload: result });
            setTimeout(()=>{
                dispatch({ type: CLEAN_GALLERY_ACTIONS, payload: result });
            }, 1000)
        }).catch(errors => {
            dispatch({ type: DELETE_IMAGE_FAILED, payload: errors.response });
        });
    }
};

/* ===== Get Categories ===== */
export const get_entity_categories = () => {
    return dispatch => {
        dispatch({ type: GET_ENTITY_CATEGORY });
        x_get(`entity/category/get/root/list`).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_ENTITY_CATEGORY_SUCCESS, payload: result.data.data });
            }else{
                dispatch({ type: GET_ENTITY_CATEGORY_FAILED, payload: [['Edit Request Problem. Please Try Again.']] });
            }
        }).catch(error => {
            console.log(error);
            dispatch({ type: GET_ENTITY_CATEGORY_FAILED, payload: error.response });
        });
    }
};

