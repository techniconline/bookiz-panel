import { post, get } from '../http';
import {
    GET_STAFFS,
    GET_STAFFS_SUCCESS,
    GET_STAFFS_FAILED,
    CURRENT_ENTITY,
    SAVE_STAFF,
    SAVE_STAFF_FAILED,
    SAVE_STAFF_SUCCESS, SAVE_STAFF_CALENDAR_SUCCESS, SAVE_STAFF_CALENDAR_FAILED
} from '../types';

export const get_members = () => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return (dispatch) => {
        dispatch({type: GET_STAFFS});
        get(`entity/admin/employee/show/${entity_id}`).then(result => {
            if(result.data.action){
                dispatch({ type: GET_STAFFS_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: GET_STAFFS_SUCCESS, payload: [[result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: GET_STAFFS_FAILED, payload: error.response.data });
        });
    }
};

export const save_member = (data) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return (dispatch) => {
        dispatch({type: SAVE_STAFF});
        post(`entity/admin/employee/${entity_id}/save`, data).then(result => {
            if(result.data.action){
                dispatch({ type: SAVE_STAFF_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: SAVE_STAFF_FAILED, payload: [[result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: SAVE_STAFF_FAILED, payload: error.response.data });
        });
    }
};

export const save_employee_working_hours = (data) => {
    const entity_id = localStorage.getItem(CURRENT_ENTITY);
    return (dispatch) => {
        dispatch({type: SAVE_STAFF_CALENDAR});
        post(`booking/admin/calendar/${entity_id}/6/save/save`, data).then(result => {
            if(result.data.action){
                dispatch({ type: SAVE_STAFF_CALENDAR_SUCCESS, payload: result.data.data });
            }else {
                dispatch({ type: SAVE_STAFF_CALENDAR_FAILED, payload: [[result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: SAVE_STAFF_CALENDAR_FAILED, payload: error.response.data });
        });
    }
};


export const get_staffs = () => {
    return (dispatch) => {
        dispatch({ type: GET_STAFFS });
        const payload = [
            {
                id: 1,
                name: 'Wendy Smith',
                image: 'https://cdn0.iconfinder.com/data/icons/user-pictures/100/malecostume-512.png'
            },
            {
                id: 2,
                name: 'Ahmad Khorshidi',
                image: 'https://cdn0.iconfinder.com/data/icons/user-pictures/100/malecostume-512.png'
            },
        ];
        setTimeout(() => {
            dispatch({ type: GET_STAFFS_SUCCESS, payload });
        }, 1000);
    }
};

// export const get_single = (id) => {
//     return (dispatch) => {
//         dispatch({ type: GET_SINGLE });
//         get(`get/${id}`).then(result => {
//             dispatch({ type: GET_SINGLE_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: GET_SINGLE_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
//
// export const add = (data) => {
//     return (dispatch) => {
//         dispatch({ type: ADD });
//         post('add', { data: 'data' }).then(result => {
//             dispatch({ type: ADD_SUCCESS, payload: result.data });
//         }).catch(error => {
//             dispatch({ type: ADD_FAILED, payload: 'FAILED MESSAGE!' });
//         })
//     }
// };
