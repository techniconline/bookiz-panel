import { post, get } from '../http';
import {
    CURRENT_ENTITY,
    GET_INVOICES_SUCCESS,
    GET_INVOICES_FAILED,
    GET_INVOICES,
    GET_INVOICE,
    GET_INVOICE_SUCCESS, GET_INVOICE_FAILED
} from '../types';

export const get_invoices = () => {
    return dispatch => {
        dispatch({ type: GET_INVOICES });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        get(`booking/admin/booking/${entity_id}/entities/list`).then(result => {
            if(result.data.action) {
                const pagination = {
                    total: result.data.data.total,
                    first_page_url: result.data.data.first_page_url,
                    last_page_url: result.data.data.last_page_url,
                    next_page_url: result.data.data.next_page_url,
                    prev_page_url: result.data.data.prev_page_url
                };
                dispatch({ type: GET_INVOICES_SUCCESS, payload: result.data.data.data, pagination });
            }else {
                dispatch({ type: GET_INVOICES_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request.status === 500) {
                dispatch({ type: GET_INVOICES_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: GET_INVOICES_FAILED, payload: errors.response});
            }
        });
    }
};

export const get_invoice_id = (id) => {
    return dispatch => {
        dispatch({ type: GET_INVOICE });
        get(`booking/admin/booking/show/${id}`).then(result => {
            if(result.data.action) {
                call_invoice(dispatch, result.data.data.order.single_url)
            }else {
                dispatch({ type: GET_INVOICE_FAILED, payload: [[result.data.message]] });
            }
        }).catch(errors => {
            if(errors.request && errors.request.status === 500) {
                dispatch({ type: GET_INVOICE_FAILED, payload: [[errors.request.statusText]] });
            }else {
                dispatch({type: GET_INVOICE_FAILED, payload: errors.response});
            }
        });
    }
};
export const get_invoice_url = (url) => {
    return dispatch => {
        dispatch({ type: GET_INVOICE });
        call_invoice(dispatch, url)
    }
};

const call_invoice = (dispatch, url) => {
    get(null, url).then(result => {
        if(result.data.action) {
            dispatch({ type: GET_INVOICE_SUCCESS, payload: result.data.data });
        }else {
            dispatch({ type: GET_INVOICE_FAILED, payload: [[result.data.message]] });
        }
    }).catch(errors => {
        if(errors.request && errors.request.status === 500) {
            dispatch({ type: GET_INVOICE_FAILED, payload: [[errors.request.statusText]] });
        }else {
            dispatch({type: GET_INVOICE_FAILED, payload: errors.response});
        }
    });
};