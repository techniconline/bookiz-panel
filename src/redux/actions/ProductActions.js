import { post, get } from '../http';
import {
    ADD,
    ADD_SUCCESS,
    ADD_FAILED,
    GET_SINGLE,
    GET_SINGLE_SUCCESS,
    GET_SINGLE_FAILED,
    GET_PRODUCT_CATEGORIES,
    GET_PRODUCT_CATEGORIES_SUCCESS,
    GET_PRODUCT_CATEGORIES_FAILED,
    GET_PRODUCTS,
    GET_PRODUCTS_SUCCESS,
    GET_PRODUCTS_FAILED, CURRENT_ENTITY, SAVE_PRODUCT, SAVE_PRODUCT_SUCCESS, SAVE_PRODUCT_FAILED
} from '../types';

export const get_products = () => {
    return (dispatch) => {
        dispatch({ type: GET_PRODUCTS });
        const entity_id = localStorage.getItem(CURRENT_ENTITY)
        get(`entity/admin/product/show/${entity_id}`).then(result => {
            if(result.data.action) {
                dispatch({ type: GET_PRODUCTS_SUCCESS, payload: result.data.data.data });
            }else {
                dispatch({ type: GET_PRODUCTS_FAILED, payload: (result.data.errors) ? result.data.errors : [[result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: GET_PRODUCTS_FAILED, payload: error.response.data.errors });
        });
    }
};
export const get_products1 = ({ category = '' }) => {
    return (dispatch) => {
        dispatch({ type: GET_PRODUCTS });
        setTimeout(()=>{
            const payload = [
                {
                    id: 1,
                    name: 'Shampoo',
                    price: 20,
                    discount: 5,
                    barcode: '123ABC',
                    stock: 6
                }
            ];
            dispatch({ type: GET_PRODUCTS_SUCCESS, payload });
        }, 1000);
    }
};

export const get_single_product = (id) => {
    return (dispatch) => {
        dispatch({ type: GET_SINGLE });
        get(`get/${id}`).then(result => {
            dispatch({ type: GET_SINGLE_SUCCESS, payload: result.data });
        }).catch(error => {
            dispatch({ type: GET_SINGLE_FAILED, payload: 'FAILED MESSAGE!' });
        })
    }
};

export const save_product = (data, id = 0) => {
    return (dispatch) => {
        dispatch({ type: SAVE_PRODUCT });
        const entity_id = localStorage.getItem(CURRENT_ENTITY);
        post(`entity/admin/product/${entity_id}/save`, data).then(result => {
            if(result.data.action) {
                dispatch({ type: SAVE_PRODUCT_SUCCESS, payload: result.data.data, edit: id !== 0 });
            }else {
                dispatch({ type: SAVE_PRODUCT_FAILED, payload: (result.data.errors) ? result.data.errors : [[result.data.message]] });
            }
        }).catch(error => {
            dispatch({ type: SAVE_PRODUCT_FAILED, payload: error.response.data.errors  });
        })
    }
};

export const get_product_categories = () => {
    return (dispatch) => {
        dispatch({ type: GET_PRODUCT_CATEGORIES });
        setTimeout(()=>{
            const payload = [
                { id: 1, name: 'Hair'},
                { id: 2, name: 'Body'},
                { id: 3, name: 'Face'},
                { id: 4, name: 'no-category'}
            ];
            dispatch({ type: GET_PRODUCT_CATEGORIES_SUCCESS, payload });
        }, 1000);
    }
};
