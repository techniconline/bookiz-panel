import React from 'react';
import styles from './../main.scss';

export const Badge = ({ linked = false, light = false, dark = false, info = false, warning = false, danger = false, success = false, primary = false, children, ...props }) => {
    const classes = [styles.badge];
    const colorType =
        (light) ? styles.light :
            (dark) ? styles.dark :
                (info) ? styles.info :
                    (warning) ? styles.warning :
                        (danger) ? styles.danger :
                            (success) ? styles.success :
                                (primary) ? styles.primary : null;
    if(colorType !== null) classes.push(colorType);
    if(linked) classes.push(styles.badge_linked);
   return (
       <span {...props} className={classes.join(' ')}>
           {children}
        </span>
   )
};

