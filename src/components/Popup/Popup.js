import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styles from '../main.scss';

class Popup extends Component {
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }
    componentWillUnmount() {
        const popup_overlay = document.getElementById('popup_overlay');
        if(popup_overlay) popup_overlay.remove();
    }

    componentDidUpdate() {
        const { refs } = this.props;
        if(refs) refs({ open: this.state.open, onToggle: this.onToggle.bind(this) });

        const popup_overlay = document.getElementById('popup_overlay');
        if(this.state.open && popup_overlay === null) {
            var layout = document.createElement("div");
            layout.id = "popup_overlay";
            layout.style = `
                position: fixed;
                background-color: rgba(0,0,0,0);
                top: 0;
                left: 0;
                height: 100%;
                width: 100%;
                z-index: 10;
            `;
            document.body.appendChild(layout);
            document.getElementById('popup_overlay').addEventListener('click', () => { this.setState({ open: false }); })
        }
        if(!this.state.open && popup_overlay !== null) {
            popup_overlay.remove();
        }
    }
    onToggle(open_status = null) {
        this.setState({ open: (open_status !== null) ? open_status : !this.state.open });
    }
    render() {
        const { item, children, position, disabled } = this.props;
        let { open } = this.state;
        if(item === null) return null;
        return (
            <div className={styles.popup_container}>
                <div onClick={(disabled) ? null : this.onToggle.bind(this)} className={(open) ? styles.item_active: styles.item}>
                    {item}
                </div>
                {(open && !disabled) ? (
                    <div className={styles[`popup_${position}`]}>
                        {children}
                    </div>
                ) : null}
            </div>
        )
    }
}

Popup.defaulProps = {
    item: null,
    open: null,
    position: 'bottom',
    disabled: false
};

Popup.propsTypes = {
    item: PropTypes.node.isRequired,
    position: PropTypes.string,
    open: PropTypes.bool,
    disabled: PropTypes.bool,
};

export default Popup;