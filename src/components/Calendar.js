import React, {Children} from 'react';
import BigCalendar from 'react-big-calendar';
import moment from 'moment';

const localizer = BigCalendar.momentLocalizer(moment);

const CURRENT_DATE = moment().toDate();

// example implementation of a wrapper
const ColoredDateCellWrapper = ({children, value}) =>
    React.cloneElement(Children.only(children), {
        style: {
            ...children.style,
            backgroundColor: value < CURRENT_DATE ? 'lightgreen' : 'lightblue',
        },
    });

export const Calendar = props => (
    <div style={{height: '100vh', margin: '10px'}}>
        <BigCalendar
            events={[]}
            startAccessor="startDate"
            endAccessor="endDate"
            localizer={localizer}
            components={{
                // you have to pass your custom wrapper here
                // so that it actually gets used
                dateCellWrapper: ColoredDateCellWrapper,
            }}
        />
    </div>
);

