import React, { Component, Fragment } from 'react';
// import style from "./styles.scss"
import style from './../main.scss';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faSignOutAlt,faTachometerAlt, faBox, faCog, faUser, faEnvelope, faReceipt, faCalendar, faBook } from '@fortawesome/free-solid-svg-icons';
import { Link, Redirect } from 'react-router-dom';
import {logout} from "../../redux/actions";
import {connect} from "react-redux";

class Sidebar extends Component{
    constructor(props) {
        super(props);
        this.state = {
            open: false
        };
    }
    onLogout() {
        this.props.logout();
    }
    componentDidUpdate() {
        const { minimize } = this.props;
        if(minimize !== this.state.minimize) {
            this.setState({ minimize, open: !minimize})
        }
    }
    onClose() {
        this.props.isMinimize(this.state.open);
        this.setState({ open: !this.state.open })
    }
    render() {
        if(!this.props.loggedIn) {
            return <Redirect to="/login" />
        }
        return (
            <Fragment>
                {(this.state.open) ?  <div onClick={this.onClose.bind(this)} className={style.sidebar_layout} /> : null}
                <div className={[style.sidebar, (!this.state.open)?style.isMinimize: null].join(' ')}>
                    <ul className={style.list}>
                        <li className={style.miniMenu} onClick={this.onClose.bind(this)}>
                           <span className={style.bars}>
                                <span className={style.barMenu} />
                                <span className={style.barMenu} />
                                <span className={style.barMenu} />
                           </span>
                        </li>
                        <li>
                            <Link to="/dashboard">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faTachometerAlt} />
                                <span>Dashboard</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/calendar">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faCalendar} />
                                <span>Calendar</span>
                            </Link>
                        </li>
                        {/*<li>*/}
                            {/*<Link to="/sales">*/}
                                {/*<FontAwesomeIcon style={{fontSize: 18}} icon={faTachometerAlt} />*/}
                                {/*<span>Sales</span>*/}
                            {/*</Link>*/}
                        {/*</li>*/}
                        <li>
                            <Link to="/bookings">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faBook} />
                                <span>Bookings</span>
                            </Link>
                        </li>

                        <li>
                            <Link to="/invoices">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faReceipt} />
                                <span>Invoices</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/comments">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faEnvelope} />
                                <span>Comments</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/staff">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faUser} />
                                <span>Staff</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/services">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faTachometerAlt} />
                                <span>Services</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/products">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faBox} />
                                <span>Products</span>
                            </Link>
                        </li>
                        <li>
                            <Link to="/settings">
                                <FontAwesomeIcon style={{fontSize: 18}} icon={faCog} />
                                <span>Settings</span>
                            </Link>
                        </li>
                        <li>
                            <a onClick={this.onLogout.bind(this)}>
                                <FontAwesomeIcon style={{fontSize: 20}} icon={faSignOutAlt} />
                               <span>Logout</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </Fragment>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        loggedIn: state.auth.loggedIn
    };
};

export default connect(mapStateToProps, { logout })(Sidebar);
