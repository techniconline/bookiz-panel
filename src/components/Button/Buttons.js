import React, {Fragment} from 'react';
import styles from './../main.scss';
import PropTypes from 'prop-types';
import uuid from "uuid";
import {MoonLoader} from "react-spinners";


export const Button = ({accept, refs, id, type, href, loading, loading_size, disabled, title, children, light, dark, info, warning, success, primary, danger, full, small, outline, onPress, classNames, ...props}) => {
    let classes = [styles.button];
    const colorType =
        (light) ? styles.light :
        (dark) ? styles.dark :
        (info) ? styles.info :
        (warning) ? styles.warning :
        (danger) ? styles.danger :
        (success) ? styles.success :
        (primary) ? styles.primary : null;
    const sizeType = (full) ? styles.full :
        (small) ? styles.small :
            null;

    if(colorType !== null) classes.push(colorType);
    if(sizeType !== null) classes.push(sizeType);

    if(outline) { classes.push(styles.outline); }

    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    
    const content = (title === '') ? children : title;
    if(type === 'file') {
        id = (id) ? id : uuid();
        return (
            <Fragment>
                <label htmlFor={id} className={classes.join(' ')}>{content}</label>
                <input accept={accept} ref={refs} {...props} type="file" id={id} className={styles.input_file} />
            </Fragment>
        )
    }
    if(type === 'link') {
        return (
            <a href={href} className={classes.join(' ')}>{content}</a>
        )
    }
    if(loading) {
        const loading_btn = [ ...classes, styles.loading_btn];
        return <div className={loading_btn.join(' ')}>
            <MoonLoader
                sizeUnit={"px"}
                size={loading_size}
                color={'#fff'}
                loading
            />
        </div>
    }
    return (
        <button type={type} onClick={onPress} disabled={disabled} className={classes.join(' ')}>{content}</button>
    )
};

export const ButtonGroup = ({ children, classNames }) => {
    let classes = [styles.btn_group];
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <div className={classes.join(' ')}>
          {children}
        </div>
    )
};

Button.defaultProps = {
    classNames: []
};
ButtonGroup.propTypes = {
    classNames: PropTypes.array
};

Button.defaultProps = {
    type: "button",
    title: "",
    children: "",
    href: "",
    light: false,
    dark: false,
    info: false,
    warning: false,
    primary: false,
    danger: false,
    full: false,
    small: false,
    classNames: [],
    loading: false,
    loading_size: 15
};

Button.propTypes = {
    type: PropTypes.string,
    title: PropTypes.string,
    href: PropTypes.string,
    children: PropTypes.string,
    light: PropTypes.bool,
    dark: PropTypes.bool,
    info: PropTypes.bool,
    warning: PropTypes.bool,
    primary: PropTypes.bool,
    danger: PropTypes.bool,
    full: PropTypes.bool,
    small: PropTypes.bool,
    onPress: PropTypes.func,
    loading: PropTypes.bool,
    loading_size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    classNames: PropTypes.array
};