import React from 'react';
import PropTypes from 'prop-types';
import styles from './../main.scss';

export const Table = ({ children, rowLinked, noPadding,  headStriped, bordered, striped }) => {
    const classes = [styles.tableContainer];
    if(striped) classes.push(styles.table_striped);
    if(bordered) classes.push(styles.table_bordered);
    if(headStriped) classes.push(styles.table_head_striped);
    if(rowLinked) classes.push(styles.table_row_linked);
    if(noPadding) classes.push(styles.table_no_padding);
    return (
        <div className={classes.join(' ')}>
            <table className={styles.table}>
                {children}
            </table>
        </div>
    )
};

Table.defaultProps = {
    rowLinked: false,
    noPadding: false,
    headStriped: false,
    bordered: false,
    striped: false
};
Table.propTypes = {
    rowLinked: PropTypes.bool,
    noPadding: PropTypes.bool,
    headStriped: PropTypes.bool,
    bordered: PropTypes.bool,
    striped: PropTypes.bool
};