import React from 'react';
// import style from './styles.scss';
import uuid from 'uuid';
import PropTypes from 'prop-types';
import style from './../main.scss';
import ReactDatePicker from 'react-datepicker';

/* ========== #Label ========== */
export const Label = (props) => {
    const id = (props.id) ? props.id : uuid();
    return (
        <label htmlFor={id} className={style.labelStyle}>{props.children}</label>
    )
};

/* ========== #Input ========== */
export const Input = ({type, id, placeholder, disabled , nobordered, onChange, value }) => {
    return (
        <input type={type}
               id={id}
               autoComplete="off"
               // defaultValue={defaultValue}
               value={value}
               className={(nobordered) ? style.inputNoBorderStyle : style.inputStyle}
               disabled={disabled}
               onChange={event => {if(onChange) onChange(event.target.value)}}
               placeholder={placeholder}
        />
    )
};
Input.defaultProps = {
    type: 'text',
    id: uuid(),
    placeholder: '' ,
    value: '',
    disabled: false,
    nobordered: false
};
Input.propTypes = {
    type: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    disabled: PropTypes.bool,
    nobordered: PropTypes.bool,
    onChange: PropTypes.func
};


/* ========== #Select ========== */
export const Select = ({id , disabled , nobordered, onChange, selected, options }) => {
    return (
        <select id={id}
               className={(nobordered) ? style.inputNoBorderStyle : style.inputStyle}
               disabled={disabled}
                value={selected}
               onChange={event => {if(onChange) onChange(event.target.value)}}>
            {
                options.map((option, ind) => {
                    return (option.data !== undefined) ? (
                        <optgroup key={ind} label={option.label}>
                            {option.data.map((opt, index) => {
                                let opt_disabled = false;
                                if(opt.disabled) opt_disabled = true;
                                return (
                                    <option key={index}
                                            value={opt.val}
                                            disabled={opt_disabled}
                                            style={(opt.hide)?{display:'none'}:null} >{opt.text}</option>
                                )
                            })}
                        </optgroup>
                    ) : (
                        <option key={ind} value={option.val} style={(option.hide)?{display:'none'}:null} >{option.text}</option>
                    )
                })
            }
        </select>
    )
};

Select.defaultProps = {
    id: uuid(),
    disabled: false,
    nobordered: false,
    selected: '',
    options: []
};
Select.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    disabled: PropTypes.bool,
    nobordered: PropTypes.bool,
    selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    options: PropTypes.array,
    onChange: PropTypes.func
};

/* ========== #Textarea ========== */
export const TextArea = ({ id , placeholder ,nobordered , onChange, value, disabled, rows, classNames }) => {
    let classes = [(nobordered) ? style.inputNoBorderStyle : style.inputStyle];
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <textarea id={id}
                  value={value}
                  className={classes.join(' ')}
                  disabled={disabled}
                  rows={rows}
                  onChange={event => onChange(event.target.value)}
                  placeholder={placeholder} />
    )
};
TextArea.defaultProps = {
    id: uuid(),
    rows: 3,
    placeholder: '',
    nobordered: false,
    classNames: [],
    value: '',
    disabled: false
};
TextArea.propTypes = {
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    classNames: PropTypes.array,
    rows: PropTypes.number,
    placeholder: PropTypes.string,
    value: PropTypes.string,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
};

/* ========== #Checkbox ========== */
export const Checkbox = ({ title, id, defaultChecked, checked, disabled, classNames, onChange, ...Props }) => {
    let classes = [style.checkboxContent];
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <div className={classes.join(' ')} {...Props}>
            <div className={style.checkContainer}>
                <input type="checkbox" id={id}
                       className={style.inputStyle}
                       // value={value}
                       // defaultChecked={defaultChecked}
                        checked={checked}
                       onChange={onChange}
                       disabled={disabled} />
                <label htmlFor={id} />
            </div>
            <label htmlFor={id}>{title}</label>
        </div>
    )
};
Checkbox.defaultProps = {
    title: '',
    id: uuid(),
    defaultChecked: false,
    checked: false,
    disabled: false,
    classNames: []
};
Checkbox.propTypes = {
    title: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    defaultChecked: PropTypes.bool,
    checked: PropTypes.bool,
    disabled: PropTypes.bool,
    classNames: PropTypes.array,
    onChange: PropTypes.func,
};

/* ========== #Radio ========== */
export const Radio = ({ title = '', id = uuid(), name = uuid(), defaultChecked = false, disabled = false, onChange }) => {
    return (
        <div className={style.checkboxContent}>
            <label htmlFor={id} className={style.checkTitleStyle}>{title}</label>
            <div className={style.radioContainer}>
                <input type="radio" id={id} name={name} defaultChecked={defaultChecked} disabled={disabled} className={style.inputStyle} />
                <label htmlFor={id} />
            </div>
        </div>
    )
};


export const InputForm = ({ labelText, type, id, placeholder, value, onChange, disabled, ...Props }) => {
    return (
        <div {...Props} className={style.inputContainer}>
            <Label id={id}>{labelText}</Label>
            <Input type={type}
                   id={id}
                   onChange={onChange}
                   value={value}
                   placeholder={placeholder}
                   disabled={disabled}/>
        </div>
    )
};
InputForm.defaultProps = {
    labelText: '',
    type: 'text',
    id: uuid(),
    placeholder: '' ,
    value: '',
    disabled: false
};
InputForm.propTypes = {
    labelText: PropTypes.string,
    type: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    disabled: PropTypes.bool,
    onChange: PropTypes.func
};

export const SelectForm = ({ labelText, id, selected, options, onChange, disabled }) => {
    return (
        <div className={style.inputContainer}>
            <Label id={id}>{labelText}</Label>
            <Select id={id}
                   onChange={onChange}
                    selected={selected}
                   options={options}
                   disabled={disabled}/>
        </div>
    )
};
SelectForm.defaultProps = {
    labelText: '',
    id: uuid(),
    selected: '' ,
    options: [],
    disabled: false
};
SelectForm.propTypes = {
    labelText: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    selected: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    // options: PropTypes.arrayOf(PropTypes.shape({
    //     val: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    //     text: PropTypes.string.isRequired
    // })),
    options: PropTypes.array,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
};

export const TextAreaForm = ({ labelText, id, rows, placeholder ,value, onChange, disabled }) => {
    return (
        <div className={style.inputContainer}>
            <Label id={id}>{labelText}</Label>
            <TextArea id={id}
                      onChange={onChange}
                      value={value}
                      rows={rows}
                      placeholder={placeholder}
                      disabled={disabled}/>
        </div>
    )
};
TextAreaForm.defaultProps = {
    labelText: '',
    id: uuid(),
    placeholder: '' ,
    value: '',
    rows: 3,
    disabled: false
};
TextAreaForm.propTypes = {
    labelText: PropTypes.string,
    id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    placeholder: PropTypes.string,
    value: PropTypes.string,
    rows: PropTypes.number,
    disabled: PropTypes.bool,
    onChange: PropTypes.func
};

export const DatePicker = ({ selected, includeDates, disabled, placeholder, showTimeSelect, timeIntervals, includeTimes, timeCaption, dateFormat, timeFormat, showTimeSelectOnly, onChange }) => {
    return (
        <ReactDatePicker
            selected={selected}
            onChange={onChange}
            includeTimes={includeTimes}
            includeDates={includeDates}
            disabled={disabled}
            showTimeSelect={showTimeSelect}
            showTimeSelectOnly={showTimeSelectOnly}
            timeIntervals={15}
            dateFormat={dateFormat}
            timeFormat={timeFormat}
            timeCaption={timeCaption}
            className={style.inputStyle}
            placeholderText={placeholder} />
    )
};

DatePicker.defaultProps = {
    selected: null,
    includeTimes: false,
    includeDates: false,
    showTimeSelectOnly: false,
    showTimeSelect: false,
    disabled: false,
    timeFormat:"HH:mm",
    dateFormat:"MMMM d, yyyy ",
    timeIntervals: false,
    placeholder: '',
    timeCaption: false,
};

DatePicker.propTypes = {
    selected: PropTypes.any,
    includeDates: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
    includeTimes: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
    disabled: PropTypes.bool,
    showTimeSelect: PropTypes.bool,
    showTimeSelectOnly: PropTypes.bool,
    placeholder: PropTypes.string,
    timeCaption: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    timeIntervals: PropTypes.oneOfType([PropTypes.number, PropTypes.bool]),
    dateFormat: PropTypes.oneOfType([PropTypes.string, PropTypes.bool]),
    onChange: PropTypes.func
};

export const DatePickerForm = ({ id, labelText, selected, includeDates, includeTimes, disabled, placeholder, showTimeSelect, timeIntervals, timeCaption, dateFormat, timeFormat,showTimeSelectOnly, onChange }) => {
    return (
        <div className={style.inputContainer}>
            <Label id={id}>{labelText}</Label>
            <DatePicker
                selected={selected}
                onChange={onChange}
                includeDates={includeDates}
                includeTimes={includeTimes}
                disabled={disabled}
                showTimeSelect={showTimeSelect}
                showTimeSelectOnly={showTimeSelectOnly}
                timeIntervals={timeIntervals}
                dateFormat={dateFormat}
                timeFormat={timeFormat}
                timeCaption={timeCaption}
                className={style.inputStyle}
                placeholderText={placeholder}  />
        </div>
    )
};

DatePickerForm.defaultProps = {
    id: uuid(),
    labelText: '',
    selected: null,
    includeDates: false,
    includeTimes: false,
    showTimeSelectOnly: false,
    showTimeSelect: false,
    disabled: false,
    timeFormat:"HH:mm",
    dateFormat:"MMMM d, yyyy ",
    timeIntervals: 15,
    placeholder: '',
    timeCaption: '',
};

DatePickerForm.propTypes = {
    labelText: PropTypes.string,
    selected: PropTypes.any,
    includeDates: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
    includeTimes: PropTypes.oneOfType([PropTypes.array, PropTypes.bool]),
    disabled: PropTypes.bool,
    showTimeSelect: PropTypes.bool,
    showTimeSelectOnly: PropTypes.bool,
    placeholder: PropTypes.string,
    timeIntervals: PropTypes.number,
    timeCaption: PropTypes.string,
    dateFormat: PropTypes.string,
    onChange: PropTypes.func
};