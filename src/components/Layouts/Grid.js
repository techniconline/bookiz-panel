import React from 'react';
// import styles from './grid.scss';
import styles from './../main.scss';
import PropTypes from "prop-types";

export const Wrapper = ({children, title = '', head = null, classNames = []}) => {
    let classes = [styles.wrapper];
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <div className={classes.join(' ')} >
            {(title !== '' || head !== null) ? (
                <div className={styles.head}>
                    {(head === null) ? (<h2 className={styles.title}>{title}</h2>) : head }
                </div>
            ) : null}
            {children}
        </div>
    )
};


export const Grid = ({children, spaceBetween, spaceAround, spaceEvenly, center, start, end, classNames}) => {
    let classes = [styles.grid];
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    if(center) classes.push(styles.grid_center);
    if(start) classes.push(styles.grid_start);
    if(end) classes.push(styles.grid_end);
    if(spaceBetween) classes.push(styles.grid_space_between);
    if(spaceAround) classes.push(styles.grid_space_around);
    if(spaceEvenly) classes.push(styles.grid_space_evenly);
    return (
        <div className={classes.join(' ')} >
            {children}
        </div>
    )
};
Grid.propTypes = {
    spaceBetween: PropTypes.bool,
    spaceAround: PropTypes.bool,
    spaceEvenly: PropTypes.bool,
    center: PropTypes.bool,
    start: PropTypes.bool,
    end: PropTypes.bool,
    classNames: PropTypes.array
};

Grid.defaultProps = {
    spaceBetween: false,
    spaceAround: false,
    spaceEvenly: false,
    center: false,
    start: false,
    end: false,
    classNames: []
};


export const Column = ({children, col, classNames, isSection, xsSize, smSize, mSize, lSize, xlSize}) => {
    let classes = [styles['column'+col]];

    if(isSection) { classes.push(styles.section); }
    if (xsSize !== 0) { classes.push(styles['column'+xsSize+'_xs']); }
    if (smSize !== 0) { classes.push(styles['column'+smSize+'_sm']); }
    if (mSize !== 0) { classes.push(styles['column'+mSize+'_m']); }
    if (lSize !== 0) { classes.push(styles['column'+lSize+'_l']); }
    if (xlSize !== 0) { classes.push(styles['column'+xlSize+'_xl']); }

    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <div className={classes.join(' ')} >
            {children}
        </div>
    )
};
Column.propTypes = {
    col: PropTypes.number,
    classNames: PropTypes.array,
    isSection: PropTypes.bool,
    xsSize: PropTypes.number,
    smSize: PropTypes.number,
    mSize: PropTypes.number,
    lSize: PropTypes.number,
    xlSize: PropTypes.number
};

Column.defaultProps = {
    col: 12,
    classNames: [],
    isSection: false,
    xsSize: 0,
    smSize: 0,
    mSize: 0,
    lSize: 0,
    xlSize: 0
};

