import React from 'react';
import _ from 'lodash';
import styles from '../main.scss';
import PropTypes from 'prop-types';

export class Tabs extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            selected: 0
        }
    }
    render() {
        const tab_head = (this.props.full) ? styles.tab_header_full : styles.tab_header;
        const children = (Array.isArray(this.props.children)) ? this.props.children : [this.props.children];
        return (
            <div className={styles.tabs}>
                <div className={tab_head}>
                    {_.map(children, (child, key) => (
                            <h4 key={key}
                                className={(this.state.selected === key) ? styles.active : null}
                                onClick={()=>{this.setState({ selected: key })}}>{child.props.heading}</h4>
                        ))}
                </div>
                <div className={styles.tab_body}>
                    {_.map(children, (child, key) => (
                        <div key={key} className={(this.state.selected === key) ? styles.active : null}>{child.props.children}</div>
                    ))}
                </div>
            </div>
        );
    }
}

Tabs.defaultProps = {
    full: false
};

Tabs.propTypes = {
    full: PropTypes.bool
};

export const Tab = ({ heading = '', children }) => {
    return (
        <div className={styles.tab}>
            <div className={styles.tab_body}>{children}</div>
        </div>
    );
};