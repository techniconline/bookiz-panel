import React from 'react';
import _ from 'lodash';
import PropTypes from 'prop-types';
import {MoonLoader} from "react-spinners";
import styles from './styles.scss';
import {Button} from "..";

export const FullLoading = () => (
    <div className={styles.full_loading}>
        <MoonLoader
            sizeUnit={"px"}
            size={30}
            color={'#24334A'}
            loading
        />
    </div>
);

export const ErrorPage = (errors) => {
    return _.map(errors, (error, key) => (
        <p key={key} className={styles.errorTxt}>{error[0]}</p>
    ))
};

ErrorPage.propTypes = {
    errors: PropTypes.oneOfType([PropTypes.array, PropTypes.object]),
};