import React from 'react';
import styles from '../../main.scss';
import PropTypes from "prop-types";

export const Alert = ({ children, linked, light, dark, info, warning, success, primary, danger, small, classNames, ...props }) => {
    let classes = [styles.alert];
    const colorType =
        (light) ? styles.light :
            (dark) ? styles.dark :
                (info) ? styles.info :
                    (warning) ? styles.warning :
                        (danger) ? styles.danger :
                            (success) ? styles.success :
                                (primary) ? styles.primary : null;
    if(colorType !== null) classes.push(colorType);
    if(linked) classes.push(styles.alert_linked);
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <div {...props} className={classes.join(' ')}>
            {children}
        </div>
    )
};

Alert.propTypes = {
    linked: PropTypes.bool,
    light: PropTypes.bool,
    dark: PropTypes.bool,
    info: PropTypes.bool,
    warning: PropTypes.bool,
    success: PropTypes.bool,
    primary: PropTypes.bool,
    danger: PropTypes.bool,
    small: PropTypes.bool,
    classNames: PropTypes.array
};

Alert.defaultProps = {
    children: '',
    linked: false,
    light: false,
    dark: false,
    info: false,
    warning: false,
    success: false,
    primary: false,
    danger: false,
    small: false,
    classNames: []
};
