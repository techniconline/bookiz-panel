import React from 'react';
import PropTypes from 'prop-types';
import styles from '../../main.scss';

export const Toast = ({ children, show, linked, light, dark, info, warning, success, primary, danger, small, classNames, duration, ...props }) => {
    let classes = [styles.toast];
    const colorType =
        (light) ? styles.light :
            (dark) ? styles.dark :
                (info) ? styles.info :
                    (warning) ? styles.warning :
                        (danger) ? styles.danger :
                            (success) ? styles.success :
                                (primary) ? styles.primary : null;
    if(colorType !== null) classes.push(colorType);
    if(linked) classes.push(styles.alert_linked);
    if(show) classes.push(styles.toast_show);
    if(typeof classNames === 'object' && classNames.length > 0) {
        classes = classes.concat(classNames);
    }
    return (
        <div {...props} className={classes.join(' ')}>
            {children}
        </div>
    )
};

Toast.defaultProps = {
    children: '',
    show: false,
    linked: false,
    light: false,
    dark: false,
    info: false,
    warning: false,
    success: false,
    primary: false,
    danger: false,
    small: false,
    classNames: [],
    duration: 3000
};

Toast.propTypes = {
    children: PropTypes.any,
    show: PropTypes.bool,
    linked: PropTypes.bool,
    light: PropTypes.bool,
    dark: PropTypes.bool,
    info: PropTypes.bool,
    warning: PropTypes.bool,
    success: PropTypes.bool,
    primary: PropTypes.bool,
    danger: PropTypes.bool,
    small: PropTypes.bool,
    classNames: PropTypes.array,
    duration: PropTypes.number
};