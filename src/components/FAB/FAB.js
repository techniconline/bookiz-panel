import React from 'react';
import PropTypes from 'prop-types';
import _ from 'lodash';
// import styles from './styles.scss';
import styles from './../main.scss';

export const FAB = ({ children, subs, right, left, top, bottom, light, dark, info, warning, success, primary, danger, size}) => {
    const classes = [styles.fab];
    if(left) classes.push(styles.left);
    if(right) classes.push(styles.right);
    if(top) classes.push(styles.top);
    if(bottom) classes.push(styles.bottom);

    const colorType =
        (light) ? styles.light :
            (dark) ? styles.dark :
                (info) ? styles.info :
                    (warning) ? styles.warning :
                        (danger) ? styles.danger :
                            (success) ? styles.success :
                                (primary) ? styles.primary : null;

    if(colorType !== null) classes.push(colorType);
    classes.push(styles[size]);
    const subclasses = [styles.sub];
    if(colorType !== null) subclasses.push(colorType);

    return (
        <div className={[styles.container, styles['l_'+subs.length]].join(' ')}>
            {_.map(subs, (sub, i) => <div key={i} className={[...subclasses, styles['priority_'+(i+1)]].join(' ')}>{sub}</div>)}
            <div className={classes.join(' ')}>
                {children}
            </div>
        </div>
    );
};

FAB.defaultProps = {
    subs: [],
    right: true,
    left: false,
    top: false,
    bottom: true,
    light: false,
    dark: false,
    info: false,
    warning: false,
    success: false,
    primary: true,
    danger: false,
    size: 'medium'
};
FAB.propTypes = {
    subs: PropTypes.array,
    right: PropTypes.bool,
    left: PropTypes.bool,
    top: PropTypes.bool,
    bottom: PropTypes.bool,
    light: PropTypes.bool,
    dark: PropTypes.bool,
    info: PropTypes.bool,
    warning: PropTypes.bool,
    success: PropTypes.bool,
    primary: PropTypes.bool,
    danger: PropTypes.bool,
    size: PropTypes.string
};

export const FABItem = ({ children, label, ...props }) => {
    return (
        <div className={styles.fab_data}  {...props}>
            {children}
            {(label !== '') ? ( <div className={styles.fab_label}>{label}</div> ) : null}
        </div>
    )
};

FABItem.defaultProps = {
    label: ''
};
FABItem.propTypes = {
    label: PropTypes.string
};
//
// export const SubFAB = ({ children, right = true, left = false, top = false, bottom = true, light=false, dark=false, info=false, warning=false, success=false, primary=true, danger=false, size='medium', priority = 1}) => {
//     const classes = [styles.sub];
//     if(left) classes.push(styles.left);
//     if(right) classes.push(styles.right);
//     if(top) classes.push(styles.top);
//     if(bottom) classes.push(styles.bottom);
//     classes.push(styles['priority_'+priority]);
//     classes.push(styles[size]);
//
//     const colorType =
//         (light) ? styles.light :
//             (dark) ? styles.dark :
//                 (info) ? styles.info :
//                     (warning) ? styles.warning :
//                         (danger) ? styles.danger :
//                             (success) ? styles.success :
//                                 (primary) ? styles.primary : null;
//
//     if(colorType !== null) classes.push(colorType);
//     // if(sizeType !== null) classes.push(sizeType);
//
//     return (
//         <div className={classes.join(' ')}>
//             {children}
//         </div>
//     );
// };

