import React from 'react';
import Chart from 'chart.js';
import uuid from 'uuid';

export const ChartJs = ({height = 400, width = 400, data = null, className=''}) => {
    const id = uuid();
    if(data === null) return;
    setTimeout(()=>{
        const ctx = document.getElementById(id);
        if(ctx) {
            const myChart = new Chart(ctx, data);
        }
    }, 1);
    return (<canvas id={id} width={width} height={height} className={className} />);
};