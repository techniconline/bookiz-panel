import React from 'react';
import PropTypes from 'prop-types';
import style from './../main.scss';
import Sidebar from '../Sidebar/Sidebar';
import {faBars, faArrowLeft} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

export class PanelContainer extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isMinimize: true
        };
    }
   render() {
       const {title, children, hasContent, main} = this.props;
        return (
            <div className={[style.panel, (this.state.isMinimize) ? style.sidebarMinimized : null ].join(' ')}>
                <Sidebar minimize={this.state.isMinimize} isMinimize={(isMinimize)=>{this.setState({ isMinimize })}} />
                <div className={style.panelHead}>
                    {(main) ? (
                        <button onClick={()=>{this.setState({ isMinimize: !this.state.isMinimize})}}
                                className={style.sidebar_toggle}>
                            <FontAwesomeIcon icon={faBars} />
                        </button>
                    ): (
                        <button onClick={()=>{history.back()}}
                                className={style.sidebar_toggle}>
                            <FontAwesomeIcon icon={faArrowLeft} />
                        </button>
                    )}

                    {(title !== '')? <h1>{title}</h1>:null}
                </div>
                <div className={style.panelContainer}>
                    {(hasContent) ? (
                        <div className={style.panelContent}>
                            { children }
                        </div>
                    ) : children}
                </div>
            </div>
        )
    }
}

PanelContainer.defaultProps = {
    title: '',
    children: '',
    hasContent: true,
    main: true
};

PanelContainer.propTypes = {
    title: PropTypes.string.isRequired,
    children: PropTypes.node,
    hasContent: PropTypes.bool,
    main: PropTypes.bool,
};