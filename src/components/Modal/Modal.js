import React from 'react';
import uuid  from 'uuid';
import PropTypes from 'prop-types';
import _ from 'lodash';
// import styles from './styles.scss';
import styles from './../main.scss';

export class Modal extends React.Component {
    initId = uuid();
    constructor(props) {
        super(props);
        this.state = {
            open: false
        }
    }
    componentDidUpdate() {
        if(this.state.open !== this.props.open){
            this.setState({ open: this.props.open });
        }
    }
    onClose(e) {
        if(e.target.dataset.id === this.initId){
            this.setState({ open: false }, () => {
                if(this.props.onClose) this.props.onClose(true);
                if(this.props.onOpen) this.props.onOpen(false);
            });
        }
    }
    onOpen() {
        this.setState({ open: true }, () => {
            if(this.props.onClose) this.props.onClose(false);
            if(this.props.onOpen) this.props.onOpen(true);
        });
    }
    render() {
        const {children, title, big, full, no_padding, height, width} = this.props;
        const data = (Array.isArray(children)) ? children : children.props.children;
        const footerData = _.filter(data, child => child.props.modalFooter === true);
        const headData = _.filter(data, child => child.props.modalHead === true);
        const mainData = _.filter(data, child => child.props.modalFooter !== true && child.props.modalHead !== true);

        const contentClasses = [styles.modalContent];
        contentClasses.push((big) ? styles.big : (full) ? styles.full : styles.medium);
        const custom_styles = {};
        if(height) custom_styles.height = height;
        if(width) custom_styles.width = width;

        return (this.state.open) ? (
            <div className={styles.modal} data-id={this.initId} onClick={this.onClose.bind(this)}>
                <div className={contentClasses.join(' ')} style={custom_styles}>
                    {(title) ? (
                        <div className={styles.modalHeader}>
                            <h1>{title}</h1>
                            <div className={styles.dismiss} data-id={this.initId} onClick={this.onClose.bind(this)} >x</div>
                        </div>
                    ) : null}

                    {(!no_padding) ?
                        <div className={styles.modalBody}>
                            {children}
                        </div>
                        : children
                    }
                    {/*<div className={styles.modalFooter}>*/}
                        {/*{footerData}*/}
                    {/*</div>*/}
                </div>
            </div>
        ) : null;
    }
}

Modal.propTypes = {
    title: PropTypes.string,
    open: PropTypes.bool,
    big: PropTypes.bool,
    full: PropTypes.bool,
    no_padding: PropTypes.bool,
    onClose: PropTypes.func,
    height: PropTypes.string,
    width: PropTypes.string,
};

Modal.defaultProps = {
    big: false,
    full: false,
    no_padding: false
};
