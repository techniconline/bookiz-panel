import { faMale, faFemale } from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import React from "react";

export * from './Date';
export const gender = (type) => {
    return (type === 1) ? 'Female' : 'Male';
};
export const gender_icon = (type) => {
    return <FontAwesomeIcon style={{fontSize: 16, color: (type === 1) ?'#d32f2f': '#1976D2'}} icon={(type === 1) ? faFemale : faMale} />;
};

