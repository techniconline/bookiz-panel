const days_min = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
const months_min = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
const days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
const months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
const start_date = 0; // Sun


export const format_date_item = (data) => {
    const date = new Date(data);
    let hour = date.getHours();
    let min = date.getMinutes();
    hour = (hour < 10) ? '0'+hour : hour;
    min = (min < 10) ? '0'+min : min;
    return date.getDate() + ' ' + months_min[date.getMonth()+1] + ' ' + date.getFullYear() + ', ' + hour +':'+min;
};

export const format_date = (data) => {
    const date = new Date(data);
    return date.getDate() + ' ' + months_min[date.getMonth()] + ' ' + date.getFullYear() ;
};

export const format_time = (data) => {
    const date = new Date(data);
    let hour = date.getHours();
    let min = date.getMinutes();
    hour = (hour < 10) ? '0'+hour : hour;
    min = (min < 10) ? '0'+min : min;
    return hour +':'+min;
};

export const date_to_string = (date) => {
    const year = date.getFullYear();
    const month = date.getMonth()+1;
    const day = date.getDate();
    return year + '-' + month + '-' + day;
};

export const string_format_date_time = (string) => {
    const full_date = string.split(' ')[0];
    const time = string.split(' ')[1];

    const year = full_date.split('-')[0];
    const month = Number(full_date.split('-')[1])-1;
    const date = full_date.split('-')[2];

    const hours = time.split(':')[0];
    const min = time.split(':')[1];
    return new Date(year,month,date,hours,min);
};

export const string_format_date = (string) => {
    const full_date = string.split(' ')[0];

    const year = full_date.split('-')[0];
    const month = Number(full_date.split('-')[1])-1;
    const date = full_date.split('-')[2];

    return new Date(year,month,date);
};

export const get_week = (date) => {

};